# Binary Neural Network

It's a library.

## Example Program

## :notebook: Metadata

| Key              | Value |
| :--------------- | :--------------------------------------------------------------------------------------------------------------------- |
| Version          | 0.1                                                                                                                    |
| Project Homepage | -                                                                                                                      |
| GitLab           | [https://gitlab.com/kerst-thomas-bnn/binary_neural_network](https://gitlab.com/kerst-thomas-bnn/binary_neural_network) |
