#include "../include/pre.h"
#include "../include/macros.h"
#include "../include/strings.h"

#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>


// constants
#define STRING_BUFFER 256
#define STRING_BUFFER_LONG 512


// static variables
static FILE *f_model;
static FILE *f_layer;


// static functions
static void start_writing(void)
{
	// model
	f_model = fopen(PATH_MODEL_HEADER, "w");
	FATAL_ERROR(!f_model, FE_HEADER);

	// layer
	f_layer = fopen(PATH_LAYER_INIT_HEADER, "w");
	FATAL_ERROR(!f_layer, FE_HEADER);
}

static void write_preamble(void)
{
	size_t len;

	// model
	len = fwrite(PREAMBLE_MODEL, 1, strlen(PREAMBLE_MODEL), f_model);
	FATAL_ERROR(strlen(PREAMBLE_MODEL) != len, FE_HEADER);

	// layer
	len = fwrite(PREAMBLE_LAYER, 1, strlen(PREAMBLE_LAYER), f_layer);
	FATAL_ERROR(strlen(PREAMBLE_LAYER) != len, FE_HEADER);
}

static bool does_file_exist(const char *path)
{
	return (access(path, F_OK) != -1) ? true : false;
}

static void get_abbreviation(const char *path, char *wrt)
{
	// open file
	FILE *g = fopen(path, "r");
	FATAL_ERROR(!g, FE_LAYER);

	// get length
	FATAL_ERROR(fseek(g, 0, SEEK_END), FE_LAYER);
	size_t len = (size_t)ftell(g) - 1;
	FATAL_ERROR(fseek(g, 0, SEEK_SET), FE_LAYER);

	// read out file
	FATAL_ERROR(len != fread(wrt, 1, len, g), FE_LAYER);
	wrt[len] = 0;

	// close file
	fclose(g);
}

static void write_layer_type_model(const char *name,
                                   const char *abbreviation,
                                   const char *sep)
{
	char buffer[STRING_BUFFER];
	sprintf(buffer, "%s\n\t{\"%s\", &%s_init}", sep, abbreviation, name);
	size_t len = fwrite(buffer, 1, strlen(buffer), f_model);
	FATAL_ERROR(strlen(buffer) != len, FE_HEADER);
}

static void write_layer_type_layer(const char *name)
{
	char buffer[STRING_BUFFER];
	sprintf(buffer, WRT_LAYER_TYPE, name);
	size_t len = fwrite(buffer, 1, strlen(buffer), f_layer);
	FATAL_ERROR(strlen(buffer) != len, FE_LAYER);
}

static void write_layer_types(void)
{
	static bool is_first = false;

	// open the directory
	DIR *dir = opendir(PATH_LAYER_TYPES);
	FATAL_ERROR(!dir, FE_DIR);

	// iterate through the content
	for (struct dirent *p = readdir(dir); p; p = readdir(dir)) {
		// check if the file is a directory
		if (*p->d_name == '.')
			continue;

		// check if the directory contains a valid layer defintion
		char buffer[STRING_BUFFER_LONG];
		sprintf(buffer, "%s%s/init", PATH_LAYER_TYPES, p->d_name);
		if (!does_file_exist(buffer))
			continue;

		// extract the init information
		char buf[STRING_BUFFER];
		get_abbreviation(buffer, buf);

		// write to the files
		write_layer_type_model(p->d_name, buf, (is_first) ? "," : "");
		write_layer_type_layer(p->d_name);

		// toggle the first layer switch
		is_first = true;
	}

	// clean up
	closedir(dir);
}

static void write_footer(void)
{
	size_t len;

	// model
	const char footer_model[] = "\n};\n\n\n#endif\n";
	len = fwrite(footer_model, 1, strlen(footer_model), f_model);
	FATAL_ERROR(strlen(footer_model) != len, FE_HEADER);

	// layer
	const char footer_layer[] = "\n#endif\n";
	len = fwrite(footer_layer, 1, strlen(footer_layer), f_layer);
	FATAL_ERROR(strlen(footer_layer) != len, FE_HEADER);
}

static void finish_writing(void)
{
	fclose(f_model);
	fclose(f_layer);
}


// main
void prepare_layer_information(void)
{
	start_writing();
	write_preamble();
	write_layer_types();
	write_footer();
	finish_writing();
}
