#include "../include/pre.h"
#include "../include/macros.h"
#include "../include/strings.h"

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>


// constants
#define STRING_BUFFER 512


// static variables
static char configuration_file_path[STRING_BUFFER];
static char include_path[STRING_BUFFER];


// static functions
static void wrt_to_file(FILE *f, const char *str, const char *err)
{
	size_t len = fwrite(str, 1, strlen(str), f);
	FATAL_ERROR(strlen(str) != len, err);
}

static bool is_training_enabled(void)
{
	// construct the full file paths
	char *p = getcwd(configuration_file_path, STRING_BUFFER);
	for (p += strlen(p); *p != '/'; --p)
	sprintf(p, "%s", TRAINING_CONFIGURATION_FILE_PATH);

	return (access(configuration_file_path, F_OK) != -1) ? true : false;
}

static char *get_file_content(const char *path)
{
	// open file
	FILE *f = fopen(path, "r");
	FATAL_ERROR(!f, FE_CONFIG);

	// get length
	FATAL_ERROR(fseek(f, 0, SEEK_END), FE_CONFIG);
	size_t len = (size_t)ftell(f) - 1;
	FATAL_ERROR(fseek(f, 0, SEEK_SET), FE_CONFIG);

	// read out file
	char *file_content = malloc(len + 1);
	FATAL_ERROR(len != fread(file_content, 1, len, f), FE_CONFIG);
	file_content[len] = 0;

	// close file
	fclose(f);

	return file_content;
}

static void get_python_include_path(void)
{
	// get file content
	char *file_content = get_file_content(configuration_file_path);

	// extract 0-th line
	char *eol = strstr(file_content, "\n");
	size_t len = eol - file_content;
	memcpy(include_path, file_content, len);
	include_path[len] = 0;

	// clean up
	free(file_content);
}

static void write_include_header(void)
{
	// build header path
	char header_path[STRING_BUFFER];
	char *p = getcwd(header_path, STRING_BUFFER);
	for (p += strlen(p); *p != '/'; --p)
	sprintf(p, "%s", HEADER_FILE_PATH);

	// open the header file
	FILE *f = fopen(header_path, "w");
	FATAL_ERROR(!f, FE_WRITE);

	// write to the file
	wrt_to_file(f, HEADER_START, FE_WRITE);
	wrt_to_file(f, include_path, FE_WRITE);
	wrt_to_file(f, HEADER_END, FE_WRITE);

	// close the file
	fclose(f);
}


// functions
void prepare_training_header(void)
{
	// if training is not enabled, return
	if (!is_training_enabled())
		return;

	// write include header
	get_python_include_path();
	write_include_header();
}
