#include "../include/pre.h"
#include "../include/macros.h"
#include "../include/strings.h"

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>


// constants
#define STRING_BUFFER 512
#define STRING_BUFFER_LONG (4 * STRING_BUFFER)


// static variables
static char configuration_file_path[STRING_BUFFER];
static char include_path[STRING_BUFFER];
static unsigned int n_paths;
static char* *paths;


// static functions
static void wrt_to_file(FILE *f, const char *str, const char *err)
{
	size_t len = fwrite(str, 1, strlen(str), f);
	FATAL_ERROR(strlen(str) != len, err);
}

static bool is_training_enabled(void)
{
	// construct the full file paths
	char *p = getcwd(configuration_file_path, STRING_BUFFER);
	for (p += strlen(p); *p != '/'; --p)
	sprintf(p, "%s", TRAINING_CONFIGURATION_FILE_PATH);

	// - include_path
	size_t len = p - configuration_file_path + 1;
	memcpy(include_path, configuration_file_path, len);
	sprintf(include_path + len, "%s", INCLUDE_PATH);

	return (access(configuration_file_path, F_OK) != -1) ? true : false;
}

static char *get_file_content(const char *path)
{
	// open file
	FILE *f = fopen(path, "r");
	FATAL_ERROR(!f, FE_CONFIG);

	// get length
	FATAL_ERROR(fseek(f, 0, SEEK_END), FE_CONFIG);
	size_t len = (size_t)ftell(f) - 1;
	FATAL_ERROR(fseek(f, 0, SEEK_SET), FE_CONFIG);

	// read out file
	char *file_content = malloc(len + 1);
	FATAL_ERROR(len != fread(file_content, 1, len, f), FE_CONFIG);
	file_content[len] = 0;

	// close file
	fclose(f);

	return file_content;
}

static char *parse_training_script(char c)
{
	static char buffer[STRING_BUFFER];
	sprintf(buffer, "%c", c);

	switch (c) {
	case '"':
		sprintf(buffer, "\\\"");

		break;

	case '\t':
		sprintf(buffer, "\\t");

		break;

	case '\n':
		sprintf(buffer, "\\n");

		break;

	case '\\':
		sprintf(buffer, "\\\\");

		break;

	default:
		break;
	}

	return buffer;
}

static void get_list_of_script_files(void)
{
	// initialise
	char *file_content = get_file_content(include_path);
	n_paths = 0;
	paths = 0;

	// build root
	char root[STRING_BUFFER];
	char *p = getcwd(root, STRING_BUFFER);
	for (p += strlen(p); *p != '/'; --p);
	sprintf(p + 1, "%s", ROOT_ADDENDUM);

	// build script paths
	size_t len_root = strlen(root);
	for (char *p = strtok(file_content, "\n"); p; p = strtok(0, "\n")) {
		// skip the line if it is a comment
		if (p[0] == '#')
			continue;

		// allocate
		++n_paths;
		char* *paths_new = realloc(paths, n_paths  *sizeof(*paths));
		if (paths_new)
			paths = paths_new;
		else
			fprintf(stdout, FE_SCRIPTS);

		// build the path
		size_t len = len_root + strlen(p);
		char *path = malloc(len + 1);
		sprintf(path, "%s%s", root, p);
		paths[n_paths - 1] = path;
	}

	// clean up
	free(file_content);
}

static bool is_layer_type_definition(const char *include_path)
{
	// return, if the include is not a layer type
	if (!strstr(include_path, "/layer_types/"))
		return false;

	// return, if the include is not a layer type definition
	if (strstr(include_path, "/layer_types/python/"))
		return false;

	return true;
}

static void add_layer_definitions(FILE *f)
{
	// add import
	wrt_to_file(f, IMPORT_TF, FE_WRT_SCRIPT);

	for (unsigned int i = 0; i < n_paths; ++i) {
		if (!is_layer_type_definition(paths[i]))
			continue;

		// get script content
		char *file_content = get_file_content(paths[i]);

		// write the content to the combined script
		for (char *p = file_content; *p; ++p) {
			char *writing_sequence = parse_training_script(*p);
			wrt_to_file(f, writing_sequence, FE_WRT_SCRIPT);
		}

		// add newline characters
		wrt_to_file(f, NEWLINES, FE_WRT_SCRIPT);

		// clean up
		free(file_content);
	}
}

static void build_layer_init_list(FILE *f)
{
	// initialise
	wrt_to_file(f, LAYER_DICT_HEADER, FE_DICT);

	for (unsigned int i = 0; i < n_paths; ++i) {
		// return, if it is not a layer type definition
		if (!is_layer_type_definition(paths[i]))
			continue;

		// extract the layer type, construct the dictionary entry
		char *p = strstr(paths[i], "/layer_types/") + 13;
		char *q = strstr(p, "/");
		size_t len = q - p;
		char buffer[STRING_BUFFER];
		memcpy(buffer, p, len);
		buffer[len] = 0;

		// construct the dictionary entry
		char entry[STRING_BUFFER_LONG];
		sprintf(entry, "\\n\\t'%s': %s,", buffer, buffer);
		wrt_to_file(f, entry, FE_DICT);
	}

	// finish
	wrt_to_file(f, LAYER_DICT_FOOTER, FE_DICT);
}

static void add_scripts(FILE *f)
{
	for (unsigned int i = 0; i < n_paths; ++i) {
		if (is_layer_type_definition(paths[i]))
			continue;

		// get script content
		char *file_content = get_file_content(paths[i]);

		// write the content to the combined script
		for (char *p = file_content; *p; ++p) {
			char *writing_sequence = parse_training_script(*p);
			wrt_to_file(f, writing_sequence, FE_WRT_SCRIPT);
		}

		// add newline characters
		wrt_to_file(f, NEWLINES, FE_WRT_SCRIPT);

		// clean up
		free(file_content);
	}
}

static void build_training_script(void)
{
	// initialise
	char f_path[STRING_BUFFER];
	char *p = getcwd(f_path, STRING_BUFFER);
	for (p += strlen(p); *p != '/'; --p);
	sprintf(p + 1, "%s", SCRIPT_HEADER_FILE_PATH);
	FILE *f = fopen(f_path, "w");

	// write the script
	wrt_to_file(f, TRAINING_HEADER, FE_WRT_SCRIPT);
	add_layer_definitions(f);
	build_layer_init_list(f);
	add_scripts(f);
	wrt_to_file(f, TRAINING_FOOTER, FE_WRT_SCRIPT);

	// clean up
	fclose(f);
}

static void free_script_file_memory(void)
{
	for (unsigned int i = 0; i < n_paths; ++i)
		free(paths[i]);

	free(paths);
}


// functions
void prepare_training_script(void)
{
	// if training is not enabled, return
	if (!is_training_enabled())
		return;

	// write training_script header
	get_list_of_script_files();
	build_training_script();

	// clean up
	free_script_file_memory();
}
