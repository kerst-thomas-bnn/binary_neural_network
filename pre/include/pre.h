#ifndef PRE_H
#define PRE_H


// functions
void prepare_layer_information(void);

void prepare_training_header(void);

void prepare_training_script(void);


#endif
