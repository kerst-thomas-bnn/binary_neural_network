#ifndef MACROS_H
#define MACROS_H


// includes
#include <stdio.h>
#include <stdlib.h>


// macros
#define FATAL_ERROR(CONDITION, MESSAGE)  \
	do {  \
		if ((CONDITION)) {  \
			fprintf(stdout, "[PRE ERROR] %s. Aborting library configuration.\n", (MESSAGE));  \
			exit(1);  \
		}  \
	} while(0)


#endif
