#ifndef STRINGS_H
#define STRINGS_H


// constants
#define PATH_LAYER_TYPES "../src/core/layer/layer_types/"
#define PATH_MODEL_HEADER "../src/core/model/c_api/intern/include/layer_types.h"
#define PATH_LAYER_INIT_HEADER "../src/core/layer/init.h"

#define FE_HEADER "Unable to write to the header file cataloguing the different layer types"
#define FE_LAYER "Unable to scan layer types"
#define FE_DIR "Unable to read the source files directory containing the different layer types"
#define FE_CONFIG "Unable to read out the training configuration"
#define FE_WRITE "Unable to write the training header file"
#define FE_SCRIPTS "Unable to read out list of scripts"
#define FE_WRT_SCRIPT "Unable to write the training script"
#define FE_DICT "Unable to write the training script layer dictionary"

#define PREAMBLE_MODEL "#ifndef MODEL_LAYER_TYPES_H\n#define MODEL_LAYER_TYPES_H\n\n\n/* THIS FILE HAS BEEN CREATED AUTOMATICALLY */\n\n\n// include \n#include \"../../../../layer/init.h\"\n\n\n// forward declarations\nstruct layer_t;\nstruct tensor_t;\n\n\n// structs\nstruct layer_init_identifier_t {\n\tconst char* abbreviation;\n\tstruct layer_t* (*function)(const struct tensor_t*, const struct tensor_t*, unsigned int, unsigned char**);\n};\n\n\n// static variables\nstatic const struct layer_init_identifier_t ids[] = {"
#define PREAMBLE_LAYER "#ifndef LAYER_INIT_H\n#define LAYER_INIT_H\n\n\n/* THIS FILE HAS BEEN CREATED AUTOMATICALLY */\n\n\n// forward declarations\nstruct layer_t;\nstruct tensor_t;\n\n\n// constructors\n"

#define WRT_LAYER_TYPE "struct layer_t* %s_init(const struct tensor_t*, const struct tensor_t*, unsigned int, unsigned char**);\n\n"

#define TRAINING_CONFIGURATION_FILE_PATH "configure/train/config"
#define HEADER_FILE_PATH "src/core/train/include/python.h"

#define HEADER_START "#ifndef PYTHON_INCLUDE_H\n#define PYTHON_INCLUDE_H\n\n\n/* THIS FILE HAS BEEN CREATED AUTOMATICALLY */\n\n\n// includes\n#include \""
#define HEADER_END "\"\n\n\n#endif"

#define TRAINING_CONFIGURATION_FILE_PATH "configure/train/config"
#define INCLUDE_PATH "src/core/train/include/include"
#define ROOT_ADDENDUM "src/core/"
#define SCRIPT_HEADER_FILE_PATH "src/core/train/include/training_script.h"

#define IMPORT_TF "import tensorflow as tf\\nfrom tensorflow.keras import layers\\nimport numpy as np\\n\\n"
#define NEWLINES "\\n\\n\\n"
#define LAYER_DICT_HEADER "# layers dictionary\\nLAYERS_DICT = {"
#define LAYER_DICT_FOOTER "\\n}\\n\\n\\n"
#define TRAINING_HEADER "#ifndef TRAINING_SCRIPT_H\n#define TRAINING_SCRIPT_H\n\n\n/* This file has been generated automatically */\n\n\n// constants\n#define TRAINING_SCRIPT \""
#define TRAINING_FOOTER "\"\n\n\n#endif\n"


#endif
