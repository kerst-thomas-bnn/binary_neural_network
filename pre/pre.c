#include "include/pre.h"


// main
int main(int argc, char const *argv[])
{
	prepare_layer_information();
	prepare_training_header();
	prepare_training_script();

	return 0;
}
