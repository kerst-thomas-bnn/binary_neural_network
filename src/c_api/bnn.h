#ifndef BNN_H
#define BNN_H


// ----------------------------------------------------------------------------
// --- general ---
// ----------------------------------------------------------------------------

void bnn_hello(void);
// prints a test message to verify the successful installation of libbnn

const char *bnn_version(void);
// retrieves libbnn version information
// returns:
// 	(const char *) string literal containing the version information


// ----------------------------------------------------------------------------
// --- setup ---
// ----------------------------------------------------------------------------

void bnn_setup_input_shape(unsigned int rank, ...);
// initialises the input shape
// accepts:
// 	(unsigned int rank) input rank
// 	(__VA_ARGS__ unsigned int) dimensions

void bnn_setup_output_shape(unsigned int rank, ...);
// initialises the output shape
// accepts:
// 	(unsigned int rank) output rank
// 	(__VA_ARGS__ unsigned int) dimensions

void bnn_easy_cleanup(void);
// simple cleanup procedure, freeing all allocated memory


// ----------------------------------------------------------------------------
// --- data ---
// ----------------------------------------------------------------------------

void bnn_data_fetch_local(const char *path);
// copies the locally stored data into memory
// accepts:
// 	(const char *path) relative path to the data file

void bnn_data_release(void);
// releases data stored in memory

unsigned int bnn_data_quantity(void);
// returns the number of items stored in memory
// returns:
// 	(unsigned int) number of items stored in memory

void bnn_data_display(unsigned int n);
// prints the tensor information of a specific item
// inputs:
// 	(unsigned int n) number of item to be displayed


// ----------------------------------------------------------------------------
// --- model ---
// ----------------------------------------------------------------------------

void bnn_model_load(const char *path_model, const char *path_weights);
// builds an internal representation of the model using locally stored model
// and weight files. If 0 is passed to the weights file, all weights be
// initialised to 0.
// accepts:
// 	(const char *path_model) relative path of the model file
// 	(const char *path_weights) relative path to the weights file

void bnn_model_release(void);
// releases the internal representation of the model, freeing allocted memory

void bnn_model_push_local(void);
// pushes all locally stored data through the network, producing a prediction


// ----------------------------------------------------------------------------
// --- train ---
// ----------------------------------------------------------------------------

void bnn_train_run(const char *path_train, const char *path_test);
// trains the model, using locally stored training and test data
// accepts:
// 	(const char *path_train) relative path to the training data file
// 	(const char *path_test) relative path to the test data file


#endif
