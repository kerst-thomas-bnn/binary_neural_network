#ifndef ACTIVATIONS_H
#define ACTIVATIONS_H


// forward declarations
struct output_t;


// functions
void activate_threshold(struct output_t *output, unsigned int n_core);

void activate_one_hot(struct output_t *output, unsigned int n_core);

void activate_maximum(struct output_t *output, unsigned int n_core);

void activate_average(struct output_t *output, unsigned int n_core);


#endif
