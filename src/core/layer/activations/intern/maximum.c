#include "../../../bits/bits.h"
#include "../../def/output/output.h"
#include "../activations.h"


// functions
void activate_maximum(struct output_t *output, unsigned int n_core)
{
	unsigned int n_bits = output_pooling_n_bits_get(output);
	unsigned char *bits = output_pooling_bits_get(output);
	struct bits_t *tar = output_get_prediction_bits(output);

	for (unsigned int i = 0; i < n_bits; ++i) {
		unsigned char byte = bits[i >> 3];
		unsigned char mask = 128 >> i % 8;
		unsigned char val = (byte & mask) ? 1 : 0;
		if (val) {
			bits_flip_up(tar, output_bit_pos_get(output, n_core));

			output_bit_pos_increment(output, n_core);

			return;
		}
	}

	output_bit_pos_increment(output, n_core);
}
