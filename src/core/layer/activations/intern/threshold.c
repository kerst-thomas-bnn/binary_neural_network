#include "../../../include/debug.h"
#include "../../../bits/bits.h"
#include "../../def/output/output.h"
#include "../activations.h"

#include <stdint.h>


// static functions
static inline unsigned int get_threshold(struct output_t *out, unsigned int n)
{
	unsigned int t = output_threshold_get(out, n);
	t += output_threshold_offset_get(out, n);

	return t;
}


// functions
void activate_threshold(struct output_t *output, unsigned int n_core)
{
	struct bits_t *tar = output_get_prediction_bits(output);
	unsigned int threshold_adjusted = get_threshold(output, n_core);

	if (output_internal_field_get(output, n_core) <= threshold_adjusted)
		bits_flip_up(tar, output_bit_pos_get(output, n_core));

	output_bit_pos_increment(output, n_core);
}
