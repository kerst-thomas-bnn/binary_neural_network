#include "../../../bits/bits.h"
#include "../../def/output/output.h"
#include "../activations.h"


// static functions
static void set_bit(struct bits_t *bits,
                    const unsigned int *accumulation,
                    unsigned int n)
{
	unsigned int min = (unsigned int)(-1);
	unsigned int min_pos = 0;

	for (unsigned int i = 0; i < n; ++i)
		if (accumulation[i] < min) {
			min = accumulation[i];
			min_pos = i;
		}

	bits_flip_up(bits, min_pos);
}


// functions
void activate_one_hot(struct output_t *output, unsigned int n_core)
{
	struct bits_t *tar = output_get_prediction_bits(output);
	unsigned int pos = output_bit_pos_get(output, n_core);

	// store value
	unsigned int *one_hot_mem = output_get_one_hot_mem(output);
	one_hot_mem[pos] = output_internal_field_get(output, n_core);
	one_hot_mem[pos] -= output_threshold_get(output, n_core);

	// set bit and end accumulation if pos = pos_max - 1
	unsigned int n_bits = bits_get_n_bits(tar);
	if (pos == n_bits - 1)
		set_bit(tar, one_hot_mem, n_bits);

	output_bit_pos_increment(output, n_core);
}
