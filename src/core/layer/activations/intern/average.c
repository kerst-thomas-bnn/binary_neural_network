#include "../../../bits/bits.h"
#include "../../def/output/output.h"
#include "../activations.h"


// functions
void activate_average(struct output_t *output, unsigned int n_core)
{
	unsigned int n_bits = output_pooling_n_bits_get(output);
	unsigned char *bits = output_pooling_bits_get(output);
	struct bits_t *tar = output_get_prediction_bits(output);

	unsigned int n_ones = 0;
	for (unsigned int i = 0; i < n_bits; ++i) {
		unsigned char byte = bits[i / 8];
		unsigned char mask = 128 >> i % 8;
		unsigned char val = (byte & mask) ? 1 : 0;
		if (val)
			++n_ones;
	}

	if (n_ones >= n_bits / 2 + n_bits % 2)
		bits_flip_up(tar, output_bit_pos_get(output, n_core));

	output_bit_pos_increment(output, n_core);
}
