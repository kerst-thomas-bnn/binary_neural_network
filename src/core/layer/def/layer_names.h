#ifndef LAYER_NAMES_H
#define LAYER_NAMES_H


// names
static char *layer_names[] = {
	"none",
	"flatten",
	"convolution",
	"maximum_pooling",
	"average_pooling",
	"xnor_pooling",
	"inception version 1",
	"knockout",
	"bitwise",
	"accumulation",
	"shift",
	"stacked_xnor"
};


#endif
