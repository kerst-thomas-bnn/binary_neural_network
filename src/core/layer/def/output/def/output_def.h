#ifndef OUTPUT_DEF_H
#define OUTPUT_DEF_H


// constants
#define N_MAX_CORES 8


// forward declarations
struct tensor_t;


// structs
struct output_t {
	// the actual output tensors
	struct tensor_t* prediction;

	// helper variables to activate
	// - activate
	unsigned int n_used_cores;
	unsigned int start[N_MAX_CORES];
	unsigned int stop[N_MAX_CORES];
	unsigned int internal_field[N_MAX_CORES];
	unsigned int bit_pos[N_MAX_CORES];

	// - threshold
	unsigned int threshold[N_MAX_CORES];
	unsigned int threshold_offset[N_MAX_CORES];

	// - one hot
	unsigned int* one_hot_mem;

	// - pooling
	unsigned int pooling_n_bits;
	unsigned char* pooling_bits;
};


#endif
