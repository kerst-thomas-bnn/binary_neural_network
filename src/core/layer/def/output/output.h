#ifndef OUTPUT_H
#define OUTPUT_H


// forward declarations
struct output_t;
struct shape_t;
struct tensor_t;
struct bits_t;


// functions
// - constructor and destructor
struct output_t* output_create(const struct shape_t*);

void output_destroy(struct output_t*);

// - member access
struct tensor_t* output_get_prediction(const struct output_t*);

unsigned int output_get_prediction_n_bytes(const struct output_t*);

unsigned int output_get_prediction_n_bits(const struct output_t*);

unsigned char* output_get_prediction_bytes(const struct output_t*);

struct bits_t* output_get_prediction_bits(const struct output_t*);

struct bits_t** output_get_prediction_bitsp(const struct output_t*);

unsigned int output_get_prediction_rank(const struct output_t*);

unsigned int* output_get_prediction_dims(const struct output_t*);

unsigned int* output_get_one_hot_mem(const struct output_t*);

unsigned int** output_get_one_hot_memp(struct output_t*);

// - activate
void output_start_set(struct output_t*, unsigned int, unsigned int);

unsigned int output_start_get(const struct output_t*, unsigned int);

void output_stop_set(struct output_t*, unsigned int, unsigned int);

unsigned int output_stop_get(const struct output_t*, unsigned int);

void output_reset(struct output_t*);

void output_internal_field_set(struct output_t*, unsigned int, unsigned int);

unsigned int output_internal_field_get(const struct output_t*, unsigned int);

unsigned int output_bit_pos_get(const struct output_t*, unsigned int);

void output_bit_pos_increment(struct output_t*, unsigned int);

// - threshold
void output_threshold_set(struct output_t*, unsigned int, unsigned int);

unsigned int output_threshold_get(const struct output_t*, unsigned int);

void output_threshold_offset_set(struct output_t*,
                                 unsigned int,
                                 const struct tensor_t*);

unsigned int output_threshold_offset_get(const struct output_t*, unsigned int);

// - pooling
void output_pooling_n_bits_set(struct output_t*, unsigned int);

unsigned int output_pooling_n_bits_get(const struct output_t*);

void output_pooling_bits_set(struct output_t*, unsigned char*);

unsigned char* output_pooling_bits_get(const struct output_t*);

// - cores
unsigned int output_n_max_cores_get(void);

void output_n_used_cores_set(struct output_t*, unsigned int);

unsigned int output_n_used_cores_get(const struct output_t*);


#endif
