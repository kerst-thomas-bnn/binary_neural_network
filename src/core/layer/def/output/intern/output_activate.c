#include "../../../../tensor/tensor.h"
#include "../output.h"
#include "../def/output_def.h"

#include <string.h>
#include <stdio.h>


// functions
void output_start_set(struct output_t* p, unsigned int n, unsigned int val)
{
	p->start[n] = val;
}

unsigned int output_start_get(const struct output_t* p, unsigned int n)
{
	return p->start[n];
}

void output_stop_set(struct output_t* p, unsigned int n, unsigned int val)
{
	p->stop[n] = val;
}

unsigned int output_stop_get(const struct output_t* p, unsigned int n)
{
	return p->stop[n];
}

void output_reset(struct output_t* p)
{
	struct tensor_t* pred = p->prediction;
	memset(tensor_get_bytes(pred), 0, tensor_get_n_bytes(pred));

	// single core
	if (p->n_used_cores == 1) {
		p->bit_pos[0] = 0;

		return;
	}

	// multi core
	memcpy(p->bit_pos, p->start, p->n_used_cores * sizeof(*p->bit_pos));
}

void output_internal_field_set(struct output_t* p,
                               unsigned int n,
                               unsigned int i)
{
	p->internal_field[n] = i;
}

unsigned int output_internal_field_get(const struct output_t* p,
                                       unsigned int n)
{
	return p->internal_field[n];
}

unsigned int output_bit_pos_get(const struct output_t* p, unsigned int n)
{
	return p->bit_pos[n];
}

void output_bit_pos_increment(struct output_t* p, unsigned int n)
{
	++p->bit_pos[n];
}
