#include "../output.h"
#include "../def/output_def.h"



// functions
void output_pooling_n_bits_set(struct output_t* p, unsigned int n)
{
	p->pooling_n_bits = n;
}

unsigned int output_pooling_n_bits_get(const struct output_t* p)
{
	return p->pooling_n_bits;
}

void output_pooling_bits_set(struct output_t* p, unsigned char* bits)
{
	p->pooling_bits = bits;
}

unsigned char* output_pooling_bits_get(const struct output_t* p)
{
	return p->pooling_bits;
}
