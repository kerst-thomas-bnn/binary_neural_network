#include "../../../../tensor/tensor.h"
#include "../output.h"
#include "../def/output_def.h"



// functions
void output_threshold_set(struct output_t* p, unsigned int n, unsigned int i)
{
	p->threshold[n] = i;
}

unsigned int output_threshold_get(const struct output_t* p, unsigned int n)
{
	return p->threshold[n];
}

void output_threshold_offset_set(struct output_t* p,
                                 unsigned int n,
                                 const struct tensor_t* t)
{
	unsigned int n_bits = tensor_get_n_bits(t);
	p->threshold_offset[n] = n_bits / 2 + n_bits % 2;
}

unsigned int output_threshold_offset_get(const struct output_t* p,
                                         unsigned int n)
{
	return p->threshold_offset[n];
}
