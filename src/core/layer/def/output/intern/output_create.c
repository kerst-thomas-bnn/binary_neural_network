#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../shape/shape.h"
#include "../output.h"
#include "../def/output_def.h"

#include <stdlib.h>


// functions
struct output_t* output_create(const struct shape_t* s)
{
	struct output_t* p = SALLOC(p);

	p->prediction = tensor_create(shape_get_rank(s), shape_get_dims(s), 0);
	p->n_used_cores = 1;
	p->one_hot_mem = 0;

	return p;
};
