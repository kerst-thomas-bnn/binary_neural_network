#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../output.h"
#include "../def/output_def.h"

#include <stdlib.h>


// functions
void output_destroy(struct output_t* p)
{
	tensor_destroy(p->prediction);
	FREE(p->one_hot_mem);
};
