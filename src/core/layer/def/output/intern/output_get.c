#include "../../../../tensor/tensor.h"
#include "../output.h"
#include "../def/output_def.h"


// functions
struct tensor_t* output_get_prediction(const struct output_t* p)
{
	return p->prediction;
}

unsigned int output_get_prediction_n_bytes(const struct output_t* p)
{
	return tensor_get_n_bytes(p->prediction);
}

unsigned int output_get_prediction_n_bits(const struct output_t* p)
{
	return tensor_get_n_bits(p->prediction);
}

unsigned char* output_get_prediction_bytes(const struct output_t* p)
{
	return tensor_get_bytes(p->prediction);
}

struct bits_t* output_get_prediction_bits(const struct output_t* p)
{
	return tensor_get_bits(p->prediction);
}

struct bits_t** output_get_prediction_bitsp(const struct output_t* p)
{
	return tensor_get_bitsp(p->prediction);
}

unsigned int output_get_prediction_rank(const struct output_t* p)
{
	return tensor_get_rank(p->prediction);
}

unsigned int* output_get_prediction_dims(const struct output_t* p)
{
	return tensor_get_dims(p->prediction);
}

unsigned int* output_get_one_hot_mem(const struct output_t* p)
{
	return p->one_hot_mem;
}

unsigned int** output_get_one_hot_memp(struct output_t* p)
{
	return &p->one_hot_mem;
}
