#include "../output.h"
#include "../def/output_def.h"


// functions
unsigned int output_n_max_cores_get()
{
	return N_MAX_CORES;
}

void output_n_used_cores_set(struct output_t* p, unsigned int n)
{
	p->n_used_cores = n;
}

unsigned int output_n_used_cores_get(const struct output_t* p)
{
	return p->n_used_cores;
}
