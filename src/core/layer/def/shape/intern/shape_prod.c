#include "../../../../include/debug.h"
#include "../../../../include/array.h"
#include "../shape.h"
#include "../def/shape_def.h"


// functions
unsigned int shape_prod(const struct shape_t *shape)
{
	return array_product_ui(shape->dims, shape->rank);
}
