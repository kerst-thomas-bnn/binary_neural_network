#include "../../../../include/memory.h"
#include "../shape.h"
#include "../def/shape_def.h"

#include <stdlib.h>
#include <string.h>


// functions
struct shape_t *shape_create(unsigned int rank, const unsigned int *dims)
{
	// allocate
	struct shape_t *p = SALLOC(p);

	// rank
	p->rank = rank;

	// dims
	MALLOC(p->dims, p->rank);
	memcpy(p->dims, dims, p->rank  *sizeof(*p->dims));

	return p;
}
