#include "../shape.h"
#include "../def/shape_def.h"


// functions
unsigned int shape_get_rank(const struct shape_t *shape)
{
	return shape->rank;
}

unsigned int *shape_get_dims(const struct shape_t *shape)
{
	return shape->dims;
}
