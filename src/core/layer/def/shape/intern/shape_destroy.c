#include "../../../../include/memory.h"
#include "../shape.h"
#include "../def/shape_def.h"

#include <stdlib.h>


// functions
void shape_destroy(struct shape_t *shape)
{
	FREE(shape->dims);
	FREE(shape);
}
