#ifndef SHAPE_H
#define SHAPE_H


// forward declarations
struct shape_t;


// functions
// - constructor and destructor
struct shape_t *shape_create(unsigned int rank, const unsigned int *dims);

void shape_destroy(struct shape_t *shape);

// - member access
unsigned int shape_get_rank(const struct shape_t *shape);

unsigned int *shape_get_dims(const struct shape_t *shape);

// - other functions
unsigned int shape_prod(const struct shape_t *shape);


#endif
