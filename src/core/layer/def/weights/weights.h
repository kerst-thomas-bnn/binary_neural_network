#ifndef WEIGHTS_H
#define WEIGHTS_H


// forward declarations
struct weights_t;
struct shape_t;
struct tensor_t;
struct bits_t;
struct layer_t;


// structs
// - constructor and destructor
void weights_assign(struct layer_t *layer, unsigned char **weights);

struct weights_t *weights_create(const struct shape_t *kernel,
                                 const struct shape_t *output,
                                 unsigned char **weights_unformatted);

struct weights_t *weights_create_naive(const struct shape_t *output,
                                       unsigned char **weights_unformatted);

struct weights_t *weights_create_bitwise(const struct shape_t *input,
                                         unsigned char **weights_unformatted);

struct weights_t *weights_create_accumulation(const struct shape_t *input,
                                              const struct shape_t *output,
                                              unsigned char **w_unformatted);

void weights_destroy(struct weights_t *p);

// - member access
// - - bit counting
unsigned char *weights_get_raw(const struct weights_t *p);

unsigned int *weights_get_thresholds(const struct weights_t *p);

unsigned int weights_get_n_filters(const struct weights_t *p);

unsigned int weights_get_filter_n_bytes(const struct weights_t *p);

struct tensor_t *weights_get_filter_weights(const struct weights_t *p,
                                            unsigned int k);

struct bits_t *weights_get_filter_bits(const struct weights_t *p,
                                       unsigned int k);

unsigned int weights_get_filter_threshold(const struct weights_t *p,
                                          unsigned int k);

// - - bitwise
unsigned char *weights_get_and(const struct weights_t *p);

unsigned char *weights_get_or(const struct weights_t *p);

unsigned char *weights_get_xor(const struct weights_t *p);

// - - accumulation
unsigned int weights_get_n_accumulations(const struct weights_t *p);

unsigned char **weights_get_xor_accumulation(const struct weights_t *p);

unsigned int *weights_get_biases_accumulation_int(const struct weights_t *p);

unsigned int *weights_get_biases_accumulation_uint(const struct weights_t *p);


#endif
