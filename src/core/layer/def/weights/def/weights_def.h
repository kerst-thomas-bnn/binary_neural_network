#ifndef WEIGHTS_DEF_H
#define WEIGHTS_DEF_H


// forward declarations
struct shape_t;
struct tensor_t;


// structs
struct filter_t {
	struct shape_t *shape;
	struct tensor_t *coeffs;
	unsigned int threshold;
};

struct weights_t {
	// bit counting
	struct tensor_t *weights_formatted;
	unsigned int *biases;
	unsigned int n_filters;
	struct filter_t *filters;

	// bitwise
	unsigned char *and;
	unsigned char *or;
	unsigned char *xor;

	// accumulation
	unsigned int n_accumulations;
	unsigned char **xor_accumulation;
	unsigned int *biases_accumulation_int;
	unsigned int *biases_accumulation_uint;
};


#endif
