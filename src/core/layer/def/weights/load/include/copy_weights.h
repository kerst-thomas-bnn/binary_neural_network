#ifndef WEIGHTS_LOAD_COPY_WEIGHTS_H
#define WEIGHTS_LOAD_COPY_WEIGHTS_H


// include
#include "../../../../../include/memory.h"

#include <string.h>


// static inline
static inline void copy_weights(unsigned char **tar,
                                unsigned int n,
                                unsigned char **weights)
{
	// initialise
	unsigned char *buffer;
	MALLOC(buffer, n);

	// copy
	if (weights) {
		memcpy(buffer, (*weights), n);
		(*weights) += n;
	} else {
		memset(buffer, 0, n);
	}

	// link
	*tar = buffer;
}


#endif
