#include "../../../../../include/debug.h"
#include "../../../../../include/memory.h"
#include "../../../../../include/bitwise.h"
#include "../../../shape/shape.h"
#include "../../../layer_def.h"
#include "../../def/weights_def.h"
#include "../load.h"
#include "../include/copy_weights.h"



// functions
void wghts_assgn_stacked_xnor(struct layer_t *layer, unsigned char **weights)
{
	// initialise
	struct weights_t *p = SALLOC(p);

	// copy
	unsigned int n = DIV8(shape_prod(layer->shape_output));
	copy_weights(&p->xor, n, weights);
	copy_weights(&p->and, n, weights);

	layer->weights = p;
}
