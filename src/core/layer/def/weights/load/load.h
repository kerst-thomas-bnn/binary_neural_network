#ifndef WEIGHTS_LOAD_H
#define WEIGHTS_LOAD_H


// forward declarations
struct layer_t;


// functions
void wghts_assgn_stacked_xnor(struct layer_t *layer, unsigned char **weights);


#endif
