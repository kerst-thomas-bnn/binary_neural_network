#include "../../../../include/debug.h"
#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../shape/shape.h"
#include "../weights.h"
#include "../def/weights_def.h"

#include <stdlib.h>
#include <stdint.h>
#include <string.h>


// static functions
static void copy_weights(unsigned char *weights_stored,
                         unsigned int n_bytes,
                         unsigned char **weights_unformatted)
{
	// copy
	if (weights_unformatted) {
		memcpy(weights_stored, (*weights_unformatted), n_bytes);
		(*weights_unformatted) += n_bytes;
	} else {
		memset(weights_stored, 0, n_bytes);
	}
}

static void load_weights(struct weights_t *p,
                         const struct shape_t *kernel,
                         unsigned char **weights_unformatted)
{
	// initialise
	unsigned int rank = shape_get_rank(kernel);
	unsigned int *dims = shape_get_dims(kernel);
	p->weights_formatted = tensor_create(rank, dims, 0);

	// copy
	unsigned char *weights_stored = tensor_get_bytes(p->weights_formatted);
	unsigned int n_bytes = tensor_get_n_bytes(p->weights_formatted);
	copy_weights(weights_stored, n_bytes, weights_unformatted);
}

static void copy_biases(uint32_t *biases, size_t n, unsigned char **weights)
{
	if (weights) {
		memcpy(biases, (*weights), n);
		(*weights) += n;
	} else {
		memset(biases, 0, n);
	}
}

static void load_biases(struct weights_t *p,
                        const struct shape_t *output,
                        unsigned char **weights_unformatted)
{
	// initialise
	unsigned int rank = shape_get_rank(output);
	unsigned int *dims = shape_get_dims(output);

	unsigned int n_biases = dims[rank - 1];
	MALLOC(p->biases, n_biases);
	uint32_t buffer;
	for (unsigned int i = 0; i < n_biases; ++i) {
		copy_biases(&buffer, 4, weights_unformatted);
		p->biases[i] = (unsigned int)buffer;
	}
}

static void construct_filters(struct weights_t *p, const struct shape_t *krnl)
{
	unsigned int rank_k = shape_get_rank(krnl);
	unsigned int *dims_k = shape_get_dims(krnl);

	unsigned int rank = rank_k - 1;
	unsigned int dims[rank];
	memcpy(dims, dims_k, rank  *sizeof(*dims));

	// set up limits
	unsigned int limits[2  *(rank + 1)];
	for (unsigned int i = 0; i < rank; ++i) {
		limits[2  *i] = 0;
		limits[2  *i + 1] = dims[i];
	}

	// initialise filters
	p->n_filters = dims_k[rank_k - 1];
	MALLOC(p->filters, p->n_filters);

	// fill in the contents
	for (unsigned int i = 0; i < p->n_filters; ++i) {
		struct filter_t *f = &p->filters[i];

		// - shape
		f->shape = shape_create(rank, dims);

		// - coeffs
		limits[2  *rank] = i;
		limits[2  *rank + 1] = i + 1;
		f->coeffs = tensor_slice(p->weights_formatted, limits, 0);

		// - threshold
		f->threshold = p->biases[i];
	}
}


// functions
struct weights_t *weights_create(const struct shape_t *kernel,
                                 const struct shape_t *output,
                                 unsigned char **weights_unformatted)
{
	// allocate
	struct weights_t *p = SALLOC(p);

	// copy weights and biases
	load_weights(p, kernel, weights_unformatted);
	load_biases(p, output, weights_unformatted);
	construct_filters(p, kernel);

	return p;
}
