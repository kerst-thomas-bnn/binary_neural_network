#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../shape/shape.h"
#include "../weights.h"
#include "../def/weights_def.h"

#include <stdlib.h>


// functions
void weights_destroy(struct weights_t *p)
{
	// formatted weights and biases
	tensor_destroy(p->weights_formatted);
	FREE(p->biases);

	// filters
	for (unsigned int i = 0; i < p->n_filters; ++i) {
		struct filter_t *f = &p->filters[i];
		shape_destroy(f->shape);
		tensor_destroy(f->coeffs);
	}
	FREE(p->filters);

	// bitwise
	FREE(p->and);
	FREE(p->or);
	FREE(p->xor);

	// accumulation
	for (unsigned int i = 0; i < p->n_accumulations; ++i)
		FREE(p->xor_accumulation[i]);

	FREE(p->xor_accumulation);
	FREE(p->biases_accumulation_int);
	FREE(p->biases_accumulation_uint);

	// weights
	FREE(p);
}
