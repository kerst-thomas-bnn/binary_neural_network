#include "../../../../include/debug.h"
#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../shape/shape.h"
#include "../weights.h"
#include "../def/weights_def.h"

#include <stdlib.h>
#include <string.h>


// static functions
static void copy_weights(unsigned char *weights_stored,
                         unsigned int n_bytes,
                         unsigned char **weights_unformatted)
{
	// copy
	if (weights_unformatted) {
		memcpy(weights_stored, (*weights_unformatted), n_bytes);
		(*weights_unformatted) += n_bytes;
	} else {
		memset(weights_stored, 0, n_bytes);
	}
}

static void load_weights(struct weights_t *p,
                         const struct shape_t *output,
                         unsigned char **weights_unformatted)
{
	// initialise
	unsigned int rank = shape_get_rank(output);
	unsigned int *dims = shape_get_dims(output);
	p->weights_formatted = tensor_create(rank, dims, 0);

	// copy
	unsigned char *weights_stored = tensor_get_bytes(p->weights_formatted);
	unsigned int n_bytes = tensor_get_n_bytes(p->weights_formatted);
	copy_weights(weights_stored, n_bytes, weights_unformatted);
}


// functions
struct weights_t *weights_create_naive(const struct shape_t *output,
                                       unsigned char **weights_unformatted)
{
	// allocate
	struct weights_t *p = SALLOC(p);
	load_weights(p, output, weights_unformatted);

	return p;
}
