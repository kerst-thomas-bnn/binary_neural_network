#include "../../../../include/debug.h"
#include "../../../../include/array.h"
#include "../../../../include/memory.h"
#include "../../../../include/bitwise.h"
#include "../../../../tensor/tensor.h"
#include "../../shape/shape.h"
#include "../weights.h"
#include "../def/weights_def.h"

#include <stdlib.h>
#include <string.h>


// static functions
static void copy_weights(unsigned char **target,
                         unsigned int n_bytes,
                         unsigned char **weights_unformatted)
{
	// initialise
	unsigned char *buffer;
	MALLOC(buffer, n_bytes);

	// copy
	if (weights_unformatted) {
		memcpy(buffer, (*weights_unformatted), n_bytes);
		(*weights_unformatted) += n_bytes;
	} else {
		memset(buffer, 0, n_bytes);
	}

	// link
	*target = buffer;
}

static void load_weights(struct weights_t *p,
                         const struct shape_t *input,
                         unsigned char **weights_unformatted)
{
	// determine the number of bytes per and-, or- and xnor-weights
	unsigned int rank = shape_get_rank(input);
	unsigned int *dims = shape_get_dims(input);
	unsigned int n_bytes = array_product_ui(dims, rank);
	n_bytes = DIV8(n_bytes);

	// copy the weights
	copy_weights(&p->and, n_bytes, weights_unformatted);
	copy_weights(&p->or, n_bytes, weights_unformatted);
	copy_weights(&p->xor, n_bytes, weights_unformatted);
}


// functions
struct weights_t *weights_create_bitwise(const struct shape_t *input,
                                         unsigned char **weights_unformatted)
{
	struct weights_t *p = SALLOC(p);
	load_weights(p, input, weights_unformatted);

	return p;
}
