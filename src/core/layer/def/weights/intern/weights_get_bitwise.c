#include "../weights.h"
#include "../def/weights_def.h"


// functions
unsigned char *weights_get_and(const struct weights_t *p)
{
	return p->and;
}

unsigned char *weights_get_or(const struct weights_t *p)
{
	return p->or;
}

unsigned char *weights_get_xor(const struct weights_t *p)
{
	return p->xor;
}
