#include "../../../../include/memory.h"
#include "../../../../include/bitwise.h"
#include "../../../../tensor/tensor.h"
#include "../../shape/shape.h"
#include "../weights.h"
#include "../def/weights_def.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>


// static functions
static void format_weights(unsigned char **p,
                           const struct tensor_t *t,
                           unsigned int rank,
                           const unsigned int *dims)
{
	// initiate limits
	unsigned int limits[2  *rank];
	memset(limits, 0, 2  *rank  *sizeof(*limits));
	for (unsigned int i = 0; i < rank; ++i)
		limits[2  *i + 1] = dims[i];

	// iterate through filters
	for (unsigned int i = 0; i < dims[rank - 1]; ++i) {
		// set limits and get tensor slice
		limits[2  *rank - 2] = i;
		limits[2  *rank - 1] = i + 1;
		struct tensor_t *slice = tensor_slice(t, limits, 0);

		// copy the slice content to the weights
		unsigned int n_bytes = tensor_get_n_bytes(slice);
		MALLOC(p[i], n_bytes);
		memcpy(p[i], tensor_get_bytes(slice), n_bytes);

		// clean up
		tensor_destroy(slice);
	}
}

static void copy_weights(unsigned char *weights_tmp,
                         unsigned int n_bytes,
                         unsigned char **weights_unformatted)
{
	if (weights_unformatted) {
		memcpy(weights_tmp, (*weights_unformatted), n_bytes);
		(*weights_unformatted) += n_bytes;
	} else {
		memset(weights_tmp, 0, n_bytes);
	}
}

static void get_weights(struct weights_t *p,
                        const struct shape_t *input,
                        unsigned char **weights_unformatted)
{
	// create tensor to format weights
	unsigned int rank = shape_get_rank(input) + 1;
	unsigned int dims[rank];
	memcpy(dims, shape_get_dims(input), (rank - 1)  *sizeof(*dims));
	dims[rank - 1] = p->n_accumulations;

	// copy
	// - init
	struct tensor_t *weights_tmp = tensor_create(rank, dims, 0);
	unsigned char *weigts_stored = tensor_get_bytes(weights_tmp);
	unsigned int n_bytes = tensor_get_n_bytes(weights_tmp);

	// - XOR
	copy_weights(weigts_stored, n_bytes, weights_unformatted);
	MALLOC(p->xor_accumulation, p->n_accumulations);
	format_weights(p->xor_accumulation, weights_tmp, rank, dims);

	// clean up
	tensor_destroy(weights_tmp);
}

static void copy_biases(int32_t *biases_stored,
                        size_t n_bytes,
                        unsigned char **weights_unformatted)
{
	if (weights_unformatted) {
		memcpy(biases_stored, (*weights_unformatted), n_bytes);
		(*weights_unformatted) += n_bytes;
	} else {
		memset(biases_stored, 0, n_bytes);
	}
}

static void get_biases(struct weights_t *p, unsigned char **w_unformatted)
{
	// int32
	size_t len = p->n_accumulations  *sizeof(int32_t);
	int32_t biases_int32[p->n_accumulations];
	copy_biases(biases_int32, len, w_unformatted);

	// int
	MALLOC(p->biases_accumulation_int, p->n_accumulations);
	for (unsigned int i = 0; i < p->n_accumulations; ++i)
		p->biases_accumulation_int[i] = (int)biases_int32[i];

	// offset the biases
	int32_t min_val = 0;
	for (unsigned int i = 0; i < p->n_accumulations; ++i)
		if (!(min_val < biases_int32[i]))
			min_val = biases_int32[i];

	// unsigned int
	MALLOC(p->biases_accumulation_uint, p->n_accumulations);
	for (unsigned int i = 0; i < p->n_accumulations; ++i) {
		unsigned int w = (unsigned int)(biases_int32[i] - min_val);
		p->biases_accumulation_uint[i] = w;
	}
}


// functions
struct weights_t *weights_create_accumulation(const struct shape_t *input,
                                              const struct shape_t *output,
                                              unsigned char **w_unformatted)
{
	// allocate
	struct weights_t *p = SALLOC(p);

	// get byte widths
	p->n_accumulations = *shape_get_dims(output);

	// load weight and biases
	get_weights(p, input, w_unformatted);
	get_biases(p, w_unformatted);

	return p;
}
