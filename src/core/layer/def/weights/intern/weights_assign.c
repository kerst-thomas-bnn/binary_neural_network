#include "../../../../include/debug.h"
#include "../../../../include/error.h"
#include "../../../layer.h"
#include "../../layer_def.h"
#include "../weights.h"
#include "../load/load.h"

#include <stdio.h>


// constants
#define ERR_MSG1 "\n[ERROR] While assigning weights to layer \"%s\":\n"
#define ERR_MSG2 "Weight loading not implemented"


// static functions
static void failure(const struct layer_t *layer)
{
	const char *name = layer_get_name(layer);

	fprintf(stdout, ERR_MSG1, name);
	FATAL_ERROR(1, ERR_MSG2);
}


// functions
void weights_assign(struct layer_t *layer, unsigned char **weights)
{
	switch (layer->layer_type) {
	case LAYER_TYPE_STACKED_XNOR:
		wghts_assgn_stacked_xnor(layer, weights);
		break;
	default:
		failure(layer);
		break;
	}
}
