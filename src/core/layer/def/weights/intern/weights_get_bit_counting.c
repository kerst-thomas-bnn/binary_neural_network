#include "../../../../tensor/tensor.h"
#include "../../shape/shape.h"
#include "../weights.h"
#include "../def/weights_def.h"


// functions
unsigned char *weights_get_raw(const struct weights_t *p)
{
	return tensor_get_bytes(p->weights_formatted);
}

unsigned int *weights_get_thresholds(const struct weights_t *p)
{
	return p->biases;
}

unsigned int weights_get_n_filters(const struct weights_t *p)
{
	return p->n_filters;
}

unsigned int weights_get_filter_n_bytes(const struct weights_t *p)
{
	return tensor_get_n_bytes(p->filters[0].coeffs);
}

struct tensor_t *weights_get_filter_weights(const struct weights_t *p,
                                            unsigned int k)
{
	return p->filters[k].coeffs;
}

struct bits_t *weights_get_filter_bits(const struct weights_t *p,
                                       unsigned int k)
{
	return tensor_get_bits(p->filters[k].coeffs);
}

unsigned int weights_get_filter_threshold(const struct weights_t *p,
                                          unsigned int k)
{
	return p->filters[k].threshold;
}
