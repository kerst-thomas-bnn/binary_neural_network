#include "../weights.h"
#include "../def/weights_def.h"


// functions
unsigned int weights_get_n_accumulations(const struct weights_t *p)
{
	return p->n_accumulations;
}

unsigned char **weights_get_xor_accumulation(const struct weights_t *p)
{
	return p->xor_accumulation;
}

unsigned int *weights_get_biases_accumulation_int(const struct weights_t *p)
{
	return p->biases_accumulation_int;
}

unsigned int *weights_get_biases_accumulation_uint(const struct weights_t *p)
{
	return p->biases_accumulation_uint;
}
