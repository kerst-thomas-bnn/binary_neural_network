#include "../../../../include/memory.h"
#include "../../shape/shape.h"
#include "../padding.h"
#include "../def/padding_def.h"

#include <stdlib.h>


// functions
struct padding_t* padding_create(const struct shape_t* input,
                                 const struct shape_t* kernel,
                                 const struct shape_t* output,
                                 unsigned int strides)
{
	unsigned int rank = shape_get_rank(input);
	unsigned int* dims_i = shape_get_dims(input);
	unsigned int* dims_k = shape_get_dims(kernel);
	unsigned int* dims_o = shape_get_dims(output);

	// allocate
	unsigned int n_paddings = rank - 1;
	struct padding_t* p;
	MALLOC(p, n_paddings);

	// fill the paddings
	for (unsigned int i = 0; i < n_paddings; ++i) {
		struct padding_t* t = &p[i];

		unsigned int s = (strides) ? strides : dims_k[i];
		int pad_int = (dims_o[i] - 1) * s + dims_k[i] - dims_i[i];
		unsigned int pad_uint = (pad_int > 0) ? pad_int : 0;
		t->left = pad_uint / 2;
		t->right = pad_uint - t->left;
	}

	return p;
}
