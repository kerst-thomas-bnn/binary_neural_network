#include "../../../../include/memory.h"
#include "../padding.h"
#include "../def/padding_def.h"

#include <stdlib.h>


// functions
void padding_destroy(struct padding_t* p)
{
	FREE(p);
}
