#include "../padding.h"
#include "../def/padding_def.h"


// functions
unsigned int padding_get_left(const struct padding_t* p, unsigned int dim)
{
	return p[dim].left;
}

unsigned int padding_get_right(const struct padding_t* p, unsigned int dim)
{
	return p[dim].right;
}
