#ifndef PADDING_H
#define PADDING_H


// forward declarations
struct shape_t;
struct padding_t;


// functions
// - constructor and destructor
struct padding_t* padding_create(const struct shape_t*,
                                 const struct shape_t*,
                                 const struct shape_t*,
                                 unsigned int);

void padding_destroy(struct padding_t*);

// - member access
unsigned int padding_get_left(const struct padding_t*, unsigned int);

unsigned int padding_get_right(const struct padding_t*, unsigned int);


#endif
