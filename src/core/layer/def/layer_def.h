#ifndef LAYER_DEF_H
#define LAYER_DEF_H


// forward declarations
struct layer_t;
struct shape_t;
struct padding_t;
struct weights_t;
struct output_t;
struct tensor_t;


// enums
enum layer_type_e {
	LAYER_TYPE_NONE,
	LAYER_TYPE_FLATTEN,
	LAYER_TYPE_CONVOLUTION,
	LAYER_TYPE_MAXIMUM_POOLING,
	LAYER_TYPE_AVERAGE_POOLING,
	LAYER_TYPE_XNOR_POOLING,
	LAYER_TYPE_INCEPTION_V1,
	LAYER_TYPE_KNOCKOUT,
	LAYER_TYPE_BITWISE,
	LAYER_TYPE_ACCUMULATION,
	LAYER_TYPE_SHIFT,
	LAYER_TYPE_STACKED_XNOR
};


// structs
struct layer_t {
	// type
	enum layer_type_e layer_type;

	// geometry
	struct shape_t *shape_input;
	struct shape_t *shape_kernel;
	struct shape_t *shape_output;
	unsigned int strides;
	struct padding_t *padding;

	// weights, biases, filters
	struct weights_t *weights;

	// sub-layers
	unsigned int n_sub_layers;
	struct layer_t **sub_layers;

	// output
	struct output_t *output;

	// functions
	void (*push)(const struct layer_t *layer,
	             const struct tensor_t *input);
	void (*destroy)(struct layer_t *layer);
	void (*activate)(struct output_t *output, unsigned int n_core);
};


#endif
