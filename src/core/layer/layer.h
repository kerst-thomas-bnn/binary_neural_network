#ifndef LAYER_H
#define LAYER_H


// forward declarations
struct layer_t;
struct tensor_t;


// functions
// - destructor
void layer_destroy(struct layer_t *layer);


// - members
unsigned int layer_get_input_rank(const struct layer_t *layer);

unsigned int *layer_get_input_dims(const struct layer_t *layer);

const struct tensor_t *layer_get_output(const struct layer_t *layer);

const char *layer_get_name(const struct layer_t *layer);


// - modifiers
void layer_set_activation_one_hot(struct layer_t *layer);

void layer_set_activation_average(struct layer_t *layer);

void layer_set_activation_maximum(struct layer_t *layer);


// - push
const struct tensor_t *layer_push(const struct layer_t *layer,
                                  const struct tensor_t *input);


#endif
