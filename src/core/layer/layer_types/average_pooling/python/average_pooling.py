class average_pooling(layers.Layer):
	def __init__(self, kernel, dropout=False, dropout_rate=0.0):
		super(average_pooling, self).__init__()
		self.kernel = kernel
		self.strides = kernel
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		return

	def activate_fn(self, inputs):
		outputs = inputs + tf.stop_gradient(tf.sign(inputs) - inputs)
		outputs = outputs + tf.stop_gradient(tf.add(outputs, 0.5) - outputs)
		outputs = outputs + tf.stop_gradient(tf.sign(outputs) - outputs)

		return outputs

	def apply_dropout(self, inputs):
		if self.dropout:
			return tf.nn.dropout(inputs, rate=self.dropout_rate)

		return inputs

	def save(self, f):
		return

	def call(self, inputs):
		# functions
		def apply_padding():
			import math

			# shapes
			shape_input = inputs.shape[1:-1]
			shape_kernel = self.kernel
			shape_output = [
				math.ceil(float(_i) / float(_k))
				for _i, _k
				in zip(shape_input, shape_kernel)
			]

			# pads
			pads_total = [
				max(_o * _k - _i, 0)
				for _i, _k, _o
				in zip(shape_input, shape_kernel, shape_output)
			]
			pads_left = [int(_t / 2) for _t in pads_total]
			pads_right = [_t - _l for _t, _l in zip(pads_total, pads_left)]
			pads_paired = [[_l, _r] for _l, _r in zip(pads_left, pads_right)]

			# apply padding
			paddings = tf.constant([[0, 0], *pads_paired, [0, 0]])
			padded = tf.pad(inputs, paddings, "CONSTANT", constant_values=-1.0)

			return padded


		# main
		input_padded = apply_padding()
		outputs = tf.nn.avg_pool(
			input=input_padded,
			ksize=self.kernel,
			strides=self.strides,
			padding="VALID"
		)
		outputs = self.activate_fn(outputs)

		if self.train:
			outputs = self.apply_dropout(outputs)

		return outputs
