class flattening(layers.Layer):
	def __init__(self):
		super(flattening, self).__init__()
		self.dropout = False
		self.dropout_rate = 0.0
		self.train = False

		return

	def call(self, inputs):
		batch_size = inputs.shape[0]
		rank = inputs.shape.__len__()
		units = np.prod(inputs.shape[1:])

		kernel = [1] * rank
		kernel[0] = batch_size
		kernel[-1] = units

		return tf.reshape(inputs, shape=kernel)

	def clip_weights(self):
		return

	def save(self, f):
		return
