#ifndef FLATTENING_H
#define FLATTENING_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t* _flattening_init(const struct tensor_t*);

void flattening_push(const struct layer_t*, const struct tensor_t*);

void flattening_destroy(struct layer_t*);


#endif
