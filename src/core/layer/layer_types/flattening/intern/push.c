#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/output/output.h"
#include "../flattening.h"


// functions
void flattening_push(const struct layer_t* l, const struct tensor_t* p)
{
	*output_get_prediction_bitsp(l->output) = tensor_get_bits(p);
}
