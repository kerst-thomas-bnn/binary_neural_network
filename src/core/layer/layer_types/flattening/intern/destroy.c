#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/output/output.h"
#include "../flattening.h"

#include <stdlib.h>


// functions
void flattening_destroy(struct layer_t* p)
{
	// geometry
	shape_destroy(p->shape_input);
	shape_destroy(p->shape_output);

	// output
	// prevent a memory error by setting the pointer to bits to 0
	*output_get_prediction_bitsp(p->output) = 0;
	output_destroy(p->output);

	// layer
	FREE(p);
}
