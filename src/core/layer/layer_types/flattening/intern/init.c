#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../../../bits/bits.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/output/output.h"
#include "../flattening.h"

#include <string.h>
#include <stdlib.h>


// static functions
static void set_shape_output(struct layer_t* p)
{
	unsigned int rank = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);

	p->shape_output = shape_create(rank, dims_i);
	unsigned int* dims_o = shape_get_dims(p->shape_output);

	unsigned int prod = 1;
	for (unsigned int i = 0; i < rank; ++i) {
		prod *= dims_i[i];
		dims_o[i] = (i == rank - 1) ? prod : 1;
	}
}

static void init_output(struct layer_t* p)
{
	// allocate the tensor
	p->output = output_create(p->shape_output);

	// immediately free the data, there is no need to perform computations
	// in a flattening layer, so no output is needed
	bits_destroy(output_get_prediction_bits(p->output));
	*output_get_prediction_bitsp(p->output) = 0;
}


// functions
struct layer_t* _flattening_init(const struct tensor_t* input)
{
	// allocate
	struct layer_t* p = SALLOC(p);
	p->layer_type = LAYER_TYPE_FLATTEN;

	// geometry
	p->shape_input = shape_create(tensor_get_rank(input), tensor_get_dims(input));
	set_shape_output(p);

	// weights
	p->weights = 0;

	// output
	init_output(p);

	// functions
	p->push = &flattening_push;
	p->destroy = &flattening_destroy;

	return p;
}
