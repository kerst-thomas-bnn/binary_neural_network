#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../../init.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/output/output.h"
#include "../inception_v1.h"

#include <stdint.h>
#include <string.h>
#include <stdlib.h>


// static functions
static void set_shape_output(struct layer_t* p, unsigned int n_filters)
{
	unsigned int rank = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);

	p->shape_output = shape_create(rank, dims_i);
	unsigned int* dims_o = shape_get_dims(p->shape_output);

	// spatial dimensions
	for (unsigned int i = 0; i < rank - 1; ++i) {
		unsigned int dim = dims_i[i] / p->strides;
		dim += (dims_i[i] > dim * p->strides) ? 1 : 0;
		dims_o[i] = dim;
	}

	// filters
	dims_o[rank - 1] = 2 * n_filters + dims_i[rank - 1];
}

static void init_sub_layers(struct layer_t* p,
                            const struct tensor_t* input,
                            unsigned int n_filters,
                            unsigned char** weights)
{
	// allocate memory for the sub-layers
	p->n_sub_layers = (p->strides > 1) ? 3 : 2;
	MALLOC(p->sub_layers, p->n_sub_layers);
	unsigned int pos = 0;

	// add max-pool, if strides > 1
	if (p->strides > 1) {
		// init the pooling layer
		unsigned int kernel_dims[2] = {p->strides, p->strides};
		struct tensor_t* kernel = tensor_create(2, kernel_dims, 0);
		p->sub_layers[pos++] = maximum_pooling_init(input, kernel, p->strides, 0);
		tensor_destroy(kernel);
	}

	// add convolution layers
	unsigned int kernel_widths[2] = {3, 5};
	for (unsigned int i = 0; i < 2; ++i) {
		// set the parameters passed to the layer init function
		unsigned int kernel_dims[3] = {kernel_widths[i], kernel_widths[i], n_filters};
		struct tensor_t* kernel = tensor_create(3, kernel_dims, 0);
		p->sub_layers[pos++] = convolution_init(input, kernel, p->strides, weights);
		tensor_destroy(kernel);
	}
}


// functions
struct layer_t* _inception_v1_init(const struct tensor_t* input,
                                   const struct tensor_t* kernel,
                                   unsigned int strides,
                                   unsigned char** weights)
{
	// allocate
	struct layer_t* p;
	MALLOC(p, 1);
	p->layer_type = LAYER_TYPE_INCEPTION_V1;
	p->strides = strides;

	// geometry
	p->shape_input = shape_create(tensor_get_rank(input), tensor_get_dims(input));
	set_shape_output(p, *tensor_get_dims(kernel));

	// sub-layers
	init_sub_layers(p, input, *tensor_get_dims(kernel), weights);

	// output
	p->output = output_create(p->shape_output);

	// functions
	p->push = &inception_v1_push;
	p->destroy = &inception_v1_destroy;

	return p;
}
