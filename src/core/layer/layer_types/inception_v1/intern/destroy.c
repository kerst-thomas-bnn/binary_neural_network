#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/output/output.h"
#include "../inception_v1.h"

#include <stdlib.h>


// functions
void inception_v1_destroy(struct layer_t* p) {
	// geometry
	shape_destroy(p->shape_input);
	shape_destroy(p->shape_output);

	// sub-layers
	for (unsigned int i = 0; i < p->n_sub_layers; ++i) {
		struct layer_t* q = p->sub_layers[i];
		q->destroy(q);
	}
	FREE(p->sub_layers);

	// output
	output_destroy(p->output);

	// layer
	FREE(p);

	return;
}
