#include "../../../../tensor/tensor.h"
#include "../../../layer.h"
#include "../../../def/layer_def.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../inception_v1.h"

#include <string.h>


// functions
void inception_v1_push(const struct layer_t* l, const struct tensor_t* p)
{
	// push through the sub-layers
	const struct tensor_t* outs[3];

	// - if no pooling, then point to the raw input
	unsigned int offsets = (l->strides == 1) ? 1 : 0;
	if (offsets)
		outs[0] = p;

	// - add the layer outputs
	for (unsigned int i = 0; i < l->n_sub_layers; ++i) {
		struct layer_t* q = l->sub_layers[i];
		q->push(q, p);
		outs[i + offsets] = output_get_prediction(q->output);
	}

	// stack the outs
	unsigned char* bytes = output_get_prediction_bytes(l->output);
	memset(bytes, 0, output_get_prediction_n_bytes(l->output));
	tensor_stack(3, outs, bytes);
}
