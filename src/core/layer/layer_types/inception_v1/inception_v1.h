#ifndef INCEPTION_V1_H
#define INCEPTION_V1_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t* _inception_v1_init(const struct tensor_t*,
                                   const struct tensor_t*,
                                   unsigned int,
                                   unsigned char**);

void inception_v1_push(const struct layer_t*, const struct tensor_t*);

void inception_v1_destroy(struct layer_t*);


#endif
