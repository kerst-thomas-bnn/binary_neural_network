class inception_v1(layers.Layer):
	def __init__(self, kernel, strides=1, dropout=False, dropout_rate=0.0):
		super(inception_v1, self).__init__()
		self.strides = strides
		self.kernel = kernel[0]
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		# layers
		if self.strides > 1:
			self.pool = maximum_pooling(kernel=[self.strides, self.strides])

		# convolutions
		self.conv3 = convolution(
			kernel=[3, 3, self.kernel],
			strides=self.strides,
			dropout=self.dropout,
			dropout_rate=self.dropout_rate
		)
		self.conv5 = convolution(
			kernel=[5, 5, self.kernel],
			strides=self.strides,
			dropout=self.dropout,
			dropout_rate=self.dropout_rate
		)

		return

	def save(self, f):
		self.conv3.save(f)
		self.conv5.save(f)

		return

	def call(self, inputs):
		# set train vars
		if self.strides > 1:
			self.pool.train = self.train

		self.conv3.train = self.train
		self.conv5.train = self.train

		# pooling
		p = inputs
		if self.strides > 1:
			p = self.pool(inputs)

		c3 = self.conv3(inputs)
		c5 = self.conv5(inputs)

		outputs = tf.concat([p, c3, c5], axis=3)

		return outputs
