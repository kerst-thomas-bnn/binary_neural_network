#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../bitwise.h"


// functions
void bitwise_push(const struct layer_t *layer, const struct tensor_t *input)
{
	// fetch pointers
	unsigned char *weights_and = weights_get_and(layer->weights);
	unsigned char *weights_or = weights_get_or(layer->weights);
	unsigned char *weights_xor = weights_get_xor(layer->weights);
	unsigned char *ipt = tensor_get_bytes(input);
	unsigned char *output = output_get_prediction_bytes(layer->output);

	// iterate
	unsigned int n = tensor_get_n_bytes(input);
	for (unsigned int i = 0; i < n; ++i) {
		output[4 * i + 0] = ipt[i];
		output[4 * i + 1] = ipt[i] & weights_and[i];
		output[4 * i + 2] = ipt[i] | weights_or[i];
		output[4 * i + 3] = ipt[i] ^ weights_xor[i];
	}
}
