#include "../../../../include/memory.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../bitwise.h"

#include <stdlib.h>


// functions
void bitwise_destroy(struct layer_t *layer)
{
	// geometry
	shape_destroy(layer->shape_input);
	shape_destroy(layer->shape_output);

	// weights
	weights_destroy(layer->weights);

	// output
	output_destroy(layer->output);

	// layer
	FREE(layer);
}
