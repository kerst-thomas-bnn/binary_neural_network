#include "../../../../include/debug.h"
#include "../../../../include/array.h"
#include "../../../../include/memory.h"
#include "../../../../include/error.h"
#include "../../../../include/bitwise.h"
#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../bitwise.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


// constants
#define ERR_MSG "Unable to define the bitwise layer for an input whose total number of bits is indivisible by 8"


// static functions
static void set_shape_input(struct layer_t *layer, const struct tensor_t *ipt)
{
	// get shape
	unsigned int rank = tensor_get_rank(ipt);
	unsigned int *dims = tensor_get_dims(ipt);

	// copy shape
	layer->shape_input = shape_create(rank, dims);
}

static void set_shape_output(struct layer_t *layer)
{
	// compute number of elements per filter
	unsigned int rank = shape_get_rank(layer->shape_input);
	unsigned int *dims = shape_get_dims(layer->shape_input);
	unsigned int n_bits = array_product_ui(dims, rank);

	// exit program if bitwise layer cannot be defined
	FATAL_ERROR(!IS_DIV8(n_bits), ERR_MSG);

	// set up output shape
	layer->shape_output = shape_create(rank, dims);
	shape_get_dims(layer->shape_output)[2] *= 4;
}


// functions
struct layer_t *_bitwise_init(const struct tensor_t *input,
                              unsigned char **weights)
{
	// allocate
	struct layer_t *p = SALLOC(p);
	p->layer_type = LAYER_TYPE_BITWISE;

	// geometry
	set_shape_input(p, input);
	set_shape_output(p);

	// weights
	p->weights = weights_create_bitwise(p->shape_input, weights);

	// output
	p->output = output_create(p->shape_output);

	// functions
	p->push = &bitwise_push;
	p->destroy = &bitwise_destroy;

	return p;
}
