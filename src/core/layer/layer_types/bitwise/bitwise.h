#ifndef BITWISE_LAYER_H
#define BITWISE_LAYER_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t *_bitwise_init(const struct tensor_t* input,
                              unsigned char** weights);

void bitwise_push(const struct layer_t *layer, const struct tensor_t *input);

void bitwise_destroy(struct layer_t *layer);


#endif
