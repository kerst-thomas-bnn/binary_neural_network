class bitwise(layers.Layer):
	def __init__(self, dropout=False, dropout_rate=0.0):
		super(bitwise, self).__init__()
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		return

	def build(self, input_shape):
		args = {"shape": input_shape, "trainable": True}

		# and
		init = tf.random_uniform_initializer(minval=0.0, maxval=1.0)
		self.w_and = self.add_weight(initializer=init, **args)

		# or
		init = tf.random_uniform_initializer(minval=-1.0, maxval=1.0)
		self.w_or = self.add_weight(initializer=init, **args)

		# xor
		init = tf.random_uniform_initializer(minval=-1.0, maxval=1.0)
		self.w_xor = self.add_weight(initializer=init, **args)

		return

	def call(self, inputs):
		# functions
		def get_and():
			# weights
			w = self.w_and
			w = w + tf.stop_gradient(tf.sign(w) - w)
			w_clipped = tf.clip_by_value(
				w,
				clip_value_min=0.0,
				clip_value_max=1.0
			)
			w = w + tf.stop_gradient(w_clipped - w)

			# calculate
			outs = tf.math.multiply(x=inputs, y=w)
			outs_subtracted = tf.subtract(outs, 0.5)
			outs = outs + tf.stop_gradient(outs_subtracted - outs)
			outs = outs + tf.stop_gradient(tf.sign(outs) - outs)

			return outs

		def get_or():
			# z1 - xnor
			w1 = self.w_or
			w1 = w1 + tf.stop_gradient(tf.sign(w1) - w1)
			z1 = tf.math.multiply(x=inputs, y=w1)
			z1 = z1 + tf.stop_gradient(tf.add(z1, 0.5) - z1)
			z1 = z1 + tf.stop_gradient(tf.sign(z1) - z1)

			# z2 - and
			w1_clipped = tf.clip_by_value(
				w1,
				clip_value_min=0.0,
				clip_value_max=1.0
			)
			w2 = w1 + tf.stop_gradient(w1_clipped - w1)
			z2 = tf.math.multiply(x=inputs, y=w2)
			z2 = z2 + tf.stop_gradient(tf.subtract(z2, 0.5) - z2)
			z2 = z2 + tf.stop_gradient(tf.sign(z2) - z2)

			# activate
			outputs = tf.multiply(x=z1, y=z2)

			return outputs

		def get_xor():
			# weights
			w = self.w_xor
			w = w + tf.stop_gradient(tf.sign(w) - w)

			# calculate
			out = tf.math.multiply(x=inputs, y=w)
			out = out + tf.stop_gradient(tf.add(out, 0.5) - out)
			out = out + tf.stop_gradient(tf.sign(out) - out)

			return out

		def concatenate(output_and, output_or, output_xor):
			shp_in = inputs.shape
			n_slices = int(shp_in[-1] / 8)
			begin = [0 for i in range(shp_in.__len__())]
			size = [*shp_in[:-1], 8]

			slices = []
			args = {"begin": begin, "size": size}
			for i in range(n_slices):
				begin[-1] = i * 8
				slices.append(tf.slice(inputs, **args))
				slices.append(tf.slice(output_and, **args))
				slices.append(tf.slice(output_or, **args))
				slices.append(tf.slice(output_xor, **args))

			outputs = tf.concat(slices, axis=shp_in.__len__() - 1)

			return outputs

		def apply_dropout(p):
			if self.dropout:
				return tf.nn.dropout(p, rate=self.dropout_rate)

			return p


		# main
		# individual layers
		output_and = get_and()
		output_or = get_or()
		output_xor = get_xor()

		# concatenate
		outputs_con = concatenate(output_and, output_or, output_xor)

		# during training
		if self.train:
			outputs_con = apply_dropout(outputs_con)

		return outputs_con

	def clip_weights(self):
		# and
		clipped_and = tf.clip_by_value(
			self.w_and,
			clip_value_min=0.0,
			clip_value_max=1.0
		)
		self.w_and.assign(clipped_and)

		# or
		clipped_or = tf.clip_by_value(
			self.w_or,
			clip_value_min=-1.0,
			clip_value_max=1.0
		)
		self.w_or.assign(clipped_or)

		# xor
		clipped_xor = tf.clip_by_value(
			self.w_xor,
			clip_value_min=-1.0,
			clip_value_max=1.0
		)
		self.w_xor.assign(clipped_xor)

		return


	def save(self, f):
		# and
		vals = self.w_and.numpy().reshape(-1, order="C")
		vals_bin = np.zeros(vals.__len__(), dtype=np.uint8)
		vals_bin[vals > 0] = 1
		vals_write = np.packbits(vals_bin)
		f.write(vals_write)

		# or
		vals = self.w_or.numpy().reshape(-1, order="C")
		vals_bin = np.zeros(vals.__len__(), dtype=np.uint8)
		vals_bin[vals > 0] = 1
		vals_write = np.packbits(vals_bin)
		f.write(vals_write)

		# xor
		vals = self.w_xor.numpy().reshape(-1, order="C")
		vals_bin = np.zeros(vals.__len__(), dtype=np.uint8)
		vals_bin[vals < 0] = 1
		vals_write = np.packbits(vals_bin)
		f.write(vals_write)

		return
