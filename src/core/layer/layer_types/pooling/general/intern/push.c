#include "../../../../../tensor/tensor.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/shape/shape.h"
#include "../../../../def/padding/padding.h"
#include "../../../../def/output/output.h"
#include "../../../../activations/activations.h"
#include "../general.h"

#include <string.h>


// static variables
static const struct layer_t* layer;
static const struct tensor_t* input;
static unsigned int* limits;
static unsigned int* paddings;
static unsigned int n_bytes;


// static functions
static inline void iterate_over_filters() {
	unsigned int rank = output_get_prediction_rank(layer->output);
	unsigned int n = output_get_prediction_dims(layer->output)[rank - 1];
	for (unsigned int i = 0; i < n; ++i) {
		limits[2 * rank - 2] = i;
		limits[2 * rank - 1] = i + 1;

		unsigned char slice_vals[n_bytes];
		memset(slice_vals, 0, n_bytes);
		tensor_slice_vals(input, limits, paddings, slice_vals);
		output_pooling_bits_set(layer->output, slice_vals);
		layer->activate(layer->output, 0);
	}

	return;
}

static void iterate_over_dimension(unsigned int dim) {
	// is this the last dim? if yes, then compute the new tensor value for
	// each filter
	if (dim == output_get_prediction_rank(layer->output) - 1) {
		iterate_over_filters();

		return;
	}

	// if no, then keep going
	unsigned int* dims = shape_get_dims(layer->shape_kernel);
	unsigned int n = tensor_get_dims(input)[dim];
	for (unsigned int i = 0; i < n; i += dims[dim]) {
		limits[2 * dim] = i;
		limits[2 * dim + 1] = i + dims[dim];
		iterate_over_dimension(dim + 1);
	}

	return;
}


// functions
void pooling_general_push(const struct layer_t* l, const struct tensor_t* p) {
	// init
	output_reset(l->output);

	// set static variables
	layer = l;
	input = p;

	// compute number of bytes for tensor slice fetch
	unsigned int rank = shape_get_rank(layer->shape_kernel);
	unsigned int* dims = shape_get_dims(layer->shape_kernel);
	unsigned int n_bits = 1;
	for (unsigned int i = 0; i < rank; ++i)
		n_bits *= dims[i];

	output_pooling_n_bits_set(l->output, n_bits);
	n_bytes = n_bits >> 3;
	if (n_bytes >> 3 < n_bits)
		++n_bytes;

	// initiate limits and pads
	unsigned int rank_output = shape_get_rank(layer->shape_output);
	unsigned int _limits[2 * rank_output];
	unsigned int _paddings[2 * (rank_output- 1)];
	limits = _limits;
	paddings = _paddings;
	for (unsigned int i = 0; i < rank_output - 1; ++i) {
		paddings[2 * i] = padding_get_left(l->padding, i);
		paddings[2 * i + 1] = padding_get_right(l->padding, i);
	}

	// fill in the contents
	iterate_over_dimension(0);

	return;
}
