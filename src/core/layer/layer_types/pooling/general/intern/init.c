#include "../../../../../include/debug.h"
#include "../../../../../include/memory.h"
#include "../../../../../tensor/tensor.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/shape/shape.h"
#include "../../../../def/padding/padding.h"
#include "../../../../def/output/output.h"
#include "../../../../activations/activations.h"
#include "../../pooling.h"
#include "../general.h"

#include <string.h>
#include <stdlib.h>


// static functions
static void set_shape_output(struct layer_t* p)
{
	unsigned int rank = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);
	unsigned int* dims_k = shape_get_dims(p->shape_kernel);

	p->shape_output = shape_create(rank, dims_i);
	unsigned int* dims_o = shape_get_dims(p->shape_output);

	for (unsigned int i = 0; i < rank - 1; ++i) {
		dims_o[i] = dims_i[i] / dims_k[i];
		if (dims_o[i] * dims_k[i] < dims_i[i])
			++dims_o[i];
	}
}


// functions
struct layer_t* pooling_general_init(const struct tensor_t* input,
                                     const struct tensor_t* kernel,
                                     enum pooling_init_flag flag)
{
	DISP("IMPLEMENT PROPER POOLING_GENERAL_INIT INCLUDING XNOR", s);
	// allocate
	struct layer_t* p = SALLOC(p);
	switch (flag) {
	case POOLING_INIT_MAXIMUM:
		p->layer_type = LAYER_TYPE_MAXIMUM_POOLING;
		break;
	case POOLING_INIT_AVERAGE:
		p->layer_type = LAYER_TYPE_AVERAGE_POOLING;
		break;
	case POOLING_INIT_XNOR:
		p->layer_type = LAYER_TYPE_XNOR_POOLING;
		break;
	}

	// geometry
	p->shape_input = shape_create(
		tensor_get_rank(input),
		tensor_get_dims(input)
	);
	p->shape_kernel = shape_create(
		tensor_get_rank(kernel),
		tensor_get_dims(kernel)
	);
	set_shape_output(p);
	p->padding = padding_create(p->shape_input,
		p->shape_kernel,
		p->shape_output,
		0
	);

	// output
	p->output = output_create(p->shape_output);

	// functions
	if (flag == POOLING_INIT_MAXIMUM)
		p->activate = &activate_maximum;
	else
		p->activate = &activate_average;
	p->push = &pooling_general_push;
	p->destroy = &pooling_general_destroy;

	return p;
}
