#include "../../../../../include/memory.h"
#include "../../../../../tensor/tensor.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/shape/shape.h"
#include "../../../../def/output/output.h"
#include "../general.h"

#include <stdlib.h>


// functions
void pooling_general_destroy(struct layer_t* p)
{
	// geometry
	shape_destroy(p->shape_input);
	shape_destroy(p->shape_kernel);
	shape_destroy(p->shape_output);
	FREE(p->padding);

	// output
	output_destroy(p->output);

	// layer
	FREE(p);
}
