#ifndef POOLING_GENERAL_H
#define POOLING_GENERAL_H


// forward declarations
enum pooling_init_flag;
struct tensor_t;
struct layer_t;


// functions
struct layer_t* pooling_general_init(const struct tensor_t*,
                                     const struct tensor_t*,
                                     enum pooling_init_flag);

void pooling_general_push(const struct layer_t*, const struct tensor_t*);

void pooling_general_destroy(struct layer_t*);


#endif
