#include "../../../../tensor/tensor.h"
#include "../pooling.h"
#include "../2d/2d.h"
#include "../general/general.h"

#include <stdbool.h>


// macros
#define POOLING_INIT_ARGS  \
	const struct tensor_t* input,  \
	const struct tensor_t* kernel,  \
	enum pooling_init_flag flag  \

#define INIT_FUNCTION_ARGS  \
	const struct tensor_t*,  \
	const struct tensor_t*,  \
	enum pooling_init_flag  \


// static functions
static bool is_2d(const struct tensor_t* input) {
	return (tensor_get_rank(input) == 3) ? true : false;
}


// functions
struct layer_t* _pooling_init(POOLING_INIT_ARGS) {
	struct layer_t* (*init_function)(INIT_FUNCTION_ARGS);
	if (is_2d(input))
		init_function = &pooling_2d_init;
	else
		init_function = &pooling_general_init;

	return init_function(input, kernel, flag);
}
