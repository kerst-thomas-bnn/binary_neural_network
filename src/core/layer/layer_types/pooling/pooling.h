#ifndef POOLING_H
#define POOLING_H


// forward declarations
struct tensor_t;
struct layer_t;


// enumerations
enum pooling_init_flag {
	POOLING_INIT_MAXIMUM,
	POOLING_INIT_AVERAGE,
	POOLING_INIT_XNOR
};


// functions
struct layer_t* _pooling_init(const struct tensor_t*,
                              const struct tensor_t*,
                              enum pooling_init_flag);


#endif
