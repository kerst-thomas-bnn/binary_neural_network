#ifndef POOLING_2D
#define POOLING_2D


// forward declarations
enum pooling_init_flag;
struct tensor_t;
struct layer_t;


// functions
struct layer_t* pooling_2d_init(const struct tensor_t*,
                                const struct tensor_t*,
                                enum pooling_init_flag);

void pooling_2d_destroy(struct layer_t*);


#endif
