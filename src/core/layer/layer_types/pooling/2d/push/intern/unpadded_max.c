#include "../../../../../../include/debug.h"
#include "../../../../../../include/bitwise.h"
#include "../../../../../../tensor/tensor.h"
#include "../../../../../def/layer_def.h"
#include "../../../../../def/shape/shape.h"
#include "../../../../../def/output/output.h"
#include "../push.h"


// static functions
static inline void it_over_filters(const unsigned char* input,
                                   unsigned char* output,
                                   unsigned int n,
                                   unsigned int input_o,
                                   unsigned int output_o)
{
	for (unsigned int i = 0; i < n; ++i)
		output[output_o + i] |= input[input_o + i];
}

static inline void it_over_kernel(const unsigned char* input,
                                  unsigned char* output,
                                  const unsigned int* dims,
                                  unsigned int row,
                                  unsigned int col,
                                  const unsigned int* indicees,
                                  unsigned int n,
                                  unsigned int output_o)
{
	for (unsigned int i = 0; i < dims[0]; ++i) {
		for (unsigned int j = 0; j < dims[1]; ++j) {
			unsigned int row_o = (row + i) * indicees[0];
			unsigned int col_o = (col + j) * indicees[1];
			unsigned int input_o = DIV8(row_o + col_o);
			it_over_filters(input, output, n, input_o, output_o);
		}
	}
}


// functions
void pooling_2d_push_unpadded_max(const struct layer_t* l,
                                  const struct tensor_t* p)
{
	// init
	output_reset(l->output);

	// clean output
	unsigned char* in = tensor_get_bytes(p);
	unsigned char* out = output_get_prediction_bytes(l->output);

	// initiate pointers
	unsigned int* p_dims = tensor_get_dims(p);
	unsigned int* ind = tensor_get_index_offset(p);
	unsigned int n = DIV8(p_dims[2]);
	unsigned int out_o = 0;

	// fill in the contents
	// - iterate over input
	unsigned int* dims = shape_get_dims(l->shape_kernel);
	for (unsigned int row = 0; row < p_dims[0]; row += dims[0]) {
		for (unsigned int col = 0; col < p_dims[1]; col += dims[1]) {
			it_over_kernel(in, out, dims, row, col, ind, n, out_o);
			out_o += n;
		}
	}
}
