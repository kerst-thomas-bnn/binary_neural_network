#include "../../../../../../include/bitwise.h"
#include "../../../../../../tensor/tensor.h"
#include "../../../../../def/layer_def.h"
#include "../../../../../def/shape/shape.h"
#include "../../../../../def/padding/padding.h"
#include "../../../../../def/output/output.h"
#include "../push.h"

#include <string.h>


// static variables
unsigned int threshold;


// static functions
static void accumulate(unsigned int* accumulation, unsigned char byte) {
	for (unsigned char i = 0; i < 8; ++i) {
		unsigned char bit_val = (byte & (128 >> i)) ? 1 : 0;
		if (bit_val)
			++(accumulation[i]);
	}

	return;
}

static void activate(unsigned int* accumulation, unsigned char* wrt_tar) {
	unsigned char byte = 0;
	for (unsigned char i = 0; i < 8; ++i)
		if (accumulation[i] >= threshold)
			byte |= 128 >> i;

	*wrt_tar = byte;

	return;
}


// functions
void pooling_2d_push_padded_avg(const struct layer_t* l, const struct tensor_t* p) {
	// init
	output_reset(l->output);

	// clean output
	unsigned char* input = tensor_get_bytes(p);
	unsigned char* output = output_get_prediction_bytes(l->output);

	// set threshold
	unsigned int* dims = shape_get_dims(l->shape_kernel);
	threshold = dims[0] * dims[1];
	threshold = (threshold >> 1) + (threshold % 2);

	// initiate pointers
	unsigned int* p_dims = tensor_get_dims(p);
	unsigned int* p_io = tensor_get_index_offset(p);
	unsigned int accumulation[p_dims[2]];
	unsigned int n_filter_bytes = p_dims[2] >> 3;
	unsigned int offset_input;
	unsigned int offset_output = 0;

	// paddings
	unsigned int padding_rows_left = padding_get_left(l->padding, 0);
	// - cols
	unsigned int padding_cols_left = padding_get_left(l->padding, 1);

	// fill in the contents
	// - iterate over input
	for (unsigned int row = 0; row < p_dims[0]; row += dims[0]) {
		for (unsigned int col = 0; col < p_dims[1]; col += dims[1]) {
			memset(accumulation, 0, p_dims[2] * sizeof(*accumulation));
			// - iterate over kernel
			for (unsigned int i = 0; i < dims[0]; ++i) {
				if (padding_rows_left > row + i)
					continue;

				if (row + i - padding_rows_left >= p_dims[0])
					continue;

				for (unsigned int j = 0; j < dims[1]; ++j) {
					if (padding_cols_left > col + j)
						continue;

					if (col + j - padding_cols_left >= p_dims[1])
						continue;

					// - iterate over filters
					offset_input = ((row + i - padding_rows_left) * p_io[0] + (col + j - padding_cols_left) * p_io[1]) >> 3;
					for (unsigned int k = 0; k < n_filter_bytes; ++k) {
						accumulate(&accumulation[k << 3], input[offset_input + k]);
					}
				}
			}
			for (unsigned int k = 0; k < n_filter_bytes; ++k) {
				activate(&accumulation[k << 3], &output[offset_output + k]);
			}
			offset_output += n_filter_bytes;
		}
	}

	return;
}
