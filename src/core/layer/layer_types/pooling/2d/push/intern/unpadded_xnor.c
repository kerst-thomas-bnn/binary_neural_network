#include "../../../../../../include/bitwise.h"
#include "../../../../../../tensor/tensor.h"
#include "../../../../../def/layer_def.h"
#include "../../../../../def/shape/shape.h"
#include "../../../../../def/output/output.h"
#include "../push.h"


// functions
void pooling_2d_push_unpadded_xnor(const struct layer_t* l, const struct tensor_t* p) {
	// init
	output_reset(l->output);

	// clean output
	unsigned char* input = tensor_get_bytes(p);
	unsigned char* output = output_get_prediction_bytes(l->output);

	// initiate pointers
	unsigned int* p_dims = tensor_get_dims(p);
	unsigned int* p_io = tensor_get_index_offset(p);
	unsigned int n_filter_bytes = p_dims[2] >> 3;
	unsigned int offset_input;
	unsigned int offset_output = 0;

	// fill in the contents
	// - iterate over input
	unsigned int* dims = shape_get_dims(l->shape_kernel);
	for (unsigned int row = 0; row < p_dims[0]; row += dims[0]) {
		for (unsigned int col = 0; col < p_dims[1]; col += dims[1]) {
			// - iterate over kernel
			for (unsigned int i = 0; i < dims[0]; ++i) {
				for (unsigned int j = 0; j < dims[1]; ++j) {
					// - iterate over filters
					offset_input = ((row + i) * p_io[0] + (col + j) * p_io[1]) >> 3;
					for (unsigned int k = 0; k < n_filter_bytes; ++k) {
						output[offset_output + k] |= input[offset_input + k];
					}
				}
			}
			offset_output += n_filter_bytes;
		}
	}

	return;
}
