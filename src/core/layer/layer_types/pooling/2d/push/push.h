#ifndef POOLING_2D_PUSH_H
#define POOLING_2D_PUSH_H


// forward declarations
struct layer_t;
struct tensor_t;


// functions
void pooling_2d_push_padded_max(const struct layer_t*, const struct tensor_t*);

void pooling_2d_push_padded_avg(const struct layer_t*, const struct tensor_t*);

void pooling_2d_push_padded_xnor(const struct layer_t*,
                                 const struct tensor_t*);

void pooling_2d_push_unpadded_max(const struct layer_t*,
                                  const struct tensor_t*);

void pooling_2d_push_unpadded_avg(const struct layer_t*,
                                  const struct tensor_t*);

void pooling_2d_push_unpadded_xnor(const struct layer_t*,
                                   const struct tensor_t*);


#endif
