#include "../../../../../include/bitwise.h"
#include "../../../../../include/memory.h"
#include "../../../../../include/error.h"
#include "../../../../../tensor/tensor.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/shape/shape.h"
#include "../../../../def/padding/padding.h"
#include "../../../../def/output/output.h"
#include "../../../pooling/pooling.h"
#include "../2d.h"
#include "../push/push.h"

#include <string.h>
#include <stdbool.h>
#include <stdlib.h>


#define ERR_MSG  "2D POOLING FOR AN INPUT LAYER WITH A NUMBER OF FILTERS INDIVISLBE BY 8 IS NOT POSSIBLE"


// static functions
static void set_layer_type(struct layer_t* p, enum pooling_init_flag flag)
{
	switch (flag) {
	case POOLING_INIT_MAXIMUM:
		p->layer_type = LAYER_TYPE_MAXIMUM_POOLING;
		break;
	case POOLING_INIT_AVERAGE:
		p->layer_type = LAYER_TYPE_AVERAGE_POOLING;
		break;
	case POOLING_INIT_XNOR:
		p->layer_type = LAYER_TYPE_XNOR_POOLING;
		break;
	}
}

static void set_shape_input(struct layer_t* p, const struct tensor_t* input)
{
	p->shape_input = shape_create(
		tensor_get_rank(input),
		tensor_get_dims(input)
	);
}

static void set_shape_kernel(struct layer_t* p, const struct tensor_t* krnl)
{
	p->shape_kernel = shape_create(
		tensor_get_rank(krnl),
		tensor_get_dims(krnl)
	);
}

static void set_shape_output(struct layer_t* p)
{
	unsigned int rank = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);
	unsigned int* dims_k = shape_get_dims(p->shape_kernel);

	p->shape_output = shape_create(rank, dims_i);
	unsigned int* dims_o = shape_get_dims(p->shape_output);

	for (unsigned int i = 0; i < rank - 1; ++i) {
		dims_o[i] = dims_i[i] / dims_k[i];
		if (dims_o[i] * dims_k[i] < dims_i[i])
			++dims_o[i];
	}
}

static void set_padding(struct layer_t* p)
{
	p->padding = padding_create(
		p->shape_input,
		p->shape_kernel,
		p->shape_output,
		0
	);
}

static void set_padded_push(struct layer_t* p, enum pooling_init_flag flag)
{
	switch (flag) {
	case POOLING_INIT_MAXIMUM:
		p->push = &pooling_2d_push_padded_max;
		break;
	case POOLING_INIT_AVERAGE:
		p->push = &pooling_2d_push_padded_avg;
		break;
	case POOLING_INIT_XNOR:
		p->push = &pooling_2d_push_padded_xnor;
		break;
	}
}

static void set_unpadded_push(struct layer_t* p, enum pooling_init_flag flag)
{
	switch (flag) {
	case POOLING_INIT_MAXIMUM:
		p->push = &pooling_2d_push_unpadded_max;
		break;
	case POOLING_INIT_AVERAGE:
		p->push = &pooling_2d_push_unpadded_avg;
		break;
	case POOLING_INIT_XNOR:
		p->push = &pooling_2d_push_unpadded_xnor;
		break;
	}
}

static void set_push_function(struct layer_t* p, enum pooling_init_flag flag)
{
	// exit with failure, if no pushing is possible
	unsigned int n_filters = shape_get_dims(p->shape_output)[2];
	FATAL_ERROR(!IS_DIV8(n_filters), ERR_MSG);

	// check if there is padding
	bool is_padded = false;
	for (unsigned int i = 0; i < shape_get_rank(p->shape_kernel); ++i) {
		if (padding_get_left(p->padding, i))
			is_padded = true;

		if (padding_get_right(p->padding, i))
			is_padded = true;
	}

	// set the push function
	if (is_padded)
		set_padded_push(p, flag);
	else
		set_unpadded_push(p, flag);
}


// functions
struct layer_t* pooling_2d_init(const struct tensor_t* input,
                                const struct tensor_t* kernel,
                                enum pooling_init_flag flag)
{
	// allocate
	struct layer_t* p = SALLOC(p);
	set_layer_type(p, flag);

	// geometry
	set_shape_input(p, input);
	set_shape_kernel(p, kernel);
	set_shape_output(p);
	set_padding(p);

	// output
	p->output = output_create(p->shape_output);

	// functions
	set_push_function(p, flag);
	p->destroy = &pooling_2d_destroy;

	return p;
}
