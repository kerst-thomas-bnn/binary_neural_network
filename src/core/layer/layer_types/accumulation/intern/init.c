#include "../../../../include/debug.h"
#include "../../../../include/memory.h"
#include "../../../../include/error.h"
#include "../../../../include/array.h"
#include "../../../../include/bitwise.h"
#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../accumulation.h"
#include "../push_init/push_init.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


// constants
#define ERR_MSG_REQ "\
\n[ERROR] Unable to define the accumlation layer for an input whose total \
number of bits is indivisible by 8. Aborting program execution.\n\
"

#define ERR_MSG_OUT "\
\n[ERROR] Only 1-dimensional accumlation is permitted.\
Aborting program execution.\n\
"


// static functions
static void check_input_requirements(struct layer_t* p)
{
	// compute number of elements per filter
	unsigned int rank = shape_get_rank(p->shape_input);
	unsigned int* dims = shape_get_dims(p->shape_input);
	unsigned int n_bits = array_product_ui(dims, rank);

	// exit program if 8 bit bitwise operations cannot be applied
	FATAL_ERROR(!IS_DIV8(n_bits), ERR_MSG_REQ);
}

static void set_shape_output(struct layer_t* p, const struct tensor_t* kernel)
{
	// only 1-dimension accumlation is permitted
	unsigned int rank = tensor_get_rank(kernel);
	FATAL_ERROR(rank != 1, ERR_MSG_OUT);

	// create the shape
	unsigned int* dims = tensor_get_dims(kernel);
	p->shape_output = shape_create(rank, dims);
}


// functions
struct layer_t* _accumulation_init(const struct tensor_t* input,
                                   const struct tensor_t* kernel,
                                   unsigned char** weights)
{
	// initialise
	struct layer_t* p = SALLOC(p);
	p->layer_type = LAYER_TYPE_ACCUMULATION;

	// geometry
	p->shape_input = shape_create(tensor_get_rank(input), tensor_get_dims(input));
	check_input_requirements(p);
	set_shape_output(p, kernel);

	// weights
	p->weights = weights_create_accumulation(
		p->shape_input,
		p->shape_output,
		weights
	);

	// output
	p->output = output_create(p->shape_output);

	// functions
	accumulation_push_init(p);
	p->destroy = &accumulation_destroy;

	return p;
}
