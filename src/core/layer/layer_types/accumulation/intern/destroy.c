#include "../../../../include/memory.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../accumulation.h"

#include <stdlib.h>


// functions
void accumulation_destroy(struct layer_t* p) {
	// geometry
	shape_destroy(p->shape_input);
	shape_destroy(p->shape_output);

	// weights
	weights_destroy(p->weights);

	// output
	output_destroy(p->output);

	// layer
	FREE(p);

	return;
}
