#ifndef ACCUMULATION_PUSH_INIT_H
#define ACCUMULATION_PUSH_INIT_H


// forward declarations
struct layer_t;


// functions
void accumulation_push_init(struct layer_t*);


#endif
