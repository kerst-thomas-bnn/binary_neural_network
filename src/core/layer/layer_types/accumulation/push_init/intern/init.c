#include "../../../../../include/array.h"
#include "../../../../../opt/opt.h"
#include "../../../../def/layer_def.h"
#include "../push_init.h"
#include "../push/push.h"


// static variables
static void (*push_fun[])(const struct layer_t*, const struct tensor_t*) =
{
	&accumulation_push_uc,
	&accumulation_push_ui,
	&accumulation_push_ul,
	&accumulation_push_ull,
};


// static functions
static void set_push_function(struct layer_t* p, unsigned int mode)
{
	p->push = push_fun[mode];
}


// functions
void accumulation_push_init(struct layer_t* p)
{
	opt_add_optimisation(p, &set_push_function, 0, ARRAY_LEN(push_fun));
}
