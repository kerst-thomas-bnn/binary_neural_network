#ifndef ACCUMULATION_PUSH_H
#define ACCUMULATION_PUSH_H


// forward declarations
struct layer_t;
struct tensor_t;


// functions
void accumulation_push_uc(const struct layer_t*, const struct tensor_t*);

void accumulation_push_ui(const struct layer_t*, const struct tensor_t*);

void accumulation_push_ul(const struct layer_t*, const struct tensor_t*);

void accumulation_push_ull(const struct layer_t*, const struct tensor_t*);


#endif
