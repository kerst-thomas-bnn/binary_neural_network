#include "../../../../../../tensor/tensor.h"
#include "../../../../../def/layer_def.h"
#include "../../../../../def/weights/weights.h"
#include "../../../../../def/output/output.h"
#include "../push.h"
#include "../include/hlp.h"

#include <string.h>


// functions
void accumulation_push_uc(const struct layer_t* l, const struct tensor_t* p)
{
	// initiate output
	unsigned int n_acc = weights_get_n_accumulations(l->weights);
	int acc[n_acc];
	cpy_biases(l->weights, acc, n_acc);

	// compute outputs
	unsigned char* input = tensor_get_bytes(p);
	unsigned char** weights_xor = weights_get_xor_accumulation(l->weights);
	unsigned int n_bytes_in = tensor_get_n_bytes(p);

	for (unsigned int i = 0; i < n_acc; ++i) {
		unsigned char* w_xor = weights_xor[i];
		unsigned int j = 0;
		PUSH(unsigned char, popcount8);
	}

	// flip up the bit at min_pos
	unsigned char* output = get_clean_output(l->output, n_acc);
	unsigned int pos = get_max_pos(n_acc, acc);
	output[DIV8(pos)] |= 128 >> MOD8(pos);
}
