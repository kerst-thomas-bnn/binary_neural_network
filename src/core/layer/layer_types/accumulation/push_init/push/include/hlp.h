#ifndef ACCUMULATION_PUSH_HLP_H
#define ACCUMULATION_PUSH_HLP_H


// include
#include "../../../../../../include/bitwise.h"
#include "../../../../../def/weights/weights.h"
#include "../../../../../def/output/output.h"

#include <string.h>


// forward declarations
struct layer_t;


// macros
#define PUSH(TYPE, FUN)  \
	do {  \
		TYPE* i_t = (TYPE*)input;  \
		TYPE* x_t = (TYPE*)w_xor;  \
		for (; j < n_bytes_in / sizeof(TYPE); ++j)  \
			acc[i] += 2 * (FUN)(i_t[j] ^ x_t[j]);  \
		  \
		j *= sizeof(TYPE);  \
	} while(0)


// static inline
static inline int popcount8(unsigned char value)
{
	int ones = 0;

	for (; value; ++ones)
		value &= value - 1;

	return ones;
}

static inline int get_max_pos(unsigned int len, int* array)
{
	unsigned int max_pos = 0;
	unsigned int max_val = 0;

	for (unsigned int i = 0; i < len; ++i) {
		if (array[i] > max_val) {
			max_pos = i;
			max_val = array[i];
		}
	}

	return max_pos;
}

static inline void cpy_biases(const struct weights_t* w,
                              int* acc,
                              unsigned int n_acc)
{
	unsigned int* b = weights_get_biases_accumulation_uint(w);
	memcpy(acc, b, n_acc * sizeof(*acc));
}

static inline unsigned char* get_clean_output(const struct output_t* o,
                                              unsigned int n_acc)
{
	unsigned int n = DIV8(n_acc) + !IS_DIV8(n_acc);
	unsigned char* output = output_get_prediction_bytes(o);
	memset(output, 0, n);

	return output;
}


#endif
