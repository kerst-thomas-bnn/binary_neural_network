#ifndef ACCUMULATION_H
#define ACCUMULATION_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t* _accumulation_init(const struct tensor_t*,
                                   const struct tensor_t*,
                                   unsigned char**);

void accumulation_destroy(struct layer_t*);


#endif
