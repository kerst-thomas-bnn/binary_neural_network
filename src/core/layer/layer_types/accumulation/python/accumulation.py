class accumulation(layers.Layer):
	def __init__(self, n_accumulations):
		super(accumulation, self).__init__()
		self.dropout = False
		self.train = False
		self.n_accumulations = n_accumulations

		return

	def build(self, input_shape):
		# number of bits
		weights_shape = [*input_shape[1:], self.n_accumulations]

		self.w_xnor = self.add_weight(
			shape=weights_shape,
			initializer=tf.random_uniform_initializer(
				minval=-1.0,
				maxval=1.0
			),
			trainable=True
		)

		# bias
		self.multiplier = int(np.prod(input_shape))
		self.bias = self.add_weight(
			shape=(self.n_accumulations, ),
			initializer=tf.zeros_initializer(),
			trainable=True
		)

		return

	def call(self, inputs):
		# static functions
		def get_xnor():
			# weights
			w = self.w_xnor
			w = w + tf.stop_gradient(tf.sign(w) - w)

			# calculate
			outputs = tf.nn.convolution(
				input=inputs,
				filters=w,
				padding="VALID"
			)
			outputs = tf.squeeze(outputs)

			return outputs

		def add_bias(ip):
			b = self.bias
			b_scaled = tf.multiply(b, self.multiplier)
			b = b + tf.stop_gradient(b_scaled - b)
			b = b + tf.stop_gradient(tf.math.round(b) - b)

			outputs = tf.math.add(ip, b)

			return outputs


		# main
		outputs_xnor = get_xnor()
		outputs = add_bias(outputs_xnor)

		return outputs

	def clip_weights(self):
		clipped = tf.clip_by_value(
			self.w_xnor,
			clip_value_min=-1.0,
			clip_value_max=1.0
		)
		self.w_xnor.assign(clipped)

		return

	def save(self, f):
		# weights_xnor
		vals = self.w_xnor.numpy().reshape(-1, order="C")
		vals_bin = np.zeros(vals.__len__(), dtype=np.uint8)
		vals_bin[vals < 0] = 1
		vals_write = np.packbits(vals_bin)
		f.write(vals_write)

		# biases
		vals = self.bias.numpy().reshape(-1, order="C")
		vals = np.round(np.multiply(vals, self.multiplier))
		vals = vals.astype(np.int32)
		f.write(vals)

		return
