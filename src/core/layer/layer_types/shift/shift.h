#ifndef SHIFT_H
#define SHIFT_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t* _shift_init(const struct tensor_t*, const struct tensor_t*);

void shift_push(const struct layer_t*, const struct tensor_t*);

void shift_destroy(struct layer_t*);


#endif
