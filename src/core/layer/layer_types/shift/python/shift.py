class shift(layers.Layer):
	def __init__(self, kernel, dropout=False, dropout_rate=0.0):
		super(shift, self).__init__()
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		# convert the kernel into shifts
		kernel_neg = [-k for k in kernel]
		self.shifts = [*kernel_neg, 0, *kernel]

		return

	def build(self, input_shape):
		shape = [np.prod(input_shape[1:]), ]
		init = tf.random_uniform_initializer(minval=-1.0, maxval=1.0)
		self.w = [
			self.add_weight(shape=shape, initializer=init, trainable=True)
			for _s in self.shifts
		]

		return

	def call(self, inputs):
		# nonlocals
		weights_binarised = None
		inputs_flattened = None
		shifted_outputs = None
		outputs = None


		# functions
		def get_binarised_weights():
			nonlocal weights_binarised

			weights_binarised = []
			for w in self.w:
				_w = w
				_w = _w + tf.stop_gradient(tf.sign(_w) - _w)
				_w = _w + tf.stop_gradient(tf.add(_w, 0.5) - _w)
				_w = _w + tf.stop_gradient(tf.sign(_w) - _w)
				weights_binarised.append(_w)

			return

		def flatten_inputs():
			nonlocal inputs_flattened

			inputs_flattened = tf.reshape(inputs, [inputs.shape[0], -1])

			return

		def get_shifted_outputs():
			nonlocal shifted_outputs

			shifted_outputs = []
			for _s, _w in zip(self.shifts, weights_binarised):
				# roll the input
				shifted = tf.roll(inputs_flattened, shift=8 * _s, axis=1)

				# reshape and add to the shifted outputs
				shifted = tf.multiply(shifted, _w)
				shifted_outputs.append(shifted)

			return

		def combine_shifts():
			nonlocal outputs

			for i, _s in enumerate(shifted_outputs):
				outputs = tf.add(outputs, _s) if i else _s

			outputs = outputs + tf.stop_gradient(tf.sign(outputs) - outputs)
			outputs = tf.reshape(outputs, shape=inputs.shape)

			return

		def apply_dropout():
			nonlocal outputs

			if self.train and self.dropout:
				outputs = tf.nn.dropout(outputs, rate=self.dropout_rate)

			return


		# main
		get_binarised_weights()
		flatten_inputs()
		get_shifted_outputs()
		combine_shifts()
		apply_dropout()

		return outputs

	def save(self, f):
		return
