#include "../../../../include/bitwise.h"
#include "../../../../include/debug.h"
#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/output/output.h"
#include "../shift.h"

#include <string.h>


// constants
#define SHIFT_INLINE_ARGS unsigned int pos, unsigned int shift, unsigned int n


// macros
#define APPEND_OUTPUT(SRC)  \
	do {  \
		memcpy(&output[pos], (SRC), cpy_len);  \
		pos += cpy_len;  \
	} while(0)  \


// static functions
static inline size_t get_cpy_len(const struct layer_t* l) {
	unsigned int rank = shape_get_rank(l->shape_input);
	unsigned int* dims = shape_get_dims(l->shape_input);
	size_t cpy_len = DIV8(dims[rank - 1]);

	return cpy_len;
}

static inline unsigned int get_shift_pos(SHIFT_INLINE_ARGS) {
	unsigned int p = (pos + shift >= n) ? pos - n + shift : pos + shift;

	return p;
}

static inline unsigned int get_shift_neg(SHIFT_INLINE_ARGS) {
	unsigned int p = (pos < shift) ? pos + n - shift : pos - shift;

	return p;
}


// functions
void shift_push(const struct layer_t* l, const struct tensor_t* p) {
	// input and output
	unsigned char* input = tensor_get_bytes(p);
	unsigned char* output = output_get_prediction_bytes(l->output);

	// shifts
	unsigned int n_shifts = shape_get_rank(l->shape_kernel);
	unsigned int* shifts = shape_get_dims(l->shape_kernel);

	// assemble output
	unsigned int pos = 0;
	unsigned int n = tensor_get_n_bytes(p);
	size_t cpy_len = get_cpy_len(l);
	for (unsigned int i = 0; i < n; i += cpy_len) {
		// copy the input
		APPEND_OUTPUT(&input[i]);

		// copy the shifted values - positive
		for (unsigned int j = 0; j < n_shifts; ++j) {
			unsigned int pos_p = get_shift_pos(i, shifts[j], n);
			APPEND_OUTPUT(&input[pos_p]);
		}

		// copy the shifted values - negative
		for (unsigned int j = 0; j < n_shifts; ++j) {
			unsigned int pos_n = get_shift_neg(i, shifts[j], n);
			APPEND_OUTPUT(&input[pos_n]);
		}
	}

	return;
}
