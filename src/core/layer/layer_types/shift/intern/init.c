#include "../../../../include/memory.h"
#include "../../../../include/debug.h"
#include "../../../../include/error.h"
#include "../../../../include/array.h"
#include "../../../../include/bitwise.h"
#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/output/output.h"
#include "../shift.h"


// constants
#define ERR_MSG "\
Unable to define the shift layer for an input whose total number of bits is \
indivisible by 8\
"


// static functions
static void set_shape_input(struct layer_t* p, const struct tensor_t* input) {
	// get shape
	unsigned int rank = tensor_get_rank(input);
	unsigned int* dims = tensor_get_dims(input);

	// copy shape
	p->shape_input = shape_create(rank, dims);

	return;
}

static void set_shape_kernel(struct layer_t* p, const struct tensor_t* krnl) {
	// get shape
	unsigned int rank_k = tensor_get_rank(krnl);
	unsigned int* dims_k = tensor_get_dims(krnl);

	// get the rescaling factor
	unsigned int rank_i = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);
	unsigned int scale = DIV8(dims_i[rank_i - 1]);

	// set the shift dimensions
	unsigned int dims[rank_k];
	for (unsigned int i = 0; i < rank_k; ++i)
		dims[i] = scale * dims_k[i];

	// copy shape
	p->shape_kernel = shape_create(rank_k, dims);

	return;
}

static void set_shape_output(struct layer_t* p) {
	unsigned int rank_i = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);

	// compute number of elements per filter
	unsigned int n_bits = array_product_ui(dims_i, rank_i);

	// exit program if bitwise layer cannot be defined
	FATAL_ERROR(!IS_DIV8(n_bits), ERR_MSG);

	// set up output shape
	// - copy the old array
	unsigned int dims_o[rank_i];
	memcpy(dims_o, dims_i, rank_i * sizeof(*dims_i));

	// - modify the number of filters
	unsigned int rank_k = shape_get_rank(p->shape_kernel);
	unsigned int n_filters = (2 * rank_k + 1) * dims_i[rank_i - 1];
	dims_o[rank_i - 1] = n_filters;

	p->shape_output = shape_create(rank_i, dims_o);

	return;
}


// functions
struct layer_t* _shift_init(const struct tensor_t* input,
                            const struct tensor_t* kernel) {
	// initialise
	struct layer_t* p = SALLOC(p);
	p->layer_type = LAYER_TYPE_SHIFT;

	// geometry
	set_shape_input(p, input);
	set_shape_kernel(p, kernel);
	set_shape_output(p);

	// output
	p->output = output_create(p->shape_output);

	// functions
	p->push = &shift_push;
	p->destroy = &shift_destroy;

	return p;
}
