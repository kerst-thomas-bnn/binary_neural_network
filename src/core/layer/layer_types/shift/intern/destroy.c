#include "../../../../include/memory.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/output/output.h"
#include "../shift.h"

#include <stdlib.h>


// functions
void shift_destroy(struct layer_t* p) {
	// geometry
	shape_destroy(p->shape_input);
	shape_destroy(p->shape_output);

	// output
	output_destroy(p->output);

	// layer
	FREE(p);

	return;
}
