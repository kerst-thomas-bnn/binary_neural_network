class knockout(layers.Layer):
	def __init__(self, dropout=False, dropout_rate=0.0):
		super(knockout, self).__init__()
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		return

	def build(self, input_shape):
		init = tf.random_uniform_initializer(minval=0.0, maxval=1.0)
		args = {"initializer": init, "trainable": True}
		self.w = self.add_weight(shape=input_shape, **args)

		return

	def call(self, inputs):
		# nonlocals
		weights = None
		outputs = None


		# functions
		def get_weights():
			nonlocal weights

			w = self.w
			w = w + tf.stop_gradient(tf.sign(w) - w)
			args = {"clip_value_min": 0.0, "clip_value_max": 1.0}
			w_clipped = tf.clip_by_value(w, **args)
			weights = w + tf.stop_gradient(w_clipped - w)

			return

		def push():
			nonlocal outputs

			out = tf.math.multiply(x=inputs, y=weights)
			out_subtracted = tf.subtract(out, 0.5)
			out = out + tf.stop_gradient(out_subtracted - out)
			outputs = out + tf.stop_gradient(tf.sign(out) - out)

			return

		def apply_dropout():
			nonlocal outputs

			if not self.train:
				return

			if not self.dropout:
				return

			args = {"rate": self.dropout_rate}
			outputs = tf.nn.dropout(outputs, **args)

			return


		# main
		get_weights()
		push()
		apply_dropout()

		return outputs

	def clip_weights(self):
		args = {"clip_value_min": 0.0, "clip_value_max": 1.0}
		clipped_w = tf.clip_by_value(self.w, **args)
		self.w.assign(clipped_w)

		return

	def save(self, f):
		# weights
		vals = self.w.numpy().reshape(-1, order="C")
		vals_bin = np.zeros(vals.__len__(), dtype=np.uint8)
		vals_bin[vals > 0] = 1
		vals_write = np.packbits(vals_bin)
		f.write(vals_write)

		return
