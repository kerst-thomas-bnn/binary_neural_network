#ifndef KNOCKOUT_H
#define KNOCKOUT_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t* _knockout_init(const struct tensor_t*, unsigned char**);

void knockout_push(const struct layer_t*, const struct tensor_t*);

void knockout_destroy(struct layer_t*);


#endif
