#include "../../../../include/debug.h"
#include "../../../../include/memory.h"
#include "../../../../tensor/tensor.h"
#include "../../../../bits/bits.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../knockout.h"

#include <string.h>
#include <stdlib.h>


// static functions
static void set_geometry(struct layer_t* p, const struct tensor_t* input)
{
	// fetch shape
	unsigned int rank = tensor_get_rank(input);
	unsigned int* dims = tensor_get_dims(input);

	// set shape
	p->shape_input = shape_create(rank, dims);
	p->shape_output = shape_create(rank, dims);
}


// functions
struct layer_t* _knockout_init(const struct tensor_t* input,
                               unsigned char** weights)
{
	// allocate
	struct layer_t* p = SALLOC(p);
	p->layer_type = LAYER_TYPE_KNOCKOUT;

	// geometry
	set_geometry(p, input);

	// weights
	p->weights = weights_create_naive(p->shape_output, weights);

	// output
	p->output = output_create(p->shape_output);

	// functions
	p->push = &knockout_push;
	p->destroy = &knockout_destroy;

	return p;
}
