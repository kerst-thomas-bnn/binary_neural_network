#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../knockout.h"


// functions
void knockout_push(const struct layer_t* l, const struct tensor_t* p)
{
	// fetch pointers
	unsigned char* weights_raw = weights_get_raw(l->weights);
	unsigned char* input_raw = tensor_get_bytes(p);
	unsigned char* output = output_get_prediction_bytes(l->output);

	// iterate
	unsigned int n = tensor_get_n_bytes(p);
	for (unsigned int i = 0; i < n; ++i)
		output[i] = input_raw[i] & weights_raw[i];
}
