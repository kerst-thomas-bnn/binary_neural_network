#ifndef CONVOLUTION_DENSE_H
#define CONVOLUTION_DENSE_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t* convolution_dense_init(const struct tensor_t*,
                                       const struct tensor_t*,
                                       unsigned int,
                                       unsigned char**);

void convolution_dense_destroy(struct layer_t*);


#endif
