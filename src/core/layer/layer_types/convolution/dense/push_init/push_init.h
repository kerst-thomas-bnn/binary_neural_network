#ifndef CONVOLUTION_DENSE_PUSH_INIT_H
#define CONVOLUTION_DENSE_PUSH_INIT_H


// forward declarations
struct layer_t;


// functions
void convolution_dense_push_init(struct layer_t*);


#endif
