#include "../../../../../../../include/debug.h"
#include "../../../../../../../include/bitwise.h"
#include "../../../../../../../tensor/tensor.h"
#include "../../../../../../def/layer_def.h"
#include "../../../../../../def/weights/weights.h"
#include "../../../../../../def/output/output.h"
#include "../push.h"

#include <string.h>


// static functions
static inline unsigned char* get_wrt(const struct layer_t* l)
{
	return output_get_prediction_bytes(l->output);
}

static inline unsigned int get_offset(const struct tensor_t* p)
{
	unsigned int n_bits = tensor_get_n_bits(p);
	unsigned int offset = n_bits / 2 + n_bits % 2;

	return offset;
}

static inline unsigned int get_internal_field(const struct layer_t* l,
                                              unsigned int k,
                                              const unsigned char* input,
                                              BITS_XNOR_DECL(fp))
{
	const struct bits_t* bits = weights_get_filter_bits(l->weights, k);

	return 2 * fp(bits, input);
}


// functions
void convolution_dense_push(const struct layer_t* l,
                            const struct tensor_t* p,
                            BITS_XNOR_DECL(fp))
{
	// initialise
	unsigned char* wrt = get_wrt(l);
	const unsigned char* input = tensor_get_bytes(p);

	// offsets and filters
	unsigned int offset = get_offset(p);
	unsigned int n_filters = weights_get_n_filters(l->weights);
	unsigned int* thresholds = weights_get_thresholds(l->weights);

	// count vars
	unsigned char val = 0;
	unsigned int pos_bit = 0;
	unsigned int pos_byte = 0;

	// push
	for (unsigned int i = 0; i < n_filters; ++i) {
		unsigned int field = get_internal_field(l, i, input, fp);
		if (!(field > thresholds[i] + offset))
			val |= 1;

		if (++pos_bit == 8) {
			pos_bit = 0;
			wrt[pos_byte++] = val;
			val = 0;
		} else {
			val <<= 1;
		}
	}

	// TODO: treat the case that IS_DIV8(n_filters) = false, e.g. shift the
	// last byte to the left
}
