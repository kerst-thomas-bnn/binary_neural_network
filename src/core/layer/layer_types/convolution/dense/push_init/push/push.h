#ifndef CONVOLUTION_DENSE_PUSH_H
#define CONVOLUTION_DENSE_PUSH_H


// macros
#define BITS_XNOR_DECL(X)  \
	unsigned int (*X)(const struct bits_t*, const unsigned char*)


// forward declarations
struct layer_t;
struct tensor_t;


// functions
void convolution_dense_push(const struct layer_t*,
                            const struct tensor_t*,
                            BITS_XNOR_DECL());


#endif
