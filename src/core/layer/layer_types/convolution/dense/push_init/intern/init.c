#include "../../../../../../include/debug.h"
#include "../../../../../../bits/bits.h"
#include "../../../../../../opt/opt.h"
#include "../../../../../def/layer_def.h"
#include "../push_init.h"
#include "../push/push.h"


// static functions
static inline void push_uc(const struct layer_t* l, const struct tensor_t* p)
{
	convolution_dense_push(l, p, &bits_xnor_uc);
}

static inline void push_ui(const struct layer_t* l, const struct tensor_t* p)
{
	convolution_dense_push(l, p, &bits_xnor_ui);
}

static inline void push_ul(const struct layer_t* l, const struct tensor_t* p)
{
	convolution_dense_push(l, p, &bits_xnor_ul);
}

static inline void push_ull(const struct layer_t* l, const struct tensor_t* p)
{
	convolution_dense_push(l, p, &bits_xnor_ull);
}

static void set_push_function(struct layer_t* p, unsigned int k)
{
	void (*funs[])(const struct layer_t*, const struct tensor_t*) =
	{
		&push_uc,
		&push_ui,
		&push_ul,
		&push_ull
	};

	p->push = funs[k];
}


// functions
void convolution_dense_push_init(struct layer_t* l)
{
	opt_add_optimisation(l, &set_push_function, 0, 4);
}
