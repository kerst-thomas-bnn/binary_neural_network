#ifndef CONVOLUTION_H
#define CONVOLUTION_H


// forward declarations
struct layer_t;
struct tensor_t;


// functions
struct layer_t* _convolution_init(const struct tensor_t*, const struct tensor_t*, unsigned int, unsigned char**);


#endif
