#ifndef CONVOLUTION_1X1_H
#define CONVOLUTION_1X1_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t* convolution_1x1_init(const struct tensor_t*,
                                     const struct tensor_t*,
                                     unsigned int,
                                     unsigned char**);

void set_convolution_1x1_push(struct layer_t*);

void convolution_1x1_destroy(struct layer_t*);


#endif
