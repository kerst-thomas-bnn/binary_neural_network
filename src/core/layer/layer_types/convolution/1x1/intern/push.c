#include "../../../../../include/debug.h"
#include "../../../../../opt/opt.h"
#include "../../../../../tensor/tensor.h"
#include "../../../../../bits/bits.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/weights/weights.h"
#include "../../../../def/output/output.h"
#include "../1x1.h"


// macros
#define POP_CNT_FUN(X)  \
	unsigned int (*X)(const struct bits_t*, const unsigned char*)


// static functions
static inline void apply_all_filters(const struct layer_t* l,
                                     const struct tensor_t* p,
                                     unsigned int n_filters,
                                     unsigned int num_byte,
                                     POP_CNT_FUN(ppcnt_f))
{
	const struct tensor_t* weights;
	for (unsigned int i = 0; i < n_filters; ++i) {
		weights = weights_get_filter_weights(l->weights, i);
		unsigned int thr = weights_get_filter_threshold(l->weights, i);
		output_threshold_set(l->output, 0, thr);

		const struct bits_t* bits = tensor_get_bits(weights);
		const unsigned char* bytes = tensor_get_bytes(p) + num_byte;
		unsigned int popcount = ppcnt_f(bits,bytes);

		output_internal_field_set(l->output, 0, 2 * popcount);
		l->activate(l->output, 0);
	}
}

static inline void iterate_through_input(const struct layer_t* l,
                                         const struct tensor_t* p,
				         POP_CNT_FUN(ppcnt_f))
{
	// initialise
	output_reset(l->output);

	// set up the push variables
	unsigned int n_bytes = weights_get_filter_n_bytes(l->weights);
	unsigned int n_filters = weights_get_n_filters(l->weights);
	const struct tensor_t* weights;
	weights = weights_get_filter_weights(l->weights, 0);
	output_threshold_offset_set(l->output, 0, weights);

	// iterate trough each filter
	for (unsigned int i = 0; i < tensor_get_n_bytes(p); i += n_bytes)
		apply_all_filters(l, p, n_filters, i, ppcnt_f);
}

static void push_uc(const struct layer_t* l, const struct tensor_t* p)
{
	iterate_through_input(l, p, &bits_xnor_uc);
}

static void push_ui(const struct layer_t* l, const struct tensor_t* p)
{
	iterate_through_input(l, p, &bits_xnor_ui);
}

static void push_ul(const struct layer_t* l, const struct tensor_t* p)
{
	iterate_through_input(l, p, &bits_xnor_ul);
}

static void push_ull(const struct layer_t* l, const struct tensor_t* p)
{
	iterate_through_input(l, p, &bits_xnor_ull);
}

static void pick_push_fun(struct layer_t* l, unsigned int n)
{
	void (*funs[])(const struct layer_t*, const struct tensor_t*) = {
		&push_uc,
		&push_ui,
		&push_ul,
		&push_ull
	};
	l->push = funs[n];
}


// functions
void set_convolution_1x1_push(struct layer_t* l)
{
	opt_add_optimisation(l, &pick_push_fun, 0, 4);
}
