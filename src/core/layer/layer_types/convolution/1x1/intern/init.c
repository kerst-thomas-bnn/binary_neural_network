#include "../../../../../include/memory.h"
#include "../../../../../tensor/tensor.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/shape/shape.h"
#include "../../../../def/weights/weights.h"
#include "../../../../def/output/output.h"
#include "../../../../activations/activations.h"
#include "../1x1.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>


// static functions
static void set_shape_kernel(struct layer_t* p,
                             unsigned int rank,
                             const unsigned int* dims)
{
	unsigned int rank_i = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);

	unsigned int dims_expanded[rank + 1];
	memcpy(dims_expanded, dims, (rank - 1) * sizeof(*dims_expanded));
	dims_expanded[rank - 1] = dims_i[rank_i - 1];
	dims_expanded[rank] = dims[rank - 1];

	p->shape_kernel = shape_create(rank + 1, dims_expanded);
}

static void set_shape_output(struct layer_t* p)
{
	unsigned int rank_i = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);
	unsigned int rank_k = shape_get_rank(p->shape_kernel);;
	unsigned int* dims_k = shape_get_dims(p->shape_kernel);

	p->shape_output = shape_create(rank_i, dims_i);
	unsigned int* dims_o = shape_get_dims(p->shape_output);

	dims_o[rank_i - 1] = dims_k[rank_k - 1];
	for (unsigned int i = 0; i < rank_i - 1; ++i) {
		dims_o[i] = dims_i[i] / p->strides;
		if (dims_o[i] * p->strides < dims_i[i])
			++dims_o[i];
	}
}


// functions
struct layer_t* convolution_1x1_init(const struct tensor_t* input,
                                     const struct tensor_t* kernel,
                                     unsigned int strides,
                                     unsigned char** weights)
{
	// allocate
	struct layer_t* p = SALLOC(p);
	p->layer_type = LAYER_TYPE_CONVOLUTION;
	p->strides = strides;

	// geometry
	p->shape_input = shape_create(
		tensor_get_rank(input),
		tensor_get_dims(input)
	);
	set_shape_kernel(p, tensor_get_rank(kernel), tensor_get_dims(kernel));
	set_shape_output(p);
	p->padding = 0;

	// weights
	p->weights = weights_create(p->shape_kernel, p->shape_output, weights);

	// output
	p->output = output_create(p->shape_output);

	// functions
	set_convolution_1x1_push(p);
	p->destroy = &convolution_1x1_destroy;
	p->activate = &activate_threshold;

	return p;
}
