#include "../../../../include/bitwise.h"
#include "../../../../tensor/tensor.h"
#include "../convolution.h"
#include "../dense/dense.h"
#include "../1x1/1x1.h"
#include "../2d/2d.h"
#include "../general/general.h"

#include <stdbool.h>


// static functions
static bool is_dense(const struct tensor_t* input) {
	for (unsigned int i = 0; i < tensor_get_rank(input) - 1; ++i)
		if (tensor_get_dims(input)[i] > 1)
			return false;

	return true;
}

static bool is_1x1(const struct tensor_t* input, const struct tensor_t* kernel, unsigned int strides) {
	// strides
	if (strides != 1)
		return false;

	// ranks
	if (tensor_get_rank(kernel) != 3)
		return false;

	// dims
	for (unsigned int i = 0; i < 2; ++i)
		if (tensor_get_dims(kernel)[i] != 1)
			return false;

	// divisible by 8
	if (!IS_DIV8(tensor_get_dims(input)[2]))
		return false;

	return true;
}

static bool is_2d(const struct tensor_t* input) {
	if (tensor_get_rank(input) != 3)
		return false;

	return true;
}


// functions
struct layer_t* _convolution_init(const struct tensor_t* input, const struct tensor_t* kernel, unsigned int strides, unsigned char** weights) {
	if (is_dense(input))
		return convolution_dense_init(input, kernel, strides, weights);

	if (is_1x1(input, kernel, strides))
		return convolution_1x1_init(input, kernel, strides, weights);

	if (is_2d(input))
		return convolution_2d_init(input, kernel, strides, weights);

	return convolution_general_init(input, kernel, strides, weights);
}
