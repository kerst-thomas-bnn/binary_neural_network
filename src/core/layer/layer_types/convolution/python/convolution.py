class convolution(layers.Layer):
	def __init__(self,
		     kernel,
		     strides=1,
		     activate=True,
		     dropout=False,
		     dropout_rate=0.0
		):
		super(convolution, self).__init__()
		self.kernel = kernel
		self.strides = strides
		self.activate = activate
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		return

	def build(self, input_shape):
		n_filters_in = input_shape[-1]
		w_shape = [*self.kernel]
		w_shape.insert(-1, input_shape[-1])

		self.clip_val = int(np.prod(w_shape[:-1]) / 2)

		# weights and biases
		self.w = self.add_weight(
			shape=w_shape,
			initializer=tf.random_uniform_initializer(
				minval=-1.0,
				maxval=1.0
			),
			trainable=True
		)
		self.b = self.add_weight(
			shape=(self.kernel[-1], ),
			initializer=tf.zeros_initializer(),
			trainable=True
		)

		return

	def call(self, inputs):
		# functions
		def get_binarised_weights():
			# binarise weights - w
			w = self.w
			w = w + tf.stop_gradient(tf.sign(w) - w)
			w = w + tf.stop_gradient(tf.add(w, 0.5) - w)
			w = w + tf.stop_gradient(tf.sign(w) - w)

			# binarise weights - b
			b = self.b
			cv = self.clip_val
			b = b + tf.stop_gradient(tf.multiply(b, cv) - b)
			b = b + tf.stop_gradient(tf.math.round(b) - b)
			b_clipped = tf.clip_by_value(b, -cv, cv)
			b = b + tf.stop_gradient(b_clipped - b)

			return w, b

		def apply_padding():
			# import
			import math

			# shapes
			shape_input = inputs.shape[1:-1]
			shape_kernel = self.w.shape[0:-2]
			shape_output = [
				math.ceil(float(_s) / float(self.strides))
				for _s
				in shape_input
			]

			# pads
			pads_total = [
				max(
					(shape_output[i] - 1)
					* self.strides
					+ shape_kernel[i]
					- shape_input[i],
					0
				)
				for i
				in range(shape_output.__len__())
			]
			pads_left = [int(_t / 2) for _t in pads_total]
			pads_right = [_t - _l
				for _t, _l
				in
				zip(pads_total, pads_left)
			]
			pads_paired = [
				[_l, _r]
				for _l, _r
				in zip(pads_left, pads_right)
			]

			# apply padding
			paddings = tf.constant([[0, 0], *pads_paired, [0, 0]])
			padded = tf.pad(
				inputs,
				paddings,
				"CONSTANT",
				constant_values=-1.0
			)

			return padded

		def activate(ip):
			op = ip + tf.stop_gradient(tf.sign(ip) - ip)
			op = op + tf.stop_gradient(tf.add(op, 0.5) - op)
			op = op + tf.stop_gradient(tf.sign(op) - op)

			return op

		def apply_dropout(ip):
			return tf.nn.dropout(ip, rate=self.dropout_rate)


		# main
		w, b = get_binarised_weights()
		input_padded = apply_padding();

		# convolution
		outputs = tf.nn.convolution(
			input=input_padded,
			filters=w,
			strides=self.strides,
			padding="VALID"
		)
		outputs = tf.nn.bias_add(outputs, b)

		# activation
		if self.activate:
			outputs = activate(outputs)

		# during training
		if self.train and self.dropout:
			outputs = apply_dropout(outputs)

		return outputs

	def clip_weights(self):
		clipped = tf.clip_by_value(
			self.w,
			clip_value_min=-1.0,
			clip_value_max=1.0
		)
		self.w.assign(clipped)

		return

	def save(self, f):
		# weights
		vals = self.w.numpy().reshape(-1, order="C")
		vals_bin = np.zeros(vals.__len__(), dtype=np.uint8)
		vals_bin[vals > 0] = 1
		vals_write = np.packbits(vals_bin)
		f.write(vals_write)

		# biases
		vals = self.b.numpy().reshape(-1, order="C")
		vals_scaled = np.multiply(vals, self.clip_val)
		vals_scaled = np.round(vals_scaled)
		vals = np.clip(vals_scaled, -self.clip_val, self.clip_val)
		vals = np.add(vals, self.clip_val)
		vals = vals.astype(np.uint32)
		f.write(vals)

		return
