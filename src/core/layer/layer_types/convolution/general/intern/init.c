#include "../../../../../include/debug.h"
#include "../../../../../include/memory.h"
#include "../../../../../tensor/tensor.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/shape/shape.h"
#include "../../../../def/padding/padding.h"
#include "../../../../def/weights/weights.h"
#include "../../../../def/output/output.h"
#include "../../../../activations/activations.h"
#include "../general.h"

#include <stdlib.h>
#include <string.h>


// static functions
static void set_shape_kernel(struct layer_t* p,
                             unsigned int rank,
                             const unsigned int* dims) {
	unsigned int rank_i = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);

	unsigned int dims_expanded[rank + 1];
	memcpy(dims_expanded, dims, (rank - 1) * sizeof(*dims_expanded));
	dims_expanded[rank - 1] = dims_i[rank_i - 1];
	dims_expanded[rank] = dims[rank - 1];

	p->shape_kernel = shape_create(rank + 1, dims_expanded);

	return;
}

static void set_shape_output(struct layer_t* p) {
	unsigned int rank_i = shape_get_rank(p->shape_input);
	unsigned int* dims_i = shape_get_dims(p->shape_input);
	unsigned int rank_k = shape_get_rank(p->shape_kernel);;
	unsigned int* dims_k = shape_get_dims(p->shape_kernel);

	p->shape_output = shape_create(rank_i, dims_i);
	unsigned int* dims_o = shape_get_dims(p->shape_output);

	dims_o[rank_i - 1] = dims_k[rank_k - 1];
	for (unsigned int i = 0; i < rank_i - 1; ++i) {
		dims_o[i] = dims_i[i] / p->strides;
		if (dims_o[i] * p->strides < dims_i[i])
			++dims_o[i];
	}

	return;
}


// functions
struct layer_t* convolution_general_init(const struct tensor_t* input,
                                         const struct tensor_t* kernel,
                                         unsigned int strides,
                                         unsigned char** weights) {
	// allocate
	struct layer_t* p;
	MALLOC(p, 1);
	p->layer_type = LAYER_TYPE_CONVOLUTION;
	p->strides = strides;

	// geometry
	p->shape_input = shape_create(
		tensor_get_rank(input),
		tensor_get_dims(input)
	);
	set_shape_kernel(p, tensor_get_rank(kernel), tensor_get_dims(kernel));
	set_shape_output(p);
	p->padding = padding_create(
		p->shape_input,
		p->shape_kernel,
		p->shape_output,
		p->strides
	);

	// weights
	p->weights = weights_create(p->shape_kernel, p->shape_output, weights);

	// output
	p->output = output_create(p->shape_output);

	// functions
	p->push = &convolution_general_push;
	p->destroy = &convolution_general_destroy;
	p->activate = &activate_threshold;

	return p;
}
