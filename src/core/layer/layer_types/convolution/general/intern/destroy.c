#include "../../../../../include/memory.h"
#include "../../../../../tensor/tensor.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/shape/shape.h"
#include "../../../../def/padding/padding.h"
#include "../../../../def/weights/weights.h"
#include "../../../../def/output/output.h"
#include "../general.h"

#include <stdlib.h>


// functions
void convolution_general_destroy(struct layer_t* p) {
	// geometry
	shape_destroy(p->shape_input);
	shape_destroy(p->shape_kernel);
	shape_destroy(p->shape_output);
	padding_destroy(p->padding);

	// weights
	weights_destroy(p->weights);

	// output
	output_destroy(p->output);

	// layer
	FREE(p);

	return;
}
