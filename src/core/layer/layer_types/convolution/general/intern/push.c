#include "../../../../../include/debug.h"
#include "../../../../../tensor/tensor.h"
#include "../../../../../bits/bits.h"
#include "../../../../def/layer_def.h"
#include "../../../../def/shape/shape.h"
#include "../../../../def/padding/padding.h"
#include "../../../../def/weights/weights.h"
#include "../../../../def/output/output.h"
#include "../general.h"

#include <string.h>


// static variables
static const struct tensor_t* input;
static const struct layer_t* layer;
static unsigned int* limits;
static unsigned int* paddings;


// static functions
static inline void iterate_over_filters() {
	// set limits
	unsigned int rank = tensor_get_rank(input);
	unsigned int* dims = tensor_get_dims(input);
	limits[2 * rank - 2] = 0;
	limits[2 * rank - 1] = dims[rank - 1];

	// get slice
	unsigned int n_bytes = weights_get_filter_n_bytes(layer->weights);
	unsigned char slice_vals[n_bytes];
	memset(slice_vals, 0, n_bytes);
	tensor_slice_vals(input, limits, paddings, slice_vals);

	// compute each individual bit for each filter applied to `slice`
	unsigned int n_filters = weights_get_n_filters(layer->weights);
	output_threshold_offset_set(
		layer->output,
		0,
		weights_get_filter_weights(layer->weights, 0)
	);
	for (unsigned int i = 0; i < n_filters; ++i) {
		const struct tensor_t* f_weights = weights_get_filter_weights(
			layer->weights,
			i
		);
		output_threshold_set(
			layer->output,
			0,
			weights_get_filter_threshold(layer->weights, i)
		);
		output_internal_field_set(
			layer->output,
			0,
			2 * bits_xnor_uc(
				tensor_get_bits(f_weights),
				slice_vals
			)
		);
		layer->activate(layer->output, 0);
	}

	return;
}

static inline void iterate_over_dimension(unsigned int dim) {
	// is this the last dim? if yes, then compute the new tensor value for
	// each filter
	if (dim == output_get_prediction_rank(layer->output) - 1) {
		iterate_over_filters();

		return;
	}

	// if no, then keep going
	unsigned int* dims = shape_get_dims(layer->shape_kernel);
	unsigned int n = tensor_get_dims(input)[dim];
	for (unsigned int i = 0; i < n; i += layer->strides) {
		limits[2 * dim] = i;
		limits[2 * dim + 1] = i + dims[dim];
		iterate_over_dimension(dim + 1);
	}

	return;
}


// functions
void convolution_general_push(const struct layer_t* l,
                              const struct tensor_t* p) {
	DISP("[WARNING] Optimise convolution_general_push before use", s);
	// init
	output_reset(l->output);

	// set static variables
	layer = l;
	input = p;

	// initiate limits and pads
	unsigned int rank = shape_get_rank(l->shape_output);
	unsigned int _limits[2 * rank];
	unsigned int _paddings[2 * (rank - 1)];
	limits = _limits;
	paddings = _paddings;
	for (unsigned int i = 0; i < rank - 1; ++i) {
		paddings[2 * i] = padding_get_left(l->padding, i);
		paddings[2 * i + 1] = padding_get_right(l->padding, i);
	}

	// fill in the contents
	iterate_over_dimension(0);

	return;
}
