#ifndef CONVOLUTION_GENERAL_H
#define CONVOLUTION_GENERAL_H


// forward declarations
struct tensor_proto_t;
struct tensor_t;
struct layer_t;


// functions
struct layer_t* convolution_general_init(const struct tensor_t*, const struct tensor_t*, unsigned int, unsigned char**);

void convolution_general_push(const struct layer_t*, const struct tensor_t*);

void convolution_general_destroy(struct layer_t*);


#endif
