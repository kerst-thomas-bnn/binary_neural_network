#ifndef CONVOLUTION_2D_PUSH_H
#define CONVOLUTION_2D_PUSH_H


// forward declarations
struct layer_t;


// functions
void convolution_2d_set_push_general(struct layer_t*);

void convolution_2d_set_push_padded(struct layer_t*);

void convolution_2d_set_push_unpadded(struct layer_t*);


#endif
