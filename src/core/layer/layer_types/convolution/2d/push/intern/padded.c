#include "../../../../../../include/debug.h"
#include "../../../../../../include/bitwise.h"
#include "../../../../../../tensor/tensor.h"
#include "../../../../../../bits/bits.h"
#include "../../../../../../opt/opt.h"
#include "../../../../../def/layer_def.h"
#include "../../../../../def/shape/shape.h"
#include "../../../../../def/padding/padding.h"
#include "../../../../../def/weights/weights.h"
#include "../../../../../def/output/output.h"
#include "../push.h"

#include <string.h>
#include <stdio.h>
#include <stdbool.h>


// macros
#define POP_CNT_FUN(X)  \
	unsigned int (*X)(const struct bits_t*, const unsigned char*)


// static functions
static inline bool skip_row(bool condition_1,
                            bool condition_2,
                            unsigned char* target,
                            unsigned int n_bytes_per_row)
{
	if (condition_1 || condition_2) {
		memset(target, 0, n_bytes_per_row);

		return true;
	}

	return false;
}

static inline bool skip_col(bool condition_1,
                            bool condition_2,
                            unsigned char* target,
                            unsigned int n_bytes_per_col)
{
	if (condition_1 || condition_2) {
		memset(target, 0, n_bytes_per_col);

		return true;
	}

	return false;
}

static inline void push_filters(const struct layer_t* layer,
                                const unsigned char* slice_vals,
                                unsigned int n_filters,
                                const unsigned int* thresholds,
                                const struct bits_t** bits,
                                POP_CNT_FUN(xnor_f))
{
	for (unsigned int i = 0; i < n_filters; ++i) {
		output_threshold_set(layer->output, 0, thresholds[i]);
		output_internal_field_set(layer->output, 0, 2 * xnor_f(bits[i], slice_vals));
		layer->activate(layer->output, 0);
	}
}

static inline void iterate_over_kernel(const struct layer_t* layer,
                                       unsigned int row,
                                       unsigned int col,
                                       unsigned int padding_rows,
                                       unsigned int padding_cols,
                                       unsigned int byte_offset_row,
                                       unsigned int byte_offset_col,
                                       const unsigned int* input_dims,
                                       const unsigned int* kernel_dims,
                                       unsigned char* slice_vals,
                                       unsigned int n_bytes_per_row,
                                       unsigned int n_bytes_per_col,
                                       const unsigned char* input_bytes,
                                       unsigned int n_filters,
                                       const unsigned int* thresholds,
                                       const struct bits_t** bits,
                                       POP_CNT_FUN(xnor_f))
{
	for (unsigned int i = 0; i < kernel_dims[0]; ++i) {
		unsigned char* target = slice_vals + i * n_bytes_per_row;
		unsigned int row_shifted = row + i - padding_rows;
		if (skip_row(padding_rows > row + i, row_shifted >= input_dims[0], target, n_bytes_per_row))
			continue;

		for (unsigned int j = 0; j < kernel_dims[1]; ++j) {
			unsigned char* target = slice_vals + i * n_bytes_per_row + j * n_bytes_per_col;
			unsigned int col_shifted = col + j - padding_cols;
			if (skip_col(padding_cols > col + j, col_shifted >= input_dims[1], target, n_bytes_per_col))
				continue;

			const unsigned char* src = input_bytes + row_shifted * byte_offset_row + col_shifted * byte_offset_col;
			memcpy(target, src, n_bytes_per_col);
		}
	}

	push_filters(layer, slice_vals, n_filters, thresholds, bits, xnor_f);
}

static void push(const struct layer_t* layer,
                 const struct tensor_t* input,
                 POP_CNT_FUN(xnor_f))
{
	// init
	output_reset(layer->output);

	// dimensions
	unsigned int* input_dims = tensor_get_dims(input);
	unsigned int* kernel_dims = shape_get_dims(layer->shape_kernel);
	unsigned char* input_bytes = tensor_get_bytes(input);

	// filters
	unsigned int n_filters = weights_get_n_filters(layer->weights);
	output_threshold_offset_set(layer->output, 0, weights_get_filter_weights(layer->weights, 0));
	const unsigned int* thresholds = weights_get_thresholds(layer->weights);
	const struct bits_t* bits[n_filters];
	for (unsigned int i = 0; i < n_filters; ++i)
		bits[i] = weights_get_filter_bits(layer->weights, i);

	// padding
	unsigned int padding_rows = padding_get_left(layer->padding, 0);
	unsigned int padding_cols = padding_get_left(layer->padding, 1);

	// slice
	unsigned int n_bytes = weights_get_filter_n_bytes(layer->weights);
	unsigned char slice_vals[n_bytes];
	unsigned int byte_offset_row = DIV8(tensor_get_index_offset(input)[0]);
	unsigned int byte_offset_col = DIV8(tensor_get_index_offset(input)[1]);
	unsigned int n_bytes_per_row = DIV8(input_dims[2]) * kernel_dims[1];
	unsigned int n_bytes_per_col = DIV8(input_dims[2]);

	// iterate over image
	for (unsigned int row = 0; row < input_dims[0]; row += layer->strides)
		for (unsigned int col = 0; col < input_dims[1]; col += layer->strides)
			iterate_over_kernel(layer, row, col, padding_rows, padding_cols, byte_offset_row, byte_offset_col, input_dims, kernel_dims, slice_vals, n_bytes_per_row, n_bytes_per_col, input_bytes, n_filters, thresholds, bits, xnor_f);
}

static void push_uc(const struct layer_t* layer, const struct tensor_t* input)
{
	return push(layer, input, &bits_xnor_uc);
}

static void push_ui(const struct layer_t* layer, const struct tensor_t* input)
{
	return push(layer, input, &bits_xnor_ui);
}

static void push_ul(const struct layer_t* layer, const struct tensor_t* input)
{
	return push(layer, input, &bits_xnor_ul);
}

static void push_ull(const struct layer_t* layer, const struct tensor_t* input)
{
	return push(layer, input, &bits_xnor_ull);
}

static void set_push_function(struct layer_t* layer, unsigned int nr)
{
	void (*functions[])(const struct layer_t*, const struct tensor_t*) = {
		&push_uc,
		&push_ui,
		&push_ul,
		&push_ull
	};
	layer->push = functions[nr];
}


// functions
void convolution_2d_set_push_padded(struct layer_t* layer)
{
	opt_add_optimisation(layer, &set_push_function, 0, 4);
}
