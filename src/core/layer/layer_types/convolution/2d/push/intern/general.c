#include "../../../../../../include/debug.h"
#include "../../../../../../tensor/tensor.h"
#include "../../../../../../bits/bits.h"
#include "../../../../../def/layer_def.h"
#include "../../../../../def/shape/shape.h"
#include "../../../../../def/padding/padding.h"
#include "../../../../../def/weights/weights.h"
#include "../../../../../def/output/output.h"
#include "../push.h"

#include <string.h>


// static functions
void push(const struct layer_t* layer, const struct tensor_t* input) {
	DISP("[WARNING] Optimise convolution_2d_push_general before use", s);
	// init
	output_reset(layer->output);

	// dimensions
	unsigned int* input_dims = tensor_get_dims(input);
	unsigned int* kernel_dims = shape_get_dims(layer->shape_kernel);
	unsigned int limits[6] = {0, 0, 0, 0, 0, input_dims[2]};

	// bytes
	unsigned int n_bytes = weights_get_filter_n_bytes(layer->weights);
	unsigned char slice_vals[n_bytes];

	// padding
	unsigned int paddings[4] = {
		padding_get_left(layer->padding, 0),
		padding_get_right(layer->padding, 0),
		padding_get_left(layer->padding, 1),
		padding_get_right(layer->padding, 1)
	};

	// filters
	unsigned int n_filters = weights_get_n_filters(layer->weights);
	output_threshold_offset_set(layer->output, 0, weights_get_filter_weights(layer->weights, 0));
	const unsigned int* thresholds = weights_get_thresholds(layer->weights);
	const struct bits_t* bits[n_filters];
	for (unsigned int i = 0; i < n_filters; ++i)
		bits[i] = weights_get_filter_bits(layer->weights, i);

	// iterate over rows
	for (unsigned int row = 0; row < input_dims[0]; row += layer->strides) {
		// set limits
		limits[0] = row;
		limits[1] = row + kernel_dims[0];

		// iterate over columns
		for (unsigned int col = 0; col < input_dims[1]; col += layer->strides) {
			// set limits
			limits[2] = col;
			limits[3] = col + kernel_dims[1];

			// get slice
			memset(slice_vals, 0, n_bytes);
			tensor_slice_vals(input, limits, paddings, slice_vals);

			// compute each individual bit for each filter applied to `slice`
			for (unsigned int i = 0; i < n_filters; ++i) {
				output_threshold_set(layer->output, 0, thresholds[i]);
				output_internal_field_set(layer->output, 0, 2 * bits_xnor_uc(bits[i], slice_vals));
				layer->activate(layer->output, 0);
			}
		}
	}

	return;
}


// functions
void convolution_2d_set_push_general(struct layer_t* layer) {
	layer->push = &push;
}
