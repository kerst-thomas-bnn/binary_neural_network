#ifndef CONVOLUTION_2D_H
#define CONVOLUTION_2D_H


// forward declarations
struct tensor_t;
struct layer_t;


// functions
struct layer_t* convolution_2d_init(const struct tensor_t*, const struct tensor_t*, unsigned int, unsigned char**);

void convolution_2d_destroy(struct layer_t*);


#endif
