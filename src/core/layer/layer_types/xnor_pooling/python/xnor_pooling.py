class xnor_pooling(layers.Layer):
	def __init__(self, kernel, dropout=False, dropout_rate=0.0):
		super(xnor_pooling, self).__init__()
		self.kernel = kernel
		self.strides = kernel
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		return

	def call(self, inputs):
		# nonlocals
		inputs_padded = None
		outputs = None


		# functions
		def apply_padding():
			import math
			nonlocal inputs_padded

			# shapes
			shape_input = inputs.shape[1:-1]
			shape_kernel = self.kernel
			shape_output = [
				math.ceil(float(_i) / float(_k))
				for _i, _k
				in zip(shape_input, shape_kernel)
			]

			# pads
			pads_total = [
				max(_o * _k - _i, 0)
				for _i, _k, _o
				in zip(shape_input, shape_kernel, shape_output)
			]
			pads_left = [
				int(_t / 2)
				for _t
				in pads_total
			]
			pads_right = [
				_t - _l
				for _t, _l
				in zip(pads_total, pads_left)
			]
			pads_paired = [
				[_l, _r]
				for _l, _r
				in zip(pads_left, pads_right)
			]

			# apply padding
			paddings = tf.constant([[0, 0], *pads_paired, [0, 0]])
			inputs_padded = tf.pad(
				inputs,
				paddings,
				"CONSTANT",
				constant_values=-1.0
			)

			return

		def pool():
			nonlocal outputs

			for row in range(self.strides[0]):
				for col in range(self.strides[1]):
					slice = tf.strided_slice(
						inputs_padded,
						[0, row, col, 0],
						inputs_padded.shape,
						[1, *self.strides, 1]
					)
					if outputs is None:
						outputs = slice
					else:
						outputs = tf.multiply(
								outputs,
								slice
							)

			outputs = tf.multiply(outputs, -1)

			return

		def apply_dropout():
			nonlocal outputs

			if not self.train:
				return

			if not self.dropout:
				return

			outputs = tf.nn.dropout(
				input_padded,
				rate=self.dropout_rate
			)

			return


		# main
		apply_padding()
		pool()
		apply_dropout()

		return outputs

	def clip_weights(self):
		return

	def save(self, f):
		return
