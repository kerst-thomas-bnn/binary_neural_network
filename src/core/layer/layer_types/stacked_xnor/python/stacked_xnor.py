class stacked_xnor(layers.Layer):
	def __init__(self, kernel, dropout=False, dropout_rate=0.0):
		super(stacked_xnor, self).__init__()
		self.kernel = kernel
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		return

	def build(self, input_shape):
		# args
		shape = [np.prod(input_shape) * self.kernel[0]]
		init = tf.random_uniform_initializer(minval=-1.0, maxval=1.0)
		args = {"shape": shape, "trainable": True, "initializer": init}

		# weights
		self.w_xor = self.add_weight(**args)
		self.w_and = self.add_weight(**args)

		return

	def call(self, inputs):
		# nonlocals
		in_bin = None
		w_xor = None
		w_and = None
		outputs = None


		# functions
		def binarise():
			nonlocal in_bin
			nonlocal w_xor
			nonlocal w_and

			# input
			prod = np.prod(inputs.shape[1:])
			shape = [inputs.shape[0], prod]
			in_bin = tf.reshape(inputs, shape=shape)
			in_bin = tf.tile(in_bin, multiples=[1, self.kernel[0]])

			# xor
			w = self.w_xor
			w = w + tf.stop_gradient(tf.sign(w) - w)
			w_xor = w

			# and
			w = self.w_and
			w = w + tf.stop_gradient(tf.sign(w) - w)
			w = tf.add(w, 1.0)
			w = tf.divide(w, 2.0)
			w_and = w

			return

		def build():
			nonlocal outputs

			# H * T
			ht = tf.math.multiply(in_bin, w_xor)
			ht = tf.math.multiply(ht, w_and)

			# (1 - T) * I
			ones = tf.ones(shape=w_and.shape)
			iti = tf.subtract(ones, w_and)
			iti = tf.multiply(iti, in_bin)

			# H * T + (1 - T) * I
			out = tf.add(ht, iti)

			# reshape
			shape = [*inputs.shape]
			shape[-1] *= self.kernel[0]
			outputs = tf.reshape(out, shape=shape)

			return

		def apply_dropout():
			nonlocal outputs

			if not self.train:
				return

			if not self.dropout:
				return

			out = tf.nn.dropout(outputs, rate=self.dropout_rate)
			out = out + tf.stop_gradient(tf.sign(out) - out)
			outputs = out

			return


		# main
		binarise()
		build()
		apply_dropout()

		return outputs

	def clip_weights(self):
		# xor
		args = {"clip_value_min": -1.0, "clip_value_max": 1.0}
		clipped_xor = tf.clip_by_value(self.w_xor, **args)
		self.w_xor.assign(clipped_xor)

		# and
		args = {"clip_value_min": -1.0, "clip_value_max": 1.0}
		clipped_and = tf.clip_by_value(self.w_and, **args)
		self.w_and.assign(clipped_and)

		return

	def save(self, f):
		# xor
		vals = self.w_xor.numpy().reshape(-1, order="C")
		vals_bin = np.zeros(vals.__len__(), dtype=np.uint8)
		vals_bin[vals < 0] = 1
		vals_write = np.packbits(vals_bin)
		f.write(vals_write)

		# and
		vals = self.w_and.numpy().reshape(-1, order="C")
		vals_bin = np.zeros(vals.__len__(), dtype=np.uint8)
		vals_bin[vals < 0] = 1
		vals_write = np.packbits(vals_bin)
		f.write(vals_write)

		return
