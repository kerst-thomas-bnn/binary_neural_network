#ifndef STACKED_XNOR_LAYER_H
#define STACKED_XNOR_LAYER_H


// foward declarations
struct layer_t;
struct tensor_t;


// functions
struct layer_t *_stacked_xnor_init(const struct tensor_t *input,
                                   const struct tensor_t *kernel,
                                   unsigned char **weights);

void stacked_xnor_push_set(struct layer_t *layer);

void stacked_xnor_destroy(struct layer_t *layer);


#endif
