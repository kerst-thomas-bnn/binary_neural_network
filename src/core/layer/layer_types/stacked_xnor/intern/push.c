#include "../../../../include/debug.h"
#include "../../../../tensor/tensor.h"
#include "../../../../opt/opt.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../stacked_xnor.h"


// macros
#define FILTER_FUN(X)  \
	void (X)(unsigned char *tar,  \
                 const unsigned char *src,  \
                 const unsigned char *w_x,  \
                 const unsigned char *w_a,  \
                 unsigned int n)  \


// static functions
static inline FILTER_FUN(apply_filter_uc)
{
	unsigned int i = 0;
	for (; i < n; ++i) {
		unsigned char ht = (src[i] ^ w_x[i]) & ~w_a[i];
		unsigned char iti = src[i] & w_a[i];
		tar[i] = ht | iti;
	}
}

static inline FILTER_FUN(apply_filter_ui)
{
	unsigned int *ti = (unsigned int *)tar;
	unsigned int *si = (unsigned int *)src;
	unsigned int *xi = (unsigned int *)w_x;
	unsigned int *ai = (unsigned int *)w_a;

	unsigned int i = 0;
	for (; i < n / sizeof(unsigned int); ++i) {
		unsigned int ht = (si[i] ^ xi[i]) & ~ai[i];
		unsigned int iti = si[i] & ai[i];
		ti[i] = ht | iti;
	}

	i *= sizeof(unsigned int);
	for (; i < n; ++i) {
		unsigned char ht = (src[i] ^ w_x[i]) & ~w_a[i];
		unsigned char iti = src[i] & w_a[i];
		tar[i] = ht | iti;
	}
}

static inline FILTER_FUN(apply_filter_ul)
{
	unsigned long *ti = (unsigned long *)tar;
	unsigned long *si = (unsigned long *)src;
	unsigned long *xi = (unsigned long *)w_x;
	unsigned long *ai = (unsigned long *)w_a;

	unsigned int i = 0;
	for (; i < n / sizeof(unsigned long); ++i) {
		unsigned long ht = (si[i] ^ xi[i]) & ~ai[i];
		unsigned long iti = si[i] & ai[i];
		ti[i] = ht | iti;
	}

	i *= sizeof(unsigned long);
	for (; i < n; ++i) {
		unsigned char ht = (src[i] ^ w_x[i]) & ~w_a[i];
		unsigned char iti = src[i] & w_a[i];
		tar[i] = ht | iti;
	}
}

static inline FILTER_FUN(apply_filter_ull)
{
	unsigned long long *ti = (unsigned long long *)tar;
	unsigned long long *si = (unsigned long long *)src;
	unsigned long long *xi = (unsigned long long *)w_x;
	unsigned long long *ai = (unsigned long long *)w_a;

	unsigned int i = 0;
	for (; i < n / sizeof(unsigned long long); ++i) {
		unsigned long long ht = (si[i] ^ xi[i]) & ~ai[i];
		unsigned long long iti = si[i] & ai[i];
		ti[i] = ht | iti;
	}

	i *= sizeof(unsigned long long);
	for (; i < n; ++i) {
		unsigned char ht = (src[i] ^ w_x[i]) & ~w_a[i];
		unsigned char iti = src[i] & w_a[i];
		tar[i] = ht | iti;
	}
}

static inline void push(const struct layer_t *layer,
                        const struct tensor_t *input,
                        FILTER_FUN(*fun))
{
	const unsigned char *w_x = weights_get_xor(layer->weights);
	const unsigned char *w_a = weights_get_and(layer->weights);
	const unsigned char *src = tensor_get_bytes(input);
	unsigned char *tar = output_get_prediction_bytes(layer->output);
	unsigned int n = tensor_get_n_bytes(input);
	unsigned int n_filters = shape_get_dims(layer->shape_kernel)[0];

	for (unsigned int i = 0; i < n_filters; ++i)
		fun(&tar[i * n], src, &w_x[i * n], &w_a[i * n], n);
}

static void push_uc(const struct layer_t *layer, const struct tensor_t* input)
{
	push(layer, input, &apply_filter_uc);
}

static void push_ui(const struct layer_t *layer, const struct tensor_t* input)
{
	push(layer, input, &apply_filter_ui);
}

static void push_ul(const struct layer_t *layer, const struct tensor_t* input)
{
	push(layer, input, &apply_filter_ul);
}

static void push_ull(const struct layer_t *layer, const struct tensor_t* input)
{
	push(layer, input, &apply_filter_ull);
}

static void set_push_function(struct layer_t *layer, unsigned int k)
{
	void (*funs[])(const struct layer_t*, const struct tensor_t*) = {
		&push_uc,
		&push_ui,
		&push_ul,
		&push_ull
	};
	layer->push = funs[k];
}


// functions
void stacked_xnor_push_set(struct layer_t *layer)
{
	opt_add_optimisation(layer, &set_push_function, 0, 4);
}
