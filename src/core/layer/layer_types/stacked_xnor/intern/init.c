#include "../../../../include/debug.h"
#include "../../../../include/array.h"
#include "../../../../include/memory.h"
#include "../../../../include/error.h"
#include "../../../../include/bitwise.h"
#include "../../../../tensor/tensor.h"
#include "../../../def/layer_def.h"
#include "../../../def/shape/shape.h"
#include "../../../def/weights/weights.h"
#include "../../../def/output/output.h"
#include "../stacked_xnor.h"
#include "../include/strings.h"

#include <string.h>
#include <stdlib.h>


// static functions
static void set_shape_input(struct layer_t *p, const struct tensor_t *input)
{
	// get shape
	unsigned int rank = tensor_get_rank(input);
	unsigned int *dims = tensor_get_dims(input);

	// copy shape
	p->shape_input = shape_create(rank, dims);

	// exit program if bitwise layer cannot be defined
	unsigned int n_bits = array_product_ui(dims, rank);
	FATAL_ERROR(!IS_DIV8(n_bits), ERR_MSG_INIT);
}

static void set_shape_kernel(struct layer_t *p, const struct tensor_t *kernel)
{
	// get shape
	unsigned int rank = tensor_get_rank(kernel);
	unsigned int *dims = tensor_get_dims(kernel);

	// copy shape
	p->shape_kernel = shape_create(rank, dims);
}

static void set_shape_output(struct layer_t *p)
{
	// initialise
	unsigned int rank_i = shape_get_rank(p->shape_input);
	unsigned int *dims_i = shape_get_dims(p->shape_input);
	unsigned int n_mult = shape_get_dims(p->shape_kernel)[0];

	// set up output shape
	p->shape_output = shape_create(rank_i, dims_i);
	shape_get_dims(p->shape_output)[rank_i - 1] *= n_mult;
}


// functions
struct layer_t *_stacked_xnor_init(const struct tensor_t *input,
                                   const struct tensor_t *kernel,
                                   unsigned char **weights)
{
	// allocate
	struct layer_t *p = SALLOC(p);
	p->layer_type = LAYER_TYPE_STACKED_XNOR;

	// geometry
	set_shape_input(p, input);
	set_shape_kernel(p, kernel);
	set_shape_output(p);

	// weights & output
	weights_assign(p, weights);
	p->output = output_create(p->shape_output);

	// functions
	stacked_xnor_push_set(p);
	p->destroy = &stacked_xnor_destroy;

	return p;
}
