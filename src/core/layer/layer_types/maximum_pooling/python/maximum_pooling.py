class maximum_pooling(layers.Layer):
	def __init__(self, kernel, dropout=False, dropout_rate=0.0):
		super(maximum_pooling, self).__init__()
		self.kernel = kernel
		self.strides = kernel
		self.dropout = dropout
		self.dropout_rate = dropout_rate
		self.train = False

		return

	def call(self, inputs):
		# nonlocals
		input_padded = None
		outputs = None


		# functions
		def get_shape_out(si, sk):
			import math

			y = lambda i, j: math.ceil(float(i) / float(j))
			so = [y(i, k) for i, k in zip(si, sk)]

			return so

		def get_pad(si, sk, so):
			# total
			mt = lambda i, k, o: max(o * k - i, 0)
			p_t = [mt(i, k, o) for i, k, o in zip(si, sk, so)]

			# left & right
			p_l = [int(t / 2) for t in p_t]
			p_r = [t - l for t, l in zip(p_t, p_l)]

			# paired
			p_p = [[l, r] for l, r in zip(p_l, p_r)]

			return tf.constant([[0, 0], *p_p, [0, 0]])

		def build_padded_input():
			nonlocal input_padded

			# shapes
			si = inputs.shape[1:-1]
			sk = self.kernel
			so = get_shape_out(si, sk)

			# pads
			pad = get_pad(si, sk, so)

			# apply padding
			args = {"mode": "CONSTANT", "constant_values": -1.0}
			input_padded = tf.pad(inputs, pad, **args)

			return

		def build_outputs():
			nonlocal outputs

			args = {"ksize": self.kernel, "strides": self.strides}
			args = {**args, "padding": "VALID"}
			outputs = tf.nn.max_pool(input_padded, **args)

			return

		def apply_dropout():
			nonlocal outputs

			if not self.train:
				return

			if not self.dropout:
				return

			out = tf.nn.dropout(outputs, rate=self.dropout_rate)
			out = out + tf.stop_gradient(tf.sign(out) - out)
			outputs = out

			return


		# main
		build_padded_input()
		build_outputs()
		apply_dropout()

		return outputs

	def clip_weights(self):
		return

	def save(self, f):
		return
