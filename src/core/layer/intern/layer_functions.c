#include "../layer.h"
#include "../def/layer_def.h"


// functions
void layer_destroy(struct layer_t *layer)
{
	layer->destroy(layer);
}
