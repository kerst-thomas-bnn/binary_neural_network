#include "../layer.h"
#include "../def/layer_def.h"
#include "../def/shape/shape.h"


// functions
unsigned int *layer_get_input_dims(const struct layer_t *layer)
{
	return shape_get_dims(layer->shape_input);
}
