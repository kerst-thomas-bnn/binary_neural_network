#include "../../include/memory.h"
#include "../../tensor/tensor.h"
#include "../activations/activations.h"
#include "../layer.h"
#include "../def/layer_def.h"
#include "../def/output/output.h"

#include <stdlib.h>


// functions
void layer_set_activation_one_hot(struct layer_t *layer)
{
	unsigned int **op = output_get_one_hot_memp(layer->output);
	FREE(*op);
	MALLOC(*op, output_get_prediction_n_bits(layer->output));
	layer->activate = activate_one_hot;
}

void layer_set_activation_average(struct layer_t *layer)
{
	unsigned int **op = output_get_one_hot_memp(layer->output);
	FREE(*op);
	layer->activate = activate_average;
}

void layer_set_activation_maximum(struct layer_t *layer)
{
	unsigned int **op = output_get_one_hot_memp(layer->output);
	FREE(*op);
	layer->activate = activate_maximum;
}
