#include "../layer.h"
#include "../def/layer_def.h"
#include "../def/layer_names.h"


// functions
const char *layer_get_name(const struct layer_t *layer)
{
	return layer_names[layer->layer_type];
}
