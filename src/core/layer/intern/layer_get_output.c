#include "../layer.h"
#include "../def/layer_def.h"
#include "../def/output/output.h"


// functions
const struct tensor_t *layer_get_output(const struct layer_t *layer)
{
	return output_get_prediction(layer->output);
}
