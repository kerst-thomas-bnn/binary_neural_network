#include "../init.h"
#include "../layer_types/flattening/flattening.h"
#include "../layer_types/convolution/convolution.h"
#include "../layer_types/pooling/pooling.h"
#include "../layer_types/inception_v1/inception_v1.h"
#include "../layer_types/knockout/knockout.h"
#include "../layer_types/bitwise/bitwise.h"
#include "../layer_types/accumulation/accumulation.h"
#include "../layer_types/shift/shift.h"
#include "../layer_types/stacked_xnor/stacked_xnor.h"


// macros
#define INIT_ARGS  \
	const struct tensor_t *input,  \
	const struct tensor_t *kernel,  \
	unsigned int strides,  \
	unsigned char **weights  \


// functions
struct layer_t *flattening_init(INIT_ARGS)
{
	return _flattening_init(input);
}

struct layer_t *convolution_init(INIT_ARGS)
{
	return _convolution_init(input, kernel, strides, weights);
}

struct layer_t *maximum_pooling_init(INIT_ARGS)
{
	return _pooling_init(input, kernel, POOLING_INIT_MAXIMUM);
}

struct layer_t *average_pooling_init(INIT_ARGS)
{
	return _pooling_init(input, kernel, POOLING_INIT_AVERAGE);
}

struct layer_t *xnor_pooling_init(INIT_ARGS)
{
	return _pooling_init(input, kernel, POOLING_INIT_XNOR);
}

struct layer_t *inception_v1_init(INIT_ARGS)
{
	return _inception_v1_init(input, kernel, strides, weights);
}

struct layer_t *knockout_init(INIT_ARGS)
{
	return _knockout_init(input, weights);
}

struct layer_t *bitwise_init(INIT_ARGS)
{
	return _bitwise_init(input, weights);
}

struct layer_t *accumulation_init(INIT_ARGS)
{
	return _accumulation_init(input, kernel, weights);
}

struct layer_t *shift_init(INIT_ARGS)
{
	return _shift_init(input, kernel);
}

struct layer_t *stacked_xnor_init(INIT_ARGS)
{
	return _stacked_xnor_init(input, kernel, weights);
}
