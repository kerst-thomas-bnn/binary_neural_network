#include "../layer.h"
#include "../def/layer_def.h"
#include "../def/output/output.h"


// functions
const struct tensor_t *layer_push(const struct layer_t *layer,
                                  const struct tensor_t *input)
{
	layer->push(layer, input);

	return output_get_prediction(layer->output);
}
