#ifndef TENSOR_DEF_H
#define TENSOR_DEF_H


// forward declarations
struct bits_t;


// structs
struct tensor_t {
	unsigned int rank;
	unsigned int *dims;
	unsigned int *index_offset;
	struct bits_t *bits;
};


#endif
