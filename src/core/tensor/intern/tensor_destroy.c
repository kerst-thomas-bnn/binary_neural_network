#include "../../include/memory.h"
#include "../../bits/bits.h"
#include "../tensor.h"
#include "../def/def.h"


// functions
void tensor_destroy(struct tensor_t *tensor)
{
	if (!tensor)
		return;

	FREE(tensor->dims);
	FREE(tensor->index_offset);
	bits_destroy(tensor->bits);
	FREE(tensor);
}
