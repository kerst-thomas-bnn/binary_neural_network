#include "../../bits/bits.h"
#include "../tensor.h"
#include "../def/def.h"

#include <stdio.h>


// static functions
static void display_tensor_address(const struct tensor_t *tensor)
{
	fprintf(stdout, "Tensor %p: (", tensor);
}

static void display_tensor_shape(const struct tensor_t *tensor)
{
	for (unsigned int i = 0; i < tensor->rank; ++i)
	{
		const char *delim = (i == tensor->rank - 1) ? ") [" : ", ";
		fprintf(stdout, "%u%s", tensor->dims[i], delim);
	}
}

static void display_tensor_index_offsets(const struct tensor_t *tensor)
{
	for (unsigned int i = 0; i < tensor->rank; ++i)
	{
		const char *delim = (i == tensor->rank - 1) ? ") [" : ", ";
		fprintf(stdout, "%u%s", tensor->index_offset[i], delim);
	}
}

static void display_tensor_bits_properties(const struct tensor_t *tensor)
{
	unsigned int n_bits = bits_get_n_bits(tensor->bits);
	unsigned int n_bytes = bits_get_n_bytes(tensor->bits);
	fprintf(stdout, "%u/%u\n", n_bits, n_bytes);
}


// functions
void tensor_display_short(const struct tensor_t *tensor)
{
	// return if 0 is passed
	if (!tensor)
		return;

	// display
	display_tensor_address(tensor);
	display_tensor_shape(tensor);
	display_tensor_index_offsets(tensor);
	display_tensor_bits_properties(tensor);
}
