#include "../../bits/bits.h"
#include "../tensor.h"
#include "../def/def.h"


// functions
unsigned int tensor_get_index(const struct tensor_t *tensor,
                              const unsigned int *indicees)
{
	unsigned int pos = 0;
	for (unsigned int i = 0; i < tensor->rank; ++i)
		pos += tensor->index_offset[i] * indicees[i];

	return pos;
}
