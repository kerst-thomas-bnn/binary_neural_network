#ifndef TENSOR_SLICE_H
#define TENSOR_SLICE_H


// forward declarations
struct tensor_t;


// functions
struct tensor_t *tensor_slice_backend(const struct tensor_t *tensor,
                                      const unsigned int *limits,
                                      const unsigned int *paddings,
                                      unsigned char *wrt_tar);


#endif
