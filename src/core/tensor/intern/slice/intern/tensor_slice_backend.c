#include "../../../../bits/bits.h"
#include "../../../tensor.h"
#include "../../../def/def.h"
#include "../slice.h"


// constants
#define MAX_INDEX_OFFSETS 16


// static variables
static const struct tensor_t *input;
static const unsigned int *limits_static;
static const unsigned int *paddings_static;
static unsigned char *wrt_tar_static;
static unsigned int pos_bits;
static struct tensor_t *slice;
static unsigned int slice_index_offsets[MAX_INDEX_OFFSETS];


// static functions
static void run_through_indicees(unsigned int dimension, unsigned int offset)
{
	// limits of this dimension
	unsigned int start = limits_static[2 * dimension];
	unsigned int stop = limits_static[2 * dimension + 1];

	// is this the last dimension?, if yes, then add this value to the tensor
	if (dimension == input->rank - 1) {
		for (unsigned int i = start; i < stop; ++i) {
			unsigned int pos_src = offset + i;
			unsigned char val = bits_get_bit(input->bits, pos_src);
			if (val) {
				if (slice) {
					bits_flip_up(slice->bits, pos_bits);
				} else {
					wrt_tar_static[pos_bits / 8] |= 128 >> (pos_bits % 8);
				}
			}
			++pos_bits;
		}

		return;
	}

	// if now, then go over this dimension from start to stop
	unsigned int dim_max = input->dims[dimension];
	unsigned int iio = input->index_offset[dimension];
	unsigned int sio = (slice) ? slice->index_offset[dimension] : slice_index_offsets[dimension];

	// - where start is modified by left padding
	unsigned int pad_left = paddings_static ? paddings_static[2  *dimension] : 0;
	if (start < pad_left) {
		pos_bits += (pad_left - start)  *sio;
		start = pad_left;
	}
	start -= pad_left;
	stop -= pad_left;

	// - and stop by right padding
	unsigned int pad_right = (stop >= dim_max) ? stop - dim_max : 0;
	stop -= pad_right;

	// - then run through the new start and stop
	for (unsigned int i = start; i < stop; ++i)
		run_through_indicees(dimension + 1, offset + i  *iio);

	// - and ajdust right padding indicees afterwards
	pos_bits += pad_right  *sio;
}


// functions
struct tensor_t *tensor_slice_backend(const struct tensor_t *tensor,
                                      const unsigned int *limits,
                                      const unsigned int *paddings,
                                      unsigned char *wrt_tar)
{
	// set static vars
	input = tensor;
	limits_static = limits;
	paddings_static = paddings;
	pos_bits = 0;
	wrt_tar_static = wrt_tar;

	// compute slice dimensions
	unsigned int rank = input->rank;
	unsigned int dims[rank];
	for (unsigned int i = 0; i < rank; ++i)
		dims[i] = limits_static[2 * i + 1] - limits_static[2 * i];

	// allocate the memory or set index offsets
	if (wrt_tar) {
		slice = 0;

		slice_index_offsets[rank - 1] = 1;
		for (unsigned int i = rank - 1; i > 0; --i)
			slice_index_offsets[i - 1] = slice_index_offsets[i]  *dims[i];

	} else {
		slice = tensor_create(input->rank, dims, 0);
	}

	// fill in the values
	run_through_indicees(0, 0);

	return slice;
}
