#include "../../bits/bits.h"
#include "../tensor.h"
#include "../def/def.h"


// functions
unsigned int tensor_get_rank(const struct tensor_t *tensor)
{
	return tensor->rank;
}

unsigned int *tensor_get_dims(const struct tensor_t *tensor)
{
	return tensor->dims;
}

unsigned int *tensor_get_index_offset(const struct tensor_t *tensor)
{
	return tensor->index_offset;
}

struct bits_t *tensor_get_bits(const struct tensor_t *tensor)
{
	return tensor->bits;
}

struct bits_t **tensor_get_bitsp(struct tensor_t *tensor)
{
	return &tensor->bits;
}

unsigned char tensor_get_bit(const struct tensor_t *tensor, unsigned int n)
{
	return bits_get_bit(tensor->bits, n);
}

unsigned char *tensor_get_bytes(const struct tensor_t *tensor)
{
	return bits_get_bytes(tensor->bits);
}

unsigned int tensor_get_n_bits(const struct tensor_t *tensor)
{
	return bits_get_n_bits(tensor->bits);
}

unsigned int tensor_get_n_bytes(const struct tensor_t *tensor)
{
	return bits_get_n_bytes(tensor->bits);
}
