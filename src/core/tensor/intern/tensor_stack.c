#include "../../include/error.h"
#include "../../include/bitwise.h"
#include "../../bits/bits.h"
#include "../tensor.h"
#include "../def/def.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>


// static variables
static unsigned int n_tensors;
static const struct tensor_t **tensors;
static unsigned int n_filters_out;
static unsigned int *limits;
static unsigned int pos_bits;
static unsigned char *tar;


// static functions
static inline void check_ranks(void)
{
	unsigned int rank = tensors[0]->rank;
	for (unsigned int i = 0; i < n_tensors; ++i) {
		const struct tensor_t *p = tensors[i];
		FATAL_ERROR(
			p->rank != rank,
			"During tensor stacking: \
			Ranks of stacked tensors do not match"
		);
	}
}

static inline void check_dimensions(void)
{
	unsigned int rank = tensors[0]->rank;
	unsigned int *dims = tensors[0]->dims;

	n_filters_out = 0;
	for (unsigned int i = 0; i < n_tensors; ++i) {
		const struct tensor_t *q = tensors[i];
		n_filters_out += q->dims[rank - 1];

		for (unsigned int j = 0; j < rank - 1; ++j)
			FATAL_ERROR(
				dims[j] != q->dims[j],
				"During tensor stacking: \
				Dimensions of stacked tensors do not match"
			);
	}
}

static inline unsigned int get_number_of_bytes(unsigned int n_filters)
{
	return (n_filters >> 3) + (IS_DIV8(n_filters) ? 0 : 1);
}

static inline bool get_bit_val(unsigned char *bytes, unsigned int pos)
{
	return (bytes[pos >> 3] & 128 >> MOD8(pos)) ? 1 : 0;
}

static inline void copy_values(void)
{
	for (unsigned int i = 0; i < n_tensors; ++i) {
		// set limits
		const struct tensor_t *q = tensors[i];
		unsigned int n_filters = q->dims[q->rank - 1];
		limits[2  *q->rank - 1] = n_filters;

		// retrieve bits
		unsigned int n_bytes = get_number_of_bytes(n_filters);
		unsigned char bytes[n_bytes];
		memset(bytes, 0, n_bytes);
		tensor_slice_vals(q, limits, 0, bytes);

		// flip bits accordingly
		for (unsigned int j = 0; j < n_filters; ++j) {
			bool bit_val = get_bit_val(bytes, j);
			if (bit_val)
				tar[pos_bits >> 3] |= 128 >> MOD8(pos_bits);

			++pos_bits;
		}
	}
}

static void run_through_dimension(unsigned int dim)
{
	// is this the last dimension? if yes, copy the values
	if (dim == tensors[0]->rank - 1) {
		copy_values();

		return;
	}

	// if no, keep going
	for (unsigned int i = 0; i < tensors[0]->dims[dim]; ++i) {
		limits[2  *dim] = i;
		limits[2  *dim + 1] = i + 1;
		run_through_dimension(dim + 1);
	}
}


// functions
void tensor_stack(unsigned int n,
                  const struct tensor_t **p,
		  unsigned char *wrt_tar)
{
	// set static variables
	n_tensors = n;
	tensors = p;
	tar = wrt_tar;

	// check ranks and dimensions
	check_ranks();
	check_dimensions();

	// run through the dimensions
	unsigned int _limits[2 * tensors[0]->rank];
	limits = _limits;
	limits[2  *tensors[0]->rank - 2] = 0;
	pos_bits = 0;
	run_through_dimension(0);
}
