#include "../../bits/bits.h"
#include "../tensor.h"
#include "../def/def.h"


// functions
unsigned char tensor_compare(const struct tensor_t *tensor_1,
                             const struct tensor_t *tensor_2)
{
	// compare the number of dimensions
	if (tensor_1->rank != tensor_2->rank)
		return 0;

	// compare the dimensions
	for (unsigned int i = 0; i < tensor_1->rank; ++i)
		if (tensor_1->dims[i] != tensor_2->dims[i])
			return 0;

	// compare the individual bits
	struct bits_t *p = tensor_1->bits;
	struct bits_t *q = tensor_2->bits;
	for (unsigned int i = 0; i < bits_get_n_bits(p); ++i)
		if (bits_get_bit(p, i) != bits_get_bit(q, i))
			return 0;

	return 1;
}
