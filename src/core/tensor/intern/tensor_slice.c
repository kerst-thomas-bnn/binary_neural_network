#include "../tensor.h"
#include "slice/slice.h"


// functions
struct tensor_t *tensor_slice(const struct tensor_t *tensor,
                              const unsigned int *limits,
                              const unsigned int *paddings)
{
	return tensor_slice_backend(tensor, limits, paddings, 0);
}

void tensor_slice_vals(const struct tensor_t *tensor,
                       const unsigned int *limits,
                       const unsigned int *paddings,
                       unsigned char *wrt_tar)
{
	tensor_slice_backend(tensor, limits, paddings, wrt_tar);
}
