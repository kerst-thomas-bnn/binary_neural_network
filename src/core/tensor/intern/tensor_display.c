#include "../../bits/bits.h"
#include "../tensor.h"
#include "../def/def.h"

#include <stdio.h>


// functions
void tensor_display(const struct tensor_t *tensor)
{
	// return if 0 is passed
	if (!tensor)
		return;

	// tensor address
	fprintf(stdout, "Tensor %p:\n", tensor);

	// dimensions
	fprintf(stdout, "Dimensions: ");
	for (unsigned int i = 0; i < tensor->rank; ++i)
		fprintf(stdout, "%u ", tensor->dims[i]);

	fprintf(stdout, "\n");

	// index offsets
	fprintf(stdout, "Index Offsets: ");
	for (unsigned int i = 0; i < tensor->rank; ++i)
		fprintf(stdout, "%u ", tensor->index_offset[i]);

	fprintf(stdout, "\n");

	// content
	if (tensor->bits) {
		bits_display(tensor->bits);
		fprintf(stdout, "\n");
	}
}
