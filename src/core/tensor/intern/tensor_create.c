#include "../../include/memory.h"
#include "../../bits/bits.h"
#include "../tensor.h"
#include "../def/def.h"

#include <string.h>
#include <stdlib.h>


// static functions
static void allocate_non_empty_tensor(struct tensor_t *p,
                                      unsigned int rank,
                                      const unsigned int *dims,
                                      void *data)
{
	// set rank and dimensions
	p->rank = rank;
	MALLOC(p->dims, p->rank);
	memcpy(p->dims, dims, p->rank * sizeof(*p->dims));

	// compute index offsets
	MALLOC(p->index_offset, p->rank);
	p->index_offset[p->rank - 1] = 1;
	for (unsigned int i = p->rank - 1; i > 0; --i)
		p->index_offset[i - 1] = p->index_offset[i] * p->dims[i];

	// allocate bits
	unsigned int n_bits = *p->index_offset * *p->dims;
	p->bits = bits_create(data, n_bits);
}


// functions
struct tensor_t *tensor_create(unsigned int rank,
                               const unsigned int *dims,
                               void *data)
{
	// initialise
	struct tensor_t *p = SALLOC(p);

	// fill in data
	if (rank)
		allocate_non_empty_tensor(p, rank, dims, data);

	return p;
}
