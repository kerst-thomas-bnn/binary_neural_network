#ifndef TENSOR_H
#define TENSOR_H


// forward declarations
struct tensor_t;
struct bits_t;


// ----------------------------------------------------------------------------
// --- create and destroy ---
// ----------------------------------------------------------------------------

struct tensor_t *tensor_create(unsigned int rank,
                               const unsigned int *dims,
                               void *data);
// creates a tensor using either freshly dynamically allocated memory or on
// top of on an existing data structure
// accepts:
// 	(unsigned int rank) rank of the new tensor
// 	(const unsigned int *dims) dimensions of the new tensors
// 	(void *data) data on top of which the bit array is created, if 0 is
// 	             passed, then the memory will be allocated and initialised
// 	             to 0
// returns:
// 	(struct tensor_t *) the newly created tensor

void tensor_destroy(struct tensor_t *tensor);
// frees ALL memory allocated for the tensor AND the data it points to
// accepts:
// 	(struct tensor_t *tensor) tensor to be destroyed


// ----------------------------------------------------------------------------
// --- member access ---
// ----------------------------------------------------------------------------

unsigned int tensor_get_rank(const struct tensor_t *tensor);
// returns the rank of a tensor
// accepts:
// 	(const struct tensor_t *tensor) tensor
// returns:
// 	(unsigned int) tensor rank

unsigned int *tensor_get_dims(const struct tensor_t *tensor);
// returns the dimensions of a tensor
// accepts:
// 	(const struct tensor_t *tensor) tensor
// returns:
// 	(unsigned int *) tensor dimensions

unsigned int *tensor_get_index_offset(const struct tensor_t *tensor);
// returns the index offset of a tensor
// accepts:
// 	(const struct tensor_t *tensor) tensor
// returns:
// 	(unsigned int *) tensor index offsets

struct bits_t *tensor_get_bits(const struct tensor_t *tensor);
// returns a tensor's underlying bit array
// accepts:
// 	(const struct tensor_t *tensor) tensor
// returns:
// 	(struct bits_t *) the tensor's bit array

struct bits_t **tensor_get_bitsp(struct tensor_t *tensor);
// returns the address of a tensor's underlying bit array
// accepts:
// 	(struct tensor_t *tensor) tensor
// returns:
// 	(struct bits_t **) address to the tensor's bit array

unsigned char tensor_get_bit(const struct tensor_t *tensor, unsigned int n);
// returns the bit value of a specific tensor element
// accepts:
// 	(const struct tensor_t *tensor) tensor
// 	(unsigned int n) number of the bit
// returns:
// 	(unsigned char) bit value, either 0 or 1

unsigned char *tensor_get_bytes(const struct tensor_t *tensor);
// returns a tensors raw unformatted content
// accepts:
// 	(const struct tensor_t *tensor) tensor
// returns:
// 	(unsigned char *) the raw data the tensor points to

unsigned int tensor_get_n_bits(const struct tensor_t *tensor);
// returns the number of bits represented in a tensor
// accepts:
// 	(const struct tensor_t *tensor) tensor
// returns:
// 	(unsigned int) the number of bits represented by the tensor

unsigned int tensor_get_n_bytes(const struct tensor_t *tensor);
// returns the number of bytes represented in a tensor
// accepts:
// 	(const struct tensor_t *tensor) tensor
// returns:
// 	(unsigned int) number of bytes represented by the tensor


// ----------------------------------------------------------------------------
// --- slice ---
// ----------------------------------------------------------------------------

struct tensor_t *tensor_slice(const struct tensor_t *tensor,
                              const unsigned int *limits,
                              const unsigned int *paddings);
// returns the slice of a tensor, the slice is stored to dynamically allocated
// memory and must be destroyed manually when no longer needed
// accepts:
// 	(const struct tensor_t *tensor) tensor
//	(const unsigned int *limits) start indicees of all dimensions
//	(const unsigned int *paddings) stop indicees of all dimensions
// returns:
// 	(struct tensor_t*) tensor slice

void tensor_slice_vals(const struct tensor_t *tensor,
                       const unsigned int *limits,
                       const unsigned int *paddings,
                       unsigned char *wrt_tar);
// returns the slice of a tensor, without allocating memory dynamically
// accepts:
// 	(const struct tensor_t *tensor) tensor
//	(const unsigned int *limits) start indicees of all dimensions
//	(const unsigned int *paddings) stop indicees of all dimensions
// 	(unsigned char *wrt_tar) array to where the raw slice value is written,
// 	                         does not check whether the array is long
// 	                         enough and can result in buffer overflow or
// 	                         undefined behaviour if used carelessly


// ----------------------------------------------------------------------------
// --- miscellaneous ---
// ----------------------------------------------------------------------------

void tensor_display(const struct tensor_t *tensor);
// prints a tensor and its contents to the console
// accepts:
// 	(const struct tensor_t *tensor) tensor

void tensor_display_short(const struct tensor_t *tensor);
// prints a tensor shape and memory location in a single line to the console
// accepts:
// 	(const struct tensor_t *tensor) tensor

unsigned char tensor_compare(const struct tensor_t *tensor_1,
                             const struct tensor_t *tensor_2);
// compares the shape and contents of two tensors
// accepts:
// 	(const struct tensor_t *tensor_1) tensor 1
// 	(const struct tensor_t *tensor_2) tensor 2
// returns:
// 	(unsigned char) 1 if both tensors have identical shape and content,
// 	                0 if not

unsigned int tensor_index(const struct tensor_t *tensor,
                          const unsigned int *indicees);
// computes the index of a tensor element, given its geometric position
// accepts:
// 	(const struct tensor_t *tensor) tensor
// 	(const unsigned int *indicees) vector of indicees, must have the same
// 	                               length as tensor dimensions
// returns:
// 	(unsigned int) index of the element


void tensor_stack(unsigned int n,
                  const struct tensor_t **p,
		  unsigned char *wrt_tar);
// stacks tensors of idential rank along the last dimension
// accepts:
// 	(unsigned int n) number of tensors
// 	(const struct tensor_t **p) array of tensors to be stacked
// 	(unsigned char *wrt_tar) pointer to place where data is being written


#endif
