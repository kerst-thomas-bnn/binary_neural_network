#ifndef ERROR_H
#define ERROR_H


// include
#include <stdio.h>
#include <stdlib.h>


// macros
#define FATAL_ERROR(CONDITION, MESSAGE)  \
	do {  \
		if ((CONDITION)) {  \
			fprintf(  \
				stdout,  \
				"[ERROR] %s. Aborting program execution.\n",  \
				(MESSAGE)  \
			);  \
			exit(1);  \
		}  \
	} while(0)


#endif
