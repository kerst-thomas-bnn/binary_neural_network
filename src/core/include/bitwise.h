#ifndef BITWISE_MACROS_H
#define BITWISE_MACROS_H


// macros
// - 8
#define MUL8(X) ((X) << (3))

#define DIV8(X) ((X) >> (3))

#define IS_DIV8(X) ((((X) >> (3)) << (3)) == (X))

#define MOD8(X) ((X) - (((X) >> (3)) << (3)))


// - 2 ** N
#define MULN(X, N) ((X) << (N))

#define DIVN(X, N) ((X) >> (N))

#define IS_DIVN(X, N) ((((X) >> (N)) << (N)) == (X))

#define MODN(X, N) ((X) - (((X) >> (N)) << (N)))



#endif
