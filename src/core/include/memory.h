#ifndef MEMORY_H
#define MEMORY_H


// include
#include <stdlib.h>


// macros
#define MALLOC(X, N)  \
	do {  \
		(X) = malloc((N) * sizeof(*(X)));  \
	} while(0)

#define SALLOC(X)  \
	calloc(1, sizeof(*(X)))

#define CALLOC(X, N)  \
	do {  \
		(X) = calloc((N), sizeof(*(X)));  \
	} while(0)

#define REALLOC(X, N)  \
	do {  \
		(X) = realloc((X), (N) * sizeof(*(X)));  \
	} while(0)

#define FREE(X)  \
	do {  \
		free((X));  \
		(X) = 0;  \
	} while(0)


#endif
