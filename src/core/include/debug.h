#ifndef DEBUG_H
#define DEBUG_H


// include
#include <stdio.h>


// macros
#define DISP(X, Y)  \
	do {  \
		fprintf(stdout, "\n%" #Y "\n", (X));  \
	} while(0)

#define DISPA(X, N, Y)  \
	do {  \
		fprintf(stdout, "\n");  \
		for (unsigned int _i = 0; _i < N; ++_i)  \
			fprintf(stdout, "%" #Y ", ", X[_i]);  \
		fprintf(stdout, "\n");  \
	} while(0)


#endif
