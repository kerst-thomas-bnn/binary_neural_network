#ifndef ARRAY_H
#define ARRAY_H


// includes
#include <string.h>



// macros
#define ARRAY_LEN(ARRAY)  \
	sizeof((ARRAY)) / sizeof((ARRAY)[0])


// static inline functions
static inline unsigned int array_product_ui(unsigned int *ar, unsigned int n)
{
	unsigned int product = 1;
	for (unsigned int i = 0; i < n; ++i)
		product *= ar[i];

	return product;
}


#endif
