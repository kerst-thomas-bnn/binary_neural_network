#include "../../../c_api/bnn.h"
#include "../../include/debug.h"
#include "local_push/local_push.h"

#include <stdio.h>
#include <stdbool.h>
#include <sys/time.h>


/*
 * Static Variables
 */
static unsigned int n_items;


/*
 * Static Functions
 */
static bool is_initialised()
{
	const struct model_t *model = model_c_api_local_push_get_model();
	n_items = model_c_api_local_push_get_number_of_local_items();

	if (!model || !n_items)
		return false;

	return true;
}

static inline void display(const struct timeval *t,
                           unsigned int n_cor,
                           unsigned int n_proc,
                           unsigned int n_tot)
{
	model_c_api_local_push_display_metrics(t, n_cor, n_proc, n_tot);
}

static void push_local_items()
{
	/* initialise */
	unsigned int n_correct = 0;
	struct timeval t_start;
	gettimeofday(&t_start, 0);

	/* push */
	for (unsigned int i = 0; i < n_items; ++i) {
		n_correct += model_c_api_local_push_predict_and_check(i);
		display(&t_start, n_correct, i + 1, n_items);
	}

	/* finish */
	fprintf(stdout, "\n");
}


/*
 * Functions
 */
void bnn_model_push_local()
{
	if (!is_initialised())
		return;

	push_local_items();
}
