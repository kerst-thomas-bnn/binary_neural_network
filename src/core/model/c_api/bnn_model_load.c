#include "../../../c_api/bnn.h"
#include "../../include/debug.h"
#include "../../include/memory.h"
#include "../../include/error.h"
#include "../../layer/layer.h"
#include "../../opt/opt.h"
#include "../model.h"
#include "../def/def.h"
#include "../print/print.h"
#include "intern.h"

#include <stdio.h>
#include <dirent.h>
#include <string.h>


// constants
#define STRING_IDENTIFIER_BUFFER 8
#define PATH_LAYER_FOLDER "src"
#define ERR_STORE "Unable to store model in memory"
#define ERR_MODEL "Unable to read the model definition file"
#define ERR_WEIGHTS "Unable to read the weights file"


// static variables
static struct model_t *model_local;
static unsigned char *weights;


// static functions
static char *fetch_file_content(const char *path, char *err_msg)
{
	// if the path is 0, return a 0
	if (!path)
		return 0;

	// open the file
	FILE *f = fopen(path, "r");
	FATAL_ERROR(!f, err_msg);

	// get file size
	FATAL_ERROR(fseek(f, 0, SEEK_END), err_msg);
	size_t len = (size_t)ftell(f);
	FATAL_ERROR(fseek(f, 0, SEEK_SET), err_msg);

	// allocate memory and copy content
	char *file_content;
	MALLOC(file_content, len + 1);
	FATAL_ERROR(len != fread(file_content, 1, len, f), err_msg);
	file_content[len] = 0;

	// clean up
	fclose(f);

	return file_content;
}

static void copy_path(char* *p, const char *path)
{
	// if no path is provided, set the stored path to 0
	if (!path) {
		*p = 0;

		return;
	}

	// else, actually copy it
	size_t len = strlen(path);
	char *buffer;
	MALLOC(buffer, len + 1);
	memcpy(buffer, path, len);
	buffer[len] = 0;

	// link the newly allocated to the model
	*p = buffer;
}

static void add_layer_to_model(struct layer_t *layer)
{
	// fetch the model
	struct model_t *model = model_set_get(0, MODEL_STORE_GET);

	// add the new layer
	++model->n_layers;
	REALLOC(model->layers, model->n_layers);
	model->layers[model->n_layers - 1] = layer;
}

static void parse_model_file_content(char *cont)
{
	// declare layer initialisation function
	struct layer_t *(*layer_init_f)(
		const struct tensor_t*,
		const struct tensor_t*,
		unsigned int,
		unsigned char**
	);

	// declare input, kernel and stride
	const struct tensor_t *shape_input;
	const struct tensor_t *shape_kernel;
	unsigned int stride;

	// iterate through the model file lines
	for (char *line = strtok(cont, "\n"); line; line = strtok(0, "\n")) {
		// fetch the appropriate initialisation function
		layer_init_f = model_c_api_pick_function(line);

		// fetch the function parameters
		shape_input = model_c_api_get_current_model_output_shape();
		shape_kernel = model_c_api_get_kernel(line);
		stride = model_c_api_get_stride(line);

		// construct the new layer, usinng the function pointer
		struct layer_t *new_layer = layer_init_f(
			shape_input,
			shape_kernel,
			stride,
			(weights) ? &weights : 0
		);
		add_layer_to_model(new_layer);
	}
}

static void add_accumulation_layer()
{
	// set layer initialisation function
	struct layer_t *(*layer_init_f)(
		const struct tensor_t*,
		const struct tensor_t*,
		unsigned int,
		unsigned char**);

	// declare layer initialisation function, input, kernel, and stride
	const struct tensor_t *shape_input;
	const struct tensor_t *shape_kernel;
	unsigned int stride;

	// set layer initialisation function, input, kernel and stride
	layer_init_f = model_c_api_pick_function("ac:");
	shape_input = model_c_api_get_current_model_output_shape();
	shape_kernel = model_c_api_get_final_kernel();
	stride = 1;

	// add the accumulation layer to the model
	struct layer_t *accumulation_layer = layer_init_f(
		shape_input,
		shape_kernel,
		stride,
		(weights) ? &weights: 0
	);
	add_layer_to_model(accumulation_layer);
}


// functions
void bnn_model_load(const char *path_model, const char *path_weights)
{
	// store an empty model in memory
	CALLOC(model_local, 1);
	struct model_t *model = model_set_get(model_local, MODEL_STORE_SET);
	FATAL_ERROR(model != model_local, ERR_STORE);

	// store the paths
	copy_path(&model->path_model, path_model);
	copy_path(&model->path_weights, path_weights);

	// read out model and weight files
	char *model_string = fetch_file_content(path_model, ERR_MODEL);
	char *weights_string = fetch_file_content(path_weights, ERR_WEIGHTS);
	weights = (unsigned char*)weights_string;

	// add the layers
	parse_model_file_content(model_string);
	add_accumulation_layer();

	// print the model and optimise the loaded network
	model_print();
	if (weights)
		opt_optimise_all_layers();

	// clean up
	FREE(model_string);
	FREE(weights_string);
}
