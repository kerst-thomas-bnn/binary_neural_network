#include "../../../setup/setup.h"
#include "../intern.h"

#include <string.h>


// functions
const struct tensor_t* model_c_api_get_final_kernel()
{
	return setup_get_tensor(SETUP_IO_Y);
}
