#include "../../../include/debug.h"
#include "../../../include/error.h"
#include "../intern.h"
#include "include/layer_types.h"

#include <string.h>


// constants
#define STRING_BUFFER_ERROR 256
#define STRING_BUFFER_ABBREVIATION 16
#define FE_FUN_PICK "Unable to extract the layer initialisation information from line \"%s\""


// static variables
static char abbreviation[STRING_BUFFER_ABBREVIATION];


// static functions
static void extract_abbreviation(const char* line)
{
	// initialise
	*abbreviation = 0;

	// get position of the double dash
	const char* pos = strstr(line, ":");
	if (!pos)
		return;

	// copy the abbreviation
	size_t len = pos - line;
	memcpy(abbreviation, line, len);
	abbreviation[len] = 0;
}

static struct layer_t* INIT_FUN(pick_function())
{
	for (size_t i = 0; i < sizeof(ids) / sizeof(*ids); ++i)
		if (!strcmp(ids[i].abbreviation, abbreviation))
			return ids[i].function;

	return 0;
}


// functions
struct layer_t* INIT_FUN(model_c_api_pick_function(const char* line))
{
	// extract the function
	extract_abbreviation(line);
	struct layer_t* INIT_FUN(fun) = pick_function();

	// throw an error, if no initialisation function could be determined
	char buffer[STRING_BUFFER_ERROR];
	sprintf(buffer, FE_FUN_PICK, line);
	FATAL_ERROR(!fun, buffer);

	return fun;
}
