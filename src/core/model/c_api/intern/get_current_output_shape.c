#include "../../../include/debug.h"
#include "../../../setup/setup.h"
#include "../../../layer/layer.h"
#include "../../model.h"
#include "../../def/def.h"
#include "../intern.h"


// static functions
static const struct tensor_t* get_input_shape()
{
	return setup_get_tensor(SETUP_IO_X);
}

static const struct tensor_t* get_output_shape(const struct model_t* model)
{
	return layer_get_output(model->layers[model->n_layers - 1]);
}


// functions
const struct tensor_t* model_c_api_get_current_model_output_shape()
{
	// fetch the model
	const struct model_t* model = model_set_get(0, MODEL_STORE_GET);
	// return the input shape if no model has been defined yet
	if (!model)
		return get_input_shape();

	// return the input shape if the model has no layers
	if (!model->n_layers)
		return get_input_shape();

	// return the last output shape of the last layer
	return get_output_shape(model);
}
