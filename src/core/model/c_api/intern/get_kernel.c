#include "../../../include/debug.h"
#include "../../../include/memory.h"
#include "../../../tensor/tensor.h"
#include "../intern.h"

#include <string.h>


// constants
#define KERNEL_STRING_BUFFER 32
#define DIM_STRING_BUFFER 8


// static variables
static char kernel_string[KERNEL_STRING_BUFFER];
static unsigned int rank;
static unsigned int* dims;


// static functions
static void extract_kernel_string(const char* line)
{
	// set pointers
	const char* start = strstr(line, ":") + 1;
	const char* end = strstr(start, "-");
	if (!end)
		end = start + strlen(start);
	size_t len = end - start;

	// copy the kernel string
	memcpy(kernel_string, start, len);
	kernel_string[len] = 0;
}

static void add_dimension(char** iterator, char* buffer)
{
	// set the iterator
	**iterator = 0;
	*iterator = buffer;

	// return, if there is no content
	if (!strlen(buffer))
		return;

	// extract the new dimensions
	int converted_buffer = atoi(buffer);
	unsigned int dim_new = (unsigned int) converted_buffer;

	// extend the memory and store the object
	++rank;
	REALLOC(dims, rank);
	dims[rank - 1] = dim_new;
}

static void extract_rank_and_dimensions_from_kernel_string()
{
	// initialise
	rank = 0;
	dims = 0;
	char buffer[DIM_STRING_BUFFER];
	char* b_it = buffer;

	// iterate through the kernel string
	for (char* it = kernel_string; *it; ++it) {
		// end of current entry
		if (*it == ' ') {
			add_dimension(&b_it, buffer);

			continue;
		}

		// copy the character
		*b_it++ = *it;
	}
	add_dimension(&b_it, buffer);
}


// functions
struct tensor_t* model_c_api_get_kernel(const char* line)
{
	static struct tensor_t* kernel = 0;

	// extrac the dimensions
	extract_kernel_string(line);
	extract_rank_and_dimensions_from_kernel_string();

	// build the new tensor
	tensor_destroy(kernel);
	kernel = (rank) ? tensor_create(rank, dims, 0) : 0;

	// clean up
	FREE(dims);

	return kernel;
}
