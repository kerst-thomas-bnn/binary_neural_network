#include "../../../include/debug.h"
#include "../intern.h"

#include <string.h>
#include <stdbool.h>
#include <stdlib.h>


// constants
#define STRING_BUFFER 32


// static variables
static char stride_string[STRING_BUFFER];


// static functions
static void extract_stride_string(const char* line)
{
	// initialise
	*stride_string = 0;

	// find the stride switch, return if none is defined
	const char* switch_position = strstr(line, "-s");
	if (!switch_position)
		return;

	// copy the relevant part of the line
	switch_position += 2;
	size_t len = strlen(switch_position);
	memcpy(stride_string, switch_position, len);
	stride_string[len] = 0;
}

static bool is_stride_defined()
{
	return (*stride_string) ? true : false;
}

static unsigned int extract_stride()
{
	// cut the stride string off at the appropriate position
	char* pos_end = stride_string;
	for (; (*pos_end) && (*pos_end != ' '); ++pos_end);
	*pos_end = 0;

	return (unsigned int)atoi(stride_string);
}


// functions
unsigned int model_c_api_get_stride(const char* line)
{
	extract_stride_string(line);

	return (is_stride_defined()) ? extract_stride() : 1;
}
