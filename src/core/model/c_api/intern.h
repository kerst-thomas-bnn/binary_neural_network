#ifndef MDOEL_C_API_INTERN_H
#define MDOEL_C_API_INTERN_H


// pre-processor
#define INIT_FUN(X)  \
	(*X)(const struct tensor_t*,  \
             const struct tensor_t*,  \
             unsigned int, unsigned char**)


// forward declarations
struct layer_t;
struct tensor_t;


// functions
struct layer_t *INIT_FUN(model_c_api_pick_function(const char*));

const struct tensor_t *model_c_api_get_current_model_output_shape(void);

struct tensor_t *model_c_api_get_kernel(const char*);

unsigned int model_c_api_get_stride(const char*);

const struct tensor_t *model_c_api_get_final_kernel(void);


#endif
