#ifndef MODEL_C_API_LOCAL_PUSH_WARNING_H
#define MODEL_C_API_LOCAL_PUSH_WARNING_H


// include
#include <stdio.h>


// macros
#define WARNING(COND, MSG)  \
	do {  \
		if (!(COND)) {  \
			fprintf(stdout, "\n[BNN_MODEL_PUSH_LOCAL] %s\n", (MSG));  \
		}  \
	} while(0)


#endif
