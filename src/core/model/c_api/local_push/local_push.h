#ifndef MODEL_C_API_LOCAL_PUSH_H
#define MODEL_C_API_LOCAL_PUSH_H


// forward declarations
struct model_t;
struct timeval;


// functions
const struct model_t* model_c_api_local_push_get_model(void);

unsigned int model_c_api_local_push_get_number_of_local_items(void);

unsigned int model_c_api_local_push_predict_and_check(unsigned int index);

void model_c_api_local_push_display_metrics(const struct timeval*,
                                            unsigned int,
                                            unsigned int,
                                            unsigned int);


#endif
