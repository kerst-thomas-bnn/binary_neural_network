#include "../local_push.h"
#include <stdio.h>
#include <sys/time.h>


// static functions
static inline double get_time(const struct timeval* t_start)
{
	struct timeval t;
	gettimeofday(&t, 0);

	double t_usec = (double)(t.tv_usec - t_start->tv_usec) / 1000000;
	double t_sec = (double)(t.tv_sec - t_start->tv_sec);

	return t_sec + t_usec;
}


// functions
void model_c_api_local_push_display_metrics(const struct timeval* t_start,
                                            unsigned int n_correct,
                                            unsigned int n_current,
                                            unsigned int n_items)
{
	double elapsed_time = get_time(t_start);

	// accuracy
	float accuracy = 100.0f * (float)n_correct / (float)n_current;

	// display
	fprintf(stdout,
		"%c[2K\rAccuracy: %.2f%% (%u of %u processed in %.2fs)",
		27,
		accuracy,
		n_current,
		n_items,
		elapsed_time
	);
	fflush(stdout);
}
