#include "../../../model.h"
#include "../local_push.h"
#include "../include/warning.h"


// constants
#define WAR_MSG "Model is not initialised. Initialise the model first before pushing."


// functions
const struct model_t* model_c_api_local_push_get_model()
{
	const struct model_t* model = model_set_get(0, MODEL_STORE_GET);
	WARNING(model, WAR_MSG);

	return model;
}
