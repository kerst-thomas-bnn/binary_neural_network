#include "../../../../include/debug.h"
#include "../../../../data/data.h"
#include "../../../../tensor/tensor.h"
#include "../../../../model/model.h"
#include "../local_push.h"


/*
 * Functions
 */
unsigned int model_c_api_local_push_predict_and_check(unsigned int index)
{
	/* fetch tensors */
	const struct tensor_t* x = data_get_local_input(index);
	const struct tensor_t* y = data_get_local_output(index);

	/* push and get prediction */
	const struct tensor_t* prediction = model_push(x);

	/* if prediction and y are identical, return 1, else 0 */
	return (tensor_compare(prediction, y)) ? 1 : 0;;
}
