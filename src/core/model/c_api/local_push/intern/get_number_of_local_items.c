#include "../../../../data/data.h"
#include "../local_push.h"
#include "../include/warning.h"


// constants
#define WAR_MSG "Local data has not been fetched. Fetch data before pushing."


// functions
unsigned int model_c_api_local_push_get_number_of_local_items()
{
	unsigned int n_items = data_get_number_of_local_test_items();
	WARNING(n_items, WAR_MSG);

	return n_items;
}
