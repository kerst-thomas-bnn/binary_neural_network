#include "../../../include/debug.h"
#include "../../../layer/layer.h"
#include "../../../tensor/tensor.h"
#include "../../model.h"
#include "../../def/def.h"
#include "../print.h"

#include <string.h>


// static variables
static size_t name_max_len;


// static functions
static void clear_screen_and_reset_cursor()
{
	fprintf(stdout, "\033[2J\033[0;0H");
}

static void determine_maximum_name_len(const struct model_t* model)
{
	name_max_len = 5;  // "input"

	for (unsigned int i = 0; i < model->n_layers; ++i) {
		const char* name = layer_get_name(model->layers[i]);
		size_t len = strlen(name);
		name_max_len = (len > name_max_len) ? len : name_max_len;
	}
}

static void print_header()
{
	fprintf(stdout, "Model:\n\n");
}

static void print_name(const char* name)
{
	// initialise
	char buffer[name_max_len + 1];
	memset(buffer, ' ', name_max_len);
	buffer[name_max_len] = 0;

	// copy the name
	memcpy(buffer, name, strlen(name));

	// print
	fprintf(stdout, "%s : (", buffer);
}

static void print_layer_input(const struct layer_t* layer)
{
	// get rank and dims
	unsigned int rank = layer_get_input_rank(layer);
	unsigned int* dims = layer_get_input_dims(layer);

	// print
	print_name("input");
	for (unsigned int i = 0; i < rank; ++i) {
		char* addendum = (i == rank - 1) ? ")\n" : ", ";
		fprintf(stdout, "%u%s", dims[i], addendum);
	}
}

static void print_layer_output(const struct layer_t* layer)
{
	// get rank and dims
	const struct tensor_t* output = layer_get_output(layer);
	unsigned int rank = tensor_get_rank(output);
	unsigned int* dims = tensor_get_dims(output);

	// get name
	const char* name = layer_get_name(layer);

	// display them
	print_name(name);
	for (unsigned int i = 0; i < rank; ++i) {
		char* addendum = (i == rank - 1) ? ")\n" : ", ";
		fprintf(stdout, "%u%s", dims[i], addendum);
	}
}

static void print_layers()
{
	// initialise
	const struct model_t* model = model_set_get(0, MODEL_STORE_GET);
	determine_maximum_name_len(model);

	// display the input layer
	print_layer_input(model->layers[0]);

	// dislay all output layers
	for (unsigned int i = 0; i < model->n_layers; ++i)
		print_layer_output(model->layers[i]);

	// finish
	fprintf(stdout, "\n");
}


// functions
void model_print()
{
	clear_screen_and_reset_cursor();
	print_header();
	print_layers();
}
