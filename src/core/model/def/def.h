#ifndef MODEL_DEF_H
#define MODEL_DEF_H


// forward declarations
struct layer_t;


// structs
struct model_t {
	char* path_model;
	char* path_weights;

	unsigned int n_layers;
	struct layer_t** layers;
};


#endif
