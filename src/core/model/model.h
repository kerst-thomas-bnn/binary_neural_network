#ifndef MODEL_H
#define MODEL_H


// forward declarations
struct model_t;
struct tensor_t;


// enumerations
enum model_store_op {
	MODEL_STORE_SET,
	MODEL_STORE_GET
};


// functions
struct model_t* model_set_get(struct model_t*, enum model_store_op);

const struct tensor_t* model_push(const struct tensor_t*);

const char* model_get_path_model(void);

const char* model_get_path_weights(void);

struct tensor_t* model_get_input_tensor(void);

void model_release(void);


#endif
