#include "../model.h"
#include "../def/def.h"


// functions
const char* model_get_path_model()
{
	struct model_t* model = model_set_get(0, MODEL_STORE_GET);
	return (model) ? model->path_model : 0;
}
