#include "../../layer/layer.h"
#include "../model.h"
#include "../def/def.h"


// functions
const struct tensor_t* model_push(const struct tensor_t* x)
{
	struct model_t* model = model_set_get(0, MODEL_STORE_GET);
	for (unsigned int i = 0; i < model->n_layers; ++i)
		x = layer_push(model->layers[i], x);

	return x;
}
