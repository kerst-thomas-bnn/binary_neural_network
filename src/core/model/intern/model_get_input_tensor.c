#include "../../layer/layer.h"
#include "../../tensor/tensor.h"
#include "../model.h"
#include "../def/def.h"


// functions
struct tensor_t* model_get_input_tensor()
{
	// fetch the model
	struct model_t* model = model_set_get(0, MODEL_STORE_GET);

	// fetch its first layer
	struct layer_t* first = model->layers[0];

	// get input rank and dims
	unsigned int rank = layer_get_input_rank(first);
	unsigned int* dims = layer_get_input_dims(first);

	// construct the tensor
	struct tensor_t* return_tensor = tensor_create(rank, dims, 0);

	return return_tensor;
}
