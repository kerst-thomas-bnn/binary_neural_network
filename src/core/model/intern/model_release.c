#include "../../include/memory.h"
#include "../../include/debug.h"
#include "../../include/error.h"
#include "../../layer/layer.h"
#include "../model.h"
#include "../def/def.h"


// static functions
static void release_layers(struct model_t* model)
{
	// if no mode is defined, return
	if (!model)
		return;

	// free the layers
	for (unsigned int i = 0; i < model->n_layers; ++i)
		layer_destroy(model->layers[i]);

	// free the layer array
	FREE(model->layers);
}


// functions
void model_release()
{
	// fetch the model
	struct model_t* model_stored = model_set_get(0, MODEL_STORE_GET);

	// release the strings
	FREE(model_stored->path_model);
	FREE(model_stored->path_weights);

	// release the layers and the model
	release_layers(model_stored);
	FREE(model_stored);

	// set stored model to 0
	struct model_t* model_zero = model_set_get(0, MODEL_STORE_SET);
	FATAL_ERROR(model_zero, "Unable to reset the stored model");
}
