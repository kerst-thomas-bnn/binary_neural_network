#include "../model.h"


// functions
struct model_t* model_set_get(struct model_t* model, enum model_store_op op)
{
	static struct model_t* model_stored = 0;
	if (op == MODEL_STORE_SET)
		model_stored = model;

	return model_stored;
}
