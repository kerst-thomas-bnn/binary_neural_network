#include "../../../c_api/bnn.h"
#include "../../include/debug.h"
#include "../../include/memory.h"
#include "../../tensor/tensor.h"
#include "../../model/model.h"
#include "../../data/data.h"
#include "../setup.h"
#include "../def/def.h"


// static functions
static void free_single_target(enum setup_io_identifier id)
{
	// fetch data
	struct data_io_shape_t *p = setup_set_get_io_shape(0, 0, id);

	// free shape
	p->rank = 0;
	FREE(p->shape);

	// free tensor
	p->n_bytes = 0;
	tensor_destroy(p->tensor);
}

static void free_targets(void)
{
	enum setup_io_identifier targets[] = {
		SETUP_IO_X,
		SETUP_IO_Y
	};
	for (int i = 0; i < sizeof(targets) / sizeof(*targets); ++i)
		free_single_target(targets[i]);
}

static void release_model(void)
{
	model_release();
}

static void release_data(void)
{
	data_release();
}


// functions
void bnn_easy_cleanup(void)
{
	free_targets();
	release_model();
	release_data();
}
