#include "../../../c_api/bnn.h"
#include "../../include/debug.h"
#include "../../include/error.h"
#include "../setup.h"

#include <stdarg.h>


// functions
void bnn_setup_input_shape(unsigned int rank, ...)
{
	// extract the shape
	unsigned int shape[rank];

	va_list ap;
	va_start(ap, rank);
	for (unsigned int i = 0; i < rank; ++i)
		shape[i] = va_arg(ap, unsigned int);

	va_end(ap);

	// store it
	void *r = setup_set_get_io_shape(rank, shape, SETUP_IO_X);
	FATAL_ERROR(r, "Unable to initialise the input shape");
}
