#ifndef SETUP_DEF_H
#define SETUP_DEF_H


// forward declarations
struct tensor_t;


// structs
struct data_io_shape_t {
	unsigned int rank;
	unsigned int *shape;
	unsigned int n_bytes;
	struct tensor_t *tensor;
};


#endif
