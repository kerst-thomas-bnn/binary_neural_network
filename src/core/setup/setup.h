#ifndef SETUP_H
#define SETUP_H


// forward declarations
struct tensor_t;


// enumaration
enum setup_io_identifier {
	SETUP_IO_X,
	SETUP_IO_Y,
};


// functions
void *setup_set_get_io_shape(unsigned int rank,
                             const unsigned int *shape,
                             enum setup_io_identifier id);

unsigned int setup_get_rank(enum setup_io_identifier id);

unsigned int *setup_get_shape(enum setup_io_identifier id);

unsigned int setup_get_n_bytes(enum setup_io_identifier id);

struct tensor_t *setup_get_tensor(enum setup_io_identifier id);


#endif
