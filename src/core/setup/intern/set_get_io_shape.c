#include "../../include/debug.h"
#include "../../include/array.h"
#include "../../include/memory.h"
#include "../../include/bitwise.h"
#include "../../tensor/tensor.h"
#include "../setup.h"
#include "../def/def.h"

#include <string.h>


// functions
void *setup_set_get_io_shape(unsigned int rank,
                             const unsigned int *shape,
                             enum setup_io_identifier id)
{
	static struct data_io_shape_t stored_shapes[] = {{0, 0, 0}, {0, 0, 0}};
	struct data_io_shape_t *p = &stored_shapes[id];

	// if rank is 0, return the stored data
	if (!rank)
		return p;

	// if not, then store the data
	// - rank
	p->rank = rank;

	// - shape
	MALLOC(p->shape, p->rank);
	memcpy(p->shape, shape, p->rank * sizeof(*p->shape));

	// - number of bytes
	unsigned int n_bits = array_product_ui(p->shape, p->rank);
	p->n_bytes = DIV8(n_bits) + (MOD8(n_bits) ? 1 : 0);

	// - tensor
	p->tensor = tensor_create(rank, shape, 0);

	// return 0 indicating successful initialisation
	return 0;
}
