#include "../setup.h"
#include "../def/def.h"


// functions
struct tensor_t *setup_get_tensor(enum setup_io_identifier id)
{
	struct data_io_shape_t *p = setup_set_get_io_shape(0, 0, id);

	return p->tensor;
}
