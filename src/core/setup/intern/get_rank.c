#include "../setup.h"
#include "../def/def.h"


// functions
unsigned int setup_get_rank(enum setup_io_identifier id)
{
	struct data_io_shape_t *p = setup_set_get_io_shape(0, 0, id);

	return p->rank;
}
