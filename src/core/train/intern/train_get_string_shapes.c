#include "../../include/debug.h"
#include "../../include/error.h"
#include "../../setup/setup.h"
#include "../train.h"

#include <string.h>


// constants
#define STRING_BUFFER 512
#define ERR_MSG "Network is not properly initialised, no data shape information could be found"


// static functions
static void write_shape(char* p,
                        enum setup_io_identifier identifier,
                        const char* prefix)
{
	// get rank and dims
	unsigned int rank = setup_get_rank(identifier);
	unsigned int* dims = setup_get_shape(identifier);
	FATAL_ERROR(!dims, ERR_MSG);

	// write to the string
	sprintf(p + strlen(p), "SHAPE_%s = [", prefix);
	for (unsigned int i = 0; i < rank; ++i)
		sprintf(p + strlen(p), "%u, ", dims[i]);

	sprintf(p + strlen(p) - 2, "]\n");
}


// functions
const char* train_get_string_shapes()
{
	// initialise
	static char string_shapes[STRING_BUFFER];
	string_shapes[0] = 0;
	sprintf(string_shapes, "# shapes\n");

	// get shapes
	write_shape(string_shapes, SETUP_IO_X, "X");
	write_shape(string_shapes, SETUP_IO_Y, "Y");

	// finalise the string
	sprintf(string_shapes + strlen(string_shapes), "\n\n");

	return string_shapes;
}
