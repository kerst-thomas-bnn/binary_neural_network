#include "../../include/debug.h"
#include "../../include/error.h"
#include "../../model/model.h"
#include "../train.h"

#include <string.h>


// constants
#define STRING_BUFFER 512
#define ERR_MSG "Network is not properly initialised, no model or weight information could be found"


// static functions
static void write_path(char* p, const char* (*fun)(void), const char* prefix)
{
	// fetch the path
	const char* path = fun();
	FATAL_ERROR(!path, ERR_MSG);

	// and write it
	sprintf(p + strlen(p), "PATH_%s = \"%s\"\n", prefix, path);
}


// functions
const char* train_get_string_model()
{
	// initialise
	static char string_model[STRING_BUFFER];
	string_model[0] = 0;
	sprintf(string_model, "# model paths\n");

	// add the paths
	write_path(string_model, &model_get_path_model, "MODEL");

	// finish the string
	sprintf(string_model + strlen(string_model), "\n\n");

	return string_model;
}
