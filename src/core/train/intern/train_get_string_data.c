#include "../train.h"

#include <stdio.h>


// constants
#define STRING_BUFFER 512
#define WRT_STRING_LITERATAL "# data paths\nPATH_DATA_TRAIN = \"%s\"\nPATH_DATA_TEST = \"%s\"\n\n\n"


// functions
const char* train_get_string_data(const char* train_p, const char* test_p)
{
	static char string_data[STRING_BUFFER];
	sprintf(string_data, WRT_STRING_LITERATAL, train_p, test_p);

	return string_data;
}
