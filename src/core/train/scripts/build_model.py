# build model
def build_model():
	# constants
	mode_dict = {
		"c": "convolution",
		"mp": "maximum_pooling",
		"ap": "average_pooling",
		"xp": "xnor_pooling",
		"f": "flattening",
		"i1": "inception_v1",
		"ko": "knockout",
		"bw": "bitwise",
		"s": "shift",
		"sx": "stacked_xnor"
	}


	# nonlocals
	model_args = []


	# static functions
	def add_layers_from_file():
		# nonlocals
		mode = None
		args = None

		# functions
		def get_mode():
			import sys
			nonlocal mode
			nonlocal args

			buffer = line[:line.find(':')]
			if buffer in mode_dict.keys():
				mode = mode_dict[buffer]
				args = {"mode": mode}

				return

			err_msg = (
				"Undefined mode in model definition: ",
				"\x1b[1;31m",
				buffer,
				"\x1b[0m. Aborting program execution.\n"
			)
			sys.stdout.write(''.join(err_msg))
			sys.exit()

			return

		def get_kernel():
			nonlocal args

			if mode in ("flattening", "knockout", "bitwise"):
				return

			buffer = line[line.find(':') + 2 : line.find(' -')]
			buffer = buffer.split(' ')
			kernel = [int(_b) for _b in buffer]

			args = {**args, "kernel": kernel}

			return

		def get_options():
			import sys
			nonlocal args

			pos = line.find(' -')
			if pos < 0:
				return

			options = line[pos + 2:].split(' -')
			for _o in options:
				option = _o[0]
				arg = _o[1:]
				if option == "s":
					args = {**args, "strides": int(arg)}

					continue

				if option == "d":
					args = {
						**args,
						"dropout": True,
						"dropout_rate": float(arg)
					}

					continue

				err_msg = (
					"Undefined option: \x1b[1;31m",
					_o,
					"\x1b[0m. ",
					"Aborting program execution.\n"
				)
				sys.stdout.write(''.join(err_msg))
				sys.exit()

			return


		# main
		f = open(PATH_MODEL, 'r')
		for line in f:
			if line.__len__() == 1:
				continue

			if line[0] == '#':
				continue

			get_mode()
			get_kernel()
			get_options()
			model_args.append(args)

		f.close()

		return

	def initialise_weights():
		import tensorflow as tf

		x = tf.zeros(shape=[1, *SHAPE_X], dtype=tf.float32)
		model(x)

		return


	# main
	add_layers_from_file()
	model = network(*model_args)
	model.add_accumulation(SHAPE_Y[-1])
	initialise_weights()

	return model
