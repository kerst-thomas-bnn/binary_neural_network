# write protocol
def write_protocol():
	# nonlocals
	protocol_path = None


	# static functions
	def get_protocol_path():
		nonlocal protocol_path

		protocol_path = PATH_MODEL.split('/')
		protocol_path = protocol_path[:-1]
		protocol_path.append("protocol.txt")
		protocol_path = '/'.join(protocol_path)

		return

	def clean_protocol_on_first_epoch():
		if epoch != 1:
			return

		with open(protocol_path, "wb") as f:
			pass

		return

	def append_protocol():
		import time

		# prepare string parts
		string_parts = (
			"Epoch {:4d}: ",
			"{:.2f}%% / {:.2f}%% / {:.2f}%% ",
			"[{:.0f}s]",
			"{:s}"
			"\n"
		)
		elapsed_time = time.time() - time_start
		if accuracy_test_max < accuracy_test:
			save_weight_string = " (saving weights)"
		else:
		 	save_weight_string = ""

		# construct string
		write_string = ''.join(string_parts).format(
				epoch,
				accuracy_train,
				accuracy_test,
				max(accuracy_test, accuracy_test_max),
				elapsed_time,
				save_weight_string
			)

		# write string
		with open(protocol_path, "a") as f:
			f.write(write_string)

		return


	# main
	get_protocol_path()
	clean_protocol_on_first_epoch()
	append_protocol()

	return
