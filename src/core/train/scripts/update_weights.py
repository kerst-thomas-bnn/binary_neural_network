# update_weights
def update_weights(model):
	# constants
	optmsr = tf.keras.optimizers.Adam()
	loss_fn = tf.keras.losses.hinge


	# nonlocals
	x = None
	y = None
	p = None
	n_correct = 0


	# static functions
	def start_measuring_time():
		import time
		global time_start

		time_start = time.time()

		return

	def set_train():
		model.set_train()

		return

	def shuffle_data():
		from random import shuffle

		shuffle(data_train)

		return

	def construct_data_slice():
		import numpy as np
		nonlocal x
		nonlocal y

		start = i * BATCH_SIZE
		stop = start + BATCH_SIZE
		x = np.stack([_d.x for _d in data_train[start : stop]], axis=0)
		y = np.stack([_d.y for _d in data_train[start : stop]], axis=0)

		return

	def apply_gradients():
		nonlocal p

		with tf.GradientTape(persistent=False) as tape:
			# losses
			p = model(x)
			loss = loss_fn(y, p)
			loss += sum(model.losses)

		# apply gradients and clip weights
		grads = tape.gradient(loss, model.trainable_variables)
		optmsr.apply_gradients(zip(grads, model.trainable_variables))
		model.clip_weights()

		return

	def compute_accuracy_train():
		global accuracy_train
		nonlocal n_correct

		axis = p.shape.__len__() - 1
		if axis == 0:
			_p = p.numpy().reshape(y.shape)
			prod = tf.math.multiply(_p, y)
			prod = tf.sign(prod)
			prod = tf.clip_by_value(prod, 0, 1)
			n_correct += tf.reduce_sum(prod).numpy()
		else:
			_p = tf.math.argmax(p, axis=axis).numpy().reshape(-1)
			_y = np.argmax(y, axis=axis).reshape(-1)
			n_correct += sum(_p == _y)

		numerator = 100.0 * float(n_correct)
		denominator = float(y.shape[0] * (i + 1))
		accuracy_train = numerator / denominator

		return

	def display_progress():
		import sys
		import time

		# prepare print variables
		display_string_parts = (
			"\r\x1b[2K",
			"Epoch {:4d}: ",
			"{:.2f}%% / --.--%% / --.--%% ",
			"[{:.0f}s] ",
			"- {:.2f}%%"
		)
		time_elapsed = time.time() - time_start
		progress = 100.0 * float(i + 1) / float(n_batches)

		# print
		sys.stdout.write(
			''.join(display_string_parts).format(
				epoch,
				accuracy_train,
				time_elapsed,
				progress
			)
		)
		sys.stdout.flush()

		return


	# main
	start_measuring_time()
	set_train()
	shuffle_data()
	n_batches = int(data_train.__len__() / BATCH_SIZE)
	for i in range(n_batches):
		construct_data_slice()
		apply_gradients()
		compute_accuracy_train()
		display_progress()


	return
