# globals
data_train = None
data_test = None
epoch = 0
accuracy_train = 0.0
accuracy_test = 0.0
accuracy_test_max = 0.0
use_confusion = False
time_start = None
confusion_matrix = None
