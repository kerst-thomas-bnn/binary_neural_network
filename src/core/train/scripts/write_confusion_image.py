# write_confusion_image
def write_confusion_image():
	# constants
	FOLDER_NAME = "confusion"
	COLOR = (34 / 255, 94 / 255, 190 / 255)


	# nonlocals
	image_path = None
	fig = None


	# static functions
	def reset_images():
		import os
		import shutil

		if epoch != 1:
			return

		# build the path to the folder
		path_folder = PATH_MODEL.split("/")
		path_folder = path_folder[:-1]
		path_folder.append(FOLDER_NAME)
		path_folder = "/".join(path_folder)

		# delete the folder
		shutil.rmtree(path_folder, ignore_errors=True)

		# add a new folder
		os.mkdir(path_folder)

		return

	def set_image_path():
		nonlocal image_path

		p = PATH_MODEL.split("/")
		p = p[:-1]
		p.append(FOLDER_NAME)
		p.append("{:04d}.png".format(epoch))
		image_path = "/".join(p)

		return

	def build_image():
		import matplotlib.pyplot as plt
		import numpy as np
		nonlocal fig

		# set up and canvas
		fig = plt.figure()
		len = confusion_matrix.shape[0]

		# axes
		ax = plt.gca()
		ax.get_xaxis().set_tick_params(which='major', length=0)
		ax.get_yaxis().set_tick_params(which='major', length=0)

		# limits
		plt.xlim([0, len])
		plt.ylim([0, len])

		# ticks
		ticks = [i + .5 for i in range(len)]
		ticklabels_x = [str(i) for i in range(len)]
		ticklabels_y = [str(len - i - 1) for i in range(len)]
		plt.xticks(ticks, ticklabels_x)
		plt.yticks(ticks, ticklabels_y)

		# labels
		plt.xlabel("Prediction")
		plt.ylabel("Label")
		ax.yaxis.set_label_coords(-.07, .5)
		ax.xaxis.set_label_coords(.5, -.08)

		# title
		title_str = "Epoch {:4d}: {:2.2f}%% Test Error Rate"
		title_str = title_str.format(epoch, 100.0 - accuracy_test)
		plt.title(title_str, loc="left")

		# contents
		for lab in range(len):
			total = np.sum(confusion_matrix[lab, :])
			for pred in range(len):
				p = confusion_matrix[lab, pred] / total
				kwargs = {
					"x": [pred, pred + 1],
					"y1": [len - lab - 1],
					"y2": [len - lab],
					"color": np.multiply(p, COLOR)
				}
				plt.fill_between(**kwargs)

		return

	def save_image():
		import matplotlib.pyplot as plt

		plt.savefig(image_path, bbox_inches='tight', dpi=400)
		plt.close()

		return


	# main
	reset_images()
	set_image_path()
	build_image()
	save_image()

	return
