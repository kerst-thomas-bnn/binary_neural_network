# save weights
def save_weights(model):
	# static functions
	def are_current_weights_better():
		import sys

		if accuracy_test < accuracy_test_max:
			sys.stdout.write('\n')

			return False

		return True

	def update_accuracy():
		global accuracy_test_max

		accuracy_test_max = accuracy_test

		return

	def announce_weight_saving():
		import sys

		sys.stdout.write(" (saving weights)\n");

		return

	def write_weights():
		# build path
		f_path = PATH_MODEL.split('/')
		f_path = f_path[:-1]
		f_path.append("weights")
		f_path = '/'.join(f_path)

		# write the weights
		with open(f_path, "wb") as f:
			model.save(f)

		return


	# main
	if not are_current_weights_better():
		return

	update_accuracy()
	announce_weight_saving()
	write_weights()

	return
