# network
class network(layers.Layer):
	# imports
	import os
	import sys


	def __init__(self, *layers):
		# main
		super(network, self).__init__()
		self.layers = []
		self.is_train = False

		# add layers
		for _l in layers:
			mode = _l['mode']
			_l.pop('mode')
			self.layers.append(LAYERS_DICT[mode](**_l))

		return

	def add_accumulation(self, n_accumulations):
		self.layers.append(accumulation(n_accumulations))

		return

	def set_train(self):
		for _l in self.layers:
			_l.train=True

		return

	def set_test(self):
		for _l in self.layers:
			_l.train=False

		return

	def clip_weights(self):
		for _l in self.layers:
			_l.clip_weights()

		return


	def save(self, f):
		for _l in self.layers:
			_l.save(f)

		return

	def call(self, inputs):
		for _l in self.layers:
			inputs = _l(inputs)

		return inputs
