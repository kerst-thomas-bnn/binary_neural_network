# load data
def load_data():
	# non-locals
	bytes_x = 0
	bytes_y = 0
	bytes_total = 0


	# static functions
	def determine_sizes():
		import numpy as np
		nonlocal bytes_x
		nonlocal bytes_y
		nonlocal bytes_total

		bytes_x = int(np.ceil(np.prod(SHAPE_X) / 8))
		bytes_y = int(np.ceil(np.prod(SHAPE_Y) / 8))
		bytes_total = bytes_x + bytes_y

		return

	def fetch_data(file_path):
		import os
		import numpy as np
		import struct as st

		# initialise
		n = int(os.path.getsize(file_path) / bytes_total)
		f = open(file_path, 'rb')
		items = []

		# fetch and format data
		for i in range(n):
			# x
			x = st.unpack(str(bytes_x) + 'B', f.read(bytes_x))
			x = np.array(x, dtype=np.uint8)
			x = np.unpackbits(x).astype(np.float32)
			x[x == 0] = -1.0
			x = x.reshape(SHAPE_X, order='C')

			# y
			y = st.unpack(str(bytes_y) + 'B', f.read(bytes_y))
			y = np.array(y, dtype=np.uint8)
			y = np.unpackbits(y).astype(np.float32)
			y = y[:np.prod(SHAPE_Y)]
			y[y == 0] = -1.0
			y = y.reshape(SHAPE_Y, order='C')

			# append
			items.append(Item(x, y))

		# clean up
		f.close()

		return items


	# main
	def main():
		import sys
		global data_train
		global data_test

		sys.stdout.write("\rLoading data...")
		sys.stdout.flush()

		determine_sizes()
		data_train = fetch_data(PATH_DATA_TRAIN)
		data_test = fetch_data(PATH_DATA_TEST)

		# print message
		msg = "\rLoaded {:d} training items and {:d} test items\n\n"
		msg = msg.format(data_train.__len__(), data_test.__len__())
		sys.stdout.write(msg)

		return

	main()

	return
