# main
def main():
	global epoch

	load_data()
	model = build_model()

	while True:
		epoch += 1
		update_weights(model)
		test(model)
		write_protocol()
		if use_confusion:
			write_confusion_image()
			write_confusion_matrix()

		save_weights(model)

	return


if __name__ == '__main__':
	main()
