# write_confusion_matrix
def write_confusion_matrix():
	# nonlocals
	write_path = None


	# static functions
	def set_write_path():
		nonlocal write_path

		write_path = PATH_MODEL.split('/')
		write_path = write_path[:-1]
		write_path.append("confusion_matrix.md")
		write_path = '/'.join(write_path)

		return

	def write_matrix():
		# open file
		f = open(write_path, "w")

		# write header
		len = confusion_matrix.shape[0]
		f.write("| l/p ")
		for i in range(len):
			f.write("| {:d} ".format(i))

		f.write("|\n")

		# write matrix line
		for i in range(len + 1):
			f.write("| :-: ")

		f.write("|\n")

		# write lines
		for i in range(len):
			f.write("| **{:d}**".format(i))
			# write columns
			for j in range(len):
				f.write("| ")
				if i == j:
					f.write("*")

				val = confusion_matrix[i, j]
				f.write("*{:d}*".format(val))
				if i == j:
					f.write("*")

			f.write("|\n")

		# clean up
		f.close()

		return


	# main
	if not accuracy_test_max < accuracy_test:
		return

	set_write_path()
	write_matrix()

	return
