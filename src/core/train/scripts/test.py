# test
def test(model):
	# nonlocals
	x = None
	y = None
	p = None
	n_correct = 0
	n_total = 0


	# static functions
	def set_test():
		model.set_test()

		return

	def reset_confusion_matrix():
		global confusion_matrix

		confusion_matrix = None

		return

	def build_x():
		nonlocal x

		x = [d.x for d in data_test[i : i + BATCH_SIZE]]
		x = np.stack(x, axis=0)

		return

	def build_y():
		nonlocal y

		y = [d.y for d in data_test[i : i + BATCH_SIZE]]
		y = np.stack(y, axis=0)

		return

	def predict():
		nonlocal p

		p = model(x)

		return

	def populate_confusion_matrix():
		if not use_confusion:
			return

		global confusion_matrix

		# initialise zeros if the confusion matrix is not set up yet
		if confusion_matrix is None:
			len = y.shape[-1]
			shp = [len, len]
			confusion_matrix = np.zeros(shape=shp, dtype=np.int32)

		# iterate through each prediction and label
		axis = p.shape.__len__() - 1
		pred = tf.math.argmax(p, axis=axis).numpy().reshape(-1)
		label = np.argmax(y, axis=axis).reshape(-1)

		for _p, _l in zip(pred, label):
			confusion_matrix[_l, _p] += 1

		return

	def compute_accuracy():
		nonlocal n_correct
		nonlocal n_total
		global accuracy_test

		# compare prediction and label
		axis = p.shape.__len__() - 1
		if axis == 0:
			_p = p.numpy().reshape(y.shape)
			prod = tf.math.multiply(_p, y)
			prod = tf.sign(prod)
			prod = tf.clip_by_value(prod, 0, 1)
			n_correct += tf.reduce_sum(prod).numpy()
			n_total += p.shape[0]
		else:
			_p = tf.math.argmax(p, axis=axis).numpy().reshape(-1)
			_y = np.argmax(y, axis=axis).reshape(-1)
			n_correct += sum(_p == _y)
			n_total += _p.__len__()

		accuracy_test = 100 * float(n_correct) / float(n_total)

		return

	def display_test_accuarcy():
		import sys
		import time

		# prepare print variables
		display_string_parts = (
			"\r\x1b[2K",
			"Epoch {:4d}: ",
			"{:.2f}%% / {:.2f}%% / --.--%% ",
			"[{:.0f}s] ",
			"- {:.2f}%%"
		)
		time_elapsed = time.time() - time_start
		progress = 100.0 * float(n_total) / float(data_test.__len__())

		# print
		sys.stdout.write(
			''.join(display_string_parts).format(
				epoch,
				accuracy_train,
				accuracy_test,
				time_elapsed,
				progress
			)
		)
		sys.stdout.flush()

		return

	def display_test_final():
		import sys
		import time

		# prepare print variables
		display_string_parts = (
			"\r\x1b[2K",
			"Epoch {:4d}: ",
			"{:.2f}%% / {:.2f}%% / {:.2f}%% ",
			"[{:.0f}s]"
		)
		time_elapsed = time.time() - time_start

		# print
		sys.stdout.write(
			''.join(display_string_parts).format(
				epoch,
				accuracy_train,
				accuracy_test,
				max(accuracy_test, accuracy_test_max),
				time_elapsed,
			)
		)
		sys.stdout.flush()

		return


	# main
	set_test()
	reset_confusion_matrix()
	for i in range(0, data_test.__len__(), BATCH_SIZE):
		build_x()
		build_y()
		predict()
		populate_confusion_matrix()
		compute_accuracy()
		display_test_accuarcy()

	display_test_final()

	return
