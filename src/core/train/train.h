#ifndef TRAIN_H
#define TRAIN_H


// functions
const char *train_get_string_shapes(void);

const char *train_get_string_model(void);

const char *train_get_string_data(const char*, const char*);


#endif
