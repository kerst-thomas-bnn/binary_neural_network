/* DO NOT MOVE PYTHON INCLUDE AWAY FROM HERE */
#include "../include/python.h"
/* DO NOT MOVE PYTHON INCLUDE AWAY FROM HERE */

#include "../../../c_api/bnn.h"
#include "../../include/debug.h"
#include "../../include/memory.h"
#include "../../include/array.h"
#include "../include/training_script.h"
#include "../train.h"

#include <string.h>


/*
 * static functions
 */
static char *build_full_script(unsigned int n_strings, const char **strings)
{
	/* get total script length */
	size_t len = strlen(TRAINING_SCRIPT);
	for (unsigned int i = 0; i < n_strings; ++i)
		len += strlen(strings[i]);

	/* allocate memory */
	char *script;
	MALLOC(script, len + 1);

	/* copy the preceeding strings */
	script[0] = 0;
	for (unsigned int i = 0; i < n_strings; ++i)
		sprintf(script + strlen(script), "%s", strings[i]);

	/* copy the script */
	sprintf(script + strlen(script), TRAINING_SCRIPT);
	script[len] = 0;

	return script;
}


/*
 * functions
 */
void bnn_train_run(const char *path_train, const char *path_test)
{
	/* get preceeding strings */
	const char *strings[] = {
		train_get_string_shapes(),
		train_get_string_model(),
		train_get_string_data(path_train, path_test)
	};

	/* compile the script */
	unsigned int n_strings = ARRAY_LEN(strings);
	char *script = build_full_script(n_strings, strings);

	/* run it */
	Py_Initialize();
	PyRun_SimpleString(script);
	Py_Finalize();

	/* clean up */
	FREE(script);
}
