#ifndef OPT_H
#define OPT_H


// forward declarations
struct layer_t;


// functions
void opt_optimise_all_layers(void);

void opt_add_optimisation(struct layer_t *layer,
                          void (*fun)(struct layer_t *layer, unsigned int k),
                          unsigned int index_start,
                          unsigned int index_stop);


#endif
