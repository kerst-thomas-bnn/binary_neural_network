#include "../../../include/debug.h"
#include "../../../include/memory.h"
#include "../mem.h"

#include <string.h>


// static functions
static void add_instruction(struct opt_set_t *set,
                            const struct opt_instruction_t *p)
{
	// increment
	++set->n_instructions;

	// expand the memory
	REALLOC(set->instructions, set->n_instructions);

	// copy the instruction content
	struct opt_instruction_t *tar;
	tar = &set->instructions[set->n_instructions - 1];
	memcpy(tar, p, sizeof(*p));
}

static void reset_instructions(struct opt_set_t *set)
{
	set->n_instructions = 0;
	FREE(set->instructions);
}


// functions
const struct opt_set_t *opt_mem_add_get(const struct opt_instruction_t *inst,
                                        enum opt_mem_flag flag)
{
	// statically stored instructions
	static struct opt_set_t stored_instructions = {0, 0};

	// case-by-case treatment
	switch (flag) {
	case OPT_MEM_ADD:
		add_instruction(&stored_instructions, inst);
		break;

	case OPT_MEM_GET:
		break;

	case OPT_MEM_RESET:
		reset_instructions(&stored_instructions);
		break;

	default:
		break;
	}

	return &stored_instructions;
}
