#ifndef OPT_MEM_H
#define OPT_MEM_H


// forward declarations
struct layer_t;


// enumerations
enum opt_mem_flag {
	OPT_MEM_ADD,
	OPT_MEM_GET,
	OPT_MEM_RESET
};


// structs
struct opt_instruction_t {
	struct layer_t *layer;
	void (*opt_function)(struct layer_t*, unsigned int);
	unsigned int index_start;
	unsigned int index_end;
};

struct opt_set_t {
	unsigned int n_instructions;
	struct opt_instruction_t *instructions;
};


// functions
const struct opt_set_t *opt_mem_add_get(const struct opt_instruction_t* inst,
                                        enum opt_mem_flag flag);


#endif
