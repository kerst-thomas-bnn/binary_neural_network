#include "../../include/debug.h"
#include "../../model/model.h"
#include "../../tensor/tensor.h"
#include "../opt.h"
#include "../mem/mem.h"

#include <stdio.h>
#include <sys/time.h>


// constants
#define N_TRIALS 100


// static variables
static unsigned int n_tests_done;
static unsigned int n_tests_total;


// static functions
static void preset_all_layers_to_default(const struct opt_set_t *s)
{
	for (unsigned int i = 0; i < s->n_instructions; ++i) {
		const struct opt_instruction_t *p = &s->instructions[i];
		p->opt_function(p->layer, p->index_start);
	}
}

static void run_single_trial(const struct tensor_t *input)
{
	// run
	model_push(input);
	++n_tests_done;

	// print
	const char msg[] = "\rOptimising network: %u%% (%u of %u steps)";
	unsigned int progress = 100 * n_tests_done / n_tests_total;

	fprintf(stdout, msg, progress, n_tests_done, n_tests_total);
	fflush(stdout);
}

static double testrun_network(const struct tensor_t *input)
{
	// initialise measurement
	struct timeval t_start;
	gettimeofday(&t_start, 0);

	// measure
	for (unsigned int i = 0; i < N_TRIALS; ++i)
		run_single_trial(input);

	// finish measurement
	struct timeval t_end;
	gettimeofday(&t_end, 0);
	double t_usec = (double)(t_end.tv_usec - t_start.tv_usec) / 1000000;
	double t_sec = (double)(t_end.tv_sec - t_start.tv_sec);

	return t_usec + t_sec;
}

static unsigned int pick_shortest_time(unsigned int n, const double *times)
{
	// initialise
	unsigned int pos = 0;
	double shortest = times[0];

	// find the shortest time
	for (unsigned int i = 0; i < n; ++i) {
		if (times[i] < shortest) {
			pos = i;
			shortest = times[i];
		}
	}

	return pos;
}

static void optimise_layer(const struct opt_instruction_t *p)
{
	// set up the test
	struct tensor_t *input = model_get_input_tensor();
	double elapsed_times[p->index_end - p->index_start];

	// test
	for (unsigned int i = p->index_start; i < p->index_end; ++i) {
		// select the function
		p->opt_function(p->layer, i);

		// measure the execution time
		elapsed_times[i - p->index_start] = testrun_network(input);
	}

	// pick the fastest function
	unsigned int n = p->index_end - p->index_start;
	unsigned int pos = pick_shortest_time(n, elapsed_times);
	p->opt_function(p->layer, pos + p->index_start);

	// clean up
	tensor_destroy(input);
}


// functions
void opt_optimise_all_layers(void)
{
	// fetch the stored instructions
	const struct opt_set_t *set;
	set = opt_mem_add_get(0, OPT_MEM_GET);

	// if no optimisations are stored, return
	if (!set->n_instructions)
		return;

	// initialise
	n_tests_done = 0;
	n_tests_total = 0;
	preset_all_layers_to_default(set);
	for (unsigned int i = 0; i < set->n_instructions; ++i) {
		const struct opt_instruction_t *p = &set->instructions[i];
		n_tests_total += (p->index_end - p->index_start) * N_TRIALS;
	}

	// if instructions are stored, go through them one-by-one
	for (unsigned int i = 0; i < set->n_instructions; ++i)
		optimise_layer(&set->instructions[i]);

	// after all is optimised, display final message
	const char msg[] = "\rOptimising network: 100%% (%u of %u steps)\n\n";
	fprintf(stdout, msg, n_tests_done, n_tests_total);

	// clean up
	opt_mem_add_get(0, OPT_MEM_RESET);
}
