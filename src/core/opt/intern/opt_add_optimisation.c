#include "../../include/debug.h"
#include "../opt.h"
#include "../mem/mem.h"


// functions
void opt_add_optimisation(struct layer_t *layer,
                          void (*fun)(struct layer_t *layer, unsigned int k),
                          unsigned int index_start,
                          unsigned int index_stop)
{
	struct opt_instruction_t set = {layer, fun, index_start, index_stop};
	opt_mem_add_get(&set, OPT_MEM_ADD);
}
