#ifndef BITS_H
#define BITS_H


// forward declarations
struct bits_t;


// ----------------------------------------------------------------------------
// --- create and destroy ---
// ----------------------------------------------------------------------------

struct bits_t *bits_create(void *data, unsigned int n_bits);
// creates a bit array using either freshly dynamically allocated memory or on
// top of on an existing data structure
// accepts:
// 	(void *data) data on top of which the bit array is created, if 0 is
// 	             passed, then the memory will be allocated and initialised
// 	             to 0
// 	(unsigned int n_bits) number of bits the bit array represents
// returns:
// 	(struct bits_t *) the generated bit array

void bits_destroy(struct bits_t *bits);
// frees all memory associated by a bit array
// accepts:
// 	(struct bits_t *bits) bit array to be destroyed


// ----------------------------------------------------------------------------
// --- member access ---
// ----------------------------------------------------------------------------

unsigned char bits_get_bit(const struct bits_t *bits, unsigned int pos);
// fetches a single bit of a bit array
// accepts:
// 	(const struct bits_t *bits) bit array from which the bit is fetched
// 	(unsigned int pos) number of the bit to be fetched
// returns:
// 	(unsigned char) the bit value, either 0 or 1

unsigned char *bits_get_bytes(const struct bits_t *bits);
// fetches the bytes on which the bit array is constructed on
// accepts:
// 	(const struct bits_t *bits) bit array from on top of the bytes
// returns:
// 	(unsigned char *) bytes on which the bit array rests

unsigned int bits_get_n_bits(const struct bits_t *bits);
// returns number of represented bits
// accepts:
// 	(const struct bits_t *bits) bit array
// returns:
// 	(unsigned int) number of represented bits

unsigned int bits_get_n_bytes(const struct bits_t *bits);
// returns number of represented bytes
// accepts:
// 	(const struct bits_t *bits) bit array
// returns:
// 	(unsigned int) number of represented bytes

unsigned int bits_get_n_last(const struct bits_t *bits);
// returns number of relevant bits in last represented byte
// accepts:
// 	(const struct bits_t *bits) bit array
// returns:
// 	(unsigned int) number of bits in last represented byte


// ----------------------------------------------------------------------------
// --- modification ---
// ----------------------------------------------------------------------------

void bits_flip_up(struct bits_t *bits, unsigned int pos);
// set a specific bit to 1
// accepts:
// 	(const struct bits_t *bits) bit array
// 	(unsigned int pos) number of bit to be set to 1

void bits_flip_down(struct bits_t *bits, unsigned int pos);
// set a specific bit to 0
// accepts:
// 	(const struct bits_t *bits) bit array
// 	(unsigned int pos) number of bit to be set to 0

void bits_flip(struct bits_t *bits, unsigned int pos);
// toggles a specific bit from either 1 to 0 or from 0 to 1
// accepts:
// 	(const struct bits_t *bits) bit array
// 	(unsigned int pos) number of bit to be toggled


// ----------------------------------------------------------------------------
// --- xnor ---
// ----------------------------------------------------------------------------

unsigned int bits_xnor_uc(const struct bits_t *bits, const unsigned char *uc);
// computes the xnor of a bit array with a target and counts the number of 1s,
// using the unsigned char representation of the data
// accepts:
// 	(const struct bits_t *bits) bit array
// 	(const unsigned char *uc) data against which xnor is performed
// returns:
// 	(unsigned int) the number of 1s

unsigned int bits_xnor_ui(const struct bits_t *bits, const unsigned char *ui);
// computes the xnor of a bit array with a target and counts the number of 1s,
// using the unsigned int representation of the data
// accepts:
// 	(const struct bits_t *bits) bit array
// 	(const unsigned char *ui) data against which xnor is performed
// returns:
// 	(unsigned int) the number of 1s

unsigned int bits_xnor_ul(const struct bits_t *bits, const unsigned char *ul);
// computes the xnor of a bit array with a target and counts the number of 1s,
// using the unsigned long representation of the data
// accepts:
// 	(const struct bits_t *bits) bit array
// 	(const unsigned char *ul) data against which xnor is performed
// returns:
// 	(unsigned int) number of 1s

unsigned int bits_xnor_ull(const struct bits_t *bits, const unsigned char *ul);
// computes the xnor of a bit array with a target and counts the number of 1s,
// using the unsigned long long representation of the data
// accepts:
// 	(const struct bits_t *bits) bit array
// 	(const unsigned char *ul) data against which xnor is performed
// returns:
// 	(unsigned int) number of 1s


// ----------------------------------------------------------------------------
// --- miscellaneous ---
// ----------------------------------------------------------------------------

void bits_display(const struct bits_t *bits);
// displays the bit array and its content
// accepts:
// 	(const struct bits_t *bits) bit array


#endif
