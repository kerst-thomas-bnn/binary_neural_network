#ifndef BITS_DEF_H
#define BITS_DEF_H


// structs
struct bits_t {
	unsigned char *bytes;
	unsigned int n_bits;
	unsigned int n_bytes;
	unsigned int n_last;

	unsigned char allocated_dynamically;
};


#endif
