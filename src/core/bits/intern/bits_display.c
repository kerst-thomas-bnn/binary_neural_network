#include "../bits.h"
#include "../def/def.h"

#include <stdio.h>


// static functions
static void display_byte_full(const unsigned char *p)
{
	unsigned char byte = *p;
	for (unsigned char mask = 128; mask; mask >>= 1) {
		unsigned char bit_val = (byte & mask) ? 1 : 0;
		fprintf(stdout, "%u", bit_val);
	}
	fprintf(stdout, " ");
}

static void display_byte_partial(const unsigned char *p, unsigned int n)
{
	char byte = *p;
	for (unsigned char mask = 128; mask; mask >>= 1) {
		unsigned char bit_val = (byte & mask) ? 1 : 0;
		fprintf(stdout, "%u", bit_val);

		if (!--n)
			return;
	}
}


// functions
void bits_display(const struct bits_t *bits)
{
	const unsigned char *byte_it = bits->bytes;

	// display all but the last byte
	for (size_t i = 0; i < bits->n_bytes - 1; ++i)
		display_byte_full(byte_it++);

	// display the last byte, print new line
	display_byte_partial(byte_it, bits->n_last);
	fprintf(stdout, "\n");
}
