#include "../../include/bitwise.h"
#include "../bits.h"
#include "../def/def.h"


// functions
void bits_flip(struct bits_t *bits, unsigned int pos)
{
	bits->bytes[DIV8(pos)] ^= 128 >> MOD8(pos);
}

void bits_flip_up(struct bits_t *bits, unsigned int pos)
{
	bits->bytes[DIV8(pos)] |= 128 >> MOD8(pos);
}

void bits_flip_down(struct bits_t *bits, unsigned int pos)
{
	bits->bytes[DIV8(pos)] &= ~(128 >> MOD8(pos));
}
