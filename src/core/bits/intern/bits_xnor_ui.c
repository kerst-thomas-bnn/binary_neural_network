#include "../bits.h"
#include "../def/def.h"


// functions
unsigned int bits_xnor_ui(const struct bits_t *bits, const unsigned char *ui)
{
	unsigned int n = bits->n_bytes;
	unsigned int i = 0;
	int internal_field = 0;

	// pointers
	unsigned int *b_ui = (unsigned int*)bits->bytes;
	unsigned int *p_ui = (unsigned int*)ui;

	// all but last xnor 64-bit
	for (; i < n / sizeof(unsigned int); ++i) {
		unsigned int xor_val = b_ui[i] ^ p_ui[i];
		internal_field += __builtin_popcountll(xor_val);
	}
	i *= sizeof(unsigned int);

	// rest 8-bit
	for (; i < n; ++i) {
		unsigned int xor_val = (unsigned int)(bits->bytes[i] ^ ui[i]);
		internal_field += __builtin_popcountll(xor_val);
	}

	return (unsigned int)internal_field;
}
