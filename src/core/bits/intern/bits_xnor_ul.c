#include "../bits.h"
#include "../def/def.h"


// functions
unsigned int bits_xnor_ul(const struct bits_t *bits, const unsigned char *ul)
{
	unsigned int n = bits->n_bytes;
	unsigned int i = 0;
	int internal_field = 0;

	// pointers
	unsigned long *b_ul = (unsigned long*)bits->bytes;
	unsigned long *p_ul = (unsigned long*)ul;

	// all but last xnor 64-bit
	for (; i < n / sizeof(unsigned long); ++i) {
		unsigned long xor_val = b_ul[i] ^ p_ul[i];
		internal_field += __builtin_popcountll(xor_val);
	}
	i *= sizeof(unsigned long);

	// rest 8-bit
	for (; i < n; ++i) {
		unsigned long xor_val = (unsigned long)(bits->bytes[i] ^ ul[i]);
		internal_field += __builtin_popcountll(xor_val);
	}

	return (unsigned int)internal_field;
}
