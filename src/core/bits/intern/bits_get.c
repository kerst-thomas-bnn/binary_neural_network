#include "../../include/bitwise.h"
#include "../bits.h"
#include "../def/def.h"


// functions
unsigned char bits_get_bit(const struct bits_t *bits, unsigned int pos)
{
	unsigned char byte = bits->bytes[DIV8(pos)];
	unsigned char mask = 128 >> MOD8(pos);
	unsigned char bit_val = (byte & mask) ? 1 : 0;

	return bit_val;
}

unsigned char *bits_get_bytes(const struct bits_t *bits)
{
	return bits->bytes;
}

unsigned int bits_get_n_bits(const struct bits_t *bits)
{
	return bits->n_bits;
}

unsigned int bits_get_n_bytes(const struct bits_t *bits)
{
	return bits->n_bytes;
}

unsigned int bits_get_n_last(const struct bits_t *bits)
{
	return bits->n_last;
}
