#include "../bits.h"
#include "../def/def.h"


// static functions
static inline unsigned long long get_xor_val(const struct bits_t *b,
                                             const unsigned char *p,
                                             unsigned int i)
{
	return (unsigned long long)(b->bytes[i] ^ p[i]);
}


// functions
unsigned int bits_xnor_ull(const struct bits_t *bits, const unsigned char *ul)
{
	// initialise
	unsigned int n = bits->n_bytes;
	unsigned int i = 0;
	int internal_field = 0;

	// pointers
	unsigned long long *b_ull = (unsigned long long*)bits->bytes;
	unsigned long long *p_ull = (unsigned long long*)ul;

	// all but last xnor 64-bit
	for (; i < n / sizeof(unsigned long long); ++i) {
		unsigned long long xor_val = b_ull[i] ^ p_ull[i];
		internal_field += __builtin_popcountll(xor_val);
	}
	i *= sizeof(unsigned long long);

	// rest 8-bit
	for (; i < n; ++i) {
		unsigned long long xor_val = get_xor_val(bits, ul, i);
		internal_field += __builtin_popcountll(xor_val);
	}

	return (unsigned int)internal_field;
}
