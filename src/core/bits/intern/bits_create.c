#include "../../include/bitwise.h"
#include "../../include/memory.h"
#include "../bits.h"
#include "../def/def.h"

#include <stdlib.h>


// functions
struct bits_t *bits_create(void *data, unsigned int n_bits)
{
	// initialise
	struct bits_t *p = SALLOC(p);

	// how many bits in the last byte are relevant?
	p->n_bits = n_bits;
	p->n_last = MOD8(n_bits);
	if (!p->n_last && n_bits)
		p->n_last = 8;

	// how many bytes does this bit array occupy?
	p->n_bytes = DIV8(p->n_bits);
	if (p->n_last && p->n_last != 8)
		++p->n_bytes;

	// is data provided?
	p->allocated_dynamically = (data) ? 1 : 0;

	// if data is provided, point to it, else allocate and point to
	// 0-initialised heap memory
	if (p->allocated_dynamically)
		p->bytes = data;
	else
		CALLOC(p->bytes, p->n_bytes);

	return p;
}
