#include "../../include/memory.h"
#include "../bits.h"
#include "../def/def.h"

#include <stdlib.h>


// functions
void bits_destroy(struct bits_t *bits)
{
	if (!bits)
		return;

	if (bits->allocated_dynamically)
		FREE(bits->bytes);

	FREE(bits);
}
