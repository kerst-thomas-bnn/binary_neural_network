#include "../bits.h"
#include "../def/def.h"


// static functions
static unsigned int popcount8(unsigned char value)
{
	unsigned int ones = 0;

	for (; value; ++ones)
		value &= value - 1;

	return ones;
}


// functions
unsigned int bits_xnor_uc(const struct bits_t *bits, const unsigned char *uc)
{
	unsigned int n = bits->n_bytes;
	unsigned int internal_field = 0;

	for (unsigned int i = 0; i < n; ++i) {
		unsigned char xor_val = bits->bytes[i] ^ uc[i];
		internal_field += popcount8(xor_val);
	}

	return internal_field;
}
