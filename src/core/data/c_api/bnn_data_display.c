#include "../../../c_api/bnn.h"
#include "../../tensor/tensor.h"
#include "../memory/memory.h"

#include <stdio.h>
#include <stdbool.h>


// static functions
static bool is_data_stored(const struct items_mem_t *p)
{
	// data is stored
	if (p)
		return true;

	// data is not stored
	fprintf(stdout, "No data stored in memory\n");

	return false;
}

static bool is_item_stored(const struct items_mem_t *p, unsigned int n)
{
	// item is stored
	if (n < p->n_items)
		return true;

	// item is not stored
	fprintf(stdout, "No item with number %u stored in memory\n", n);

	return false;
}


// functions
void bnn_data_display(unsigned int n)
{
	struct items_mem_t *p = data_memory_set_get(0, ITEMS_MEM_RETRIEVE);

	// perform checks
	if (!is_data_stored(p) || !is_item_stored(p, n))
		return;

	// display
	const struct item_t *item = &p->items[n];

	// - x
	fprintf(stdout, "\nx:\n\n");
	tensor_display(item->x);

	// - y
	fprintf(stdout, "\ny:\n\n");
	tensor_display(item->y);
}
