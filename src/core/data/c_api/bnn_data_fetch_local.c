#include "../../../c_api/bnn.h"
#include "../../include/memory.h"
#include "../../include/debug.h"
#include "../../include/error.h"
#include "../../setup/setup.h"
#include "../../tensor/tensor.h"
#include "../memory/memory.h"

#include <string.h>


// constants
#define MSG_ERR_STORE "Local data could not be stored in memory"
#define MSG_ERR_OPEN "Unable to open loca data file"
#define MSG_ERR_CHECKSUM "Checksums of locally fetched data do not match"
#define MSG_ERR_UNINITIALISED "Unable to load data, bnn is uninitialised"


// static variables
static struct items_mem_t *data_local;
static size_t size_data;
static unsigned char *data_raw;


// static functions
static void copy_data_into_memory(const char *path)
{
	// open file
	FILE *f = fopen(path, "r");
	FATAL_ERROR(!f, MSG_ERR_OPEN);

	// get file size
	FATAL_ERROR(fseek(f, 0, SEEK_END), MSG_ERR_OPEN);
	size_data = (size_t)ftell(f);
	FATAL_ERROR(fseek(f, 0, SEEK_SET), MSG_ERR_OPEN);

	// copy the data into memory
	MALLOC(data_raw, size_data);
	size_t checksum = fread(data_raw, 1, size_data, f);
	FATAL_ERROR(size_data != checksum, MSG_ERR_CHECKSUM);

	// clean up
	fclose(f);
}

static void format_data(void)
{
	// get sizes and shapes
	// - x
	unsigned int r_x = setup_get_rank(SETUP_IO_X);
	const unsigned int *s_x = setup_get_shape(SETUP_IO_X);
	unsigned int n_x = setup_get_n_bytes(SETUP_IO_X);

	// - y
	unsigned int r_y = setup_get_rank(SETUP_IO_Y);
	const unsigned int *s_y = setup_get_shape(SETUP_IO_Y);
	unsigned int n_y = setup_get_n_bytes(SETUP_IO_Y);

	// allocate memory
	unsigned int n_bytes_total = n_x + n_y;
	FATAL_ERROR(!n_bytes_total, MSG_ERR_UNINITIALISED);
	data_local->n_items = size_data / n_bytes_total;
	MALLOC(data_local->items, data_local->n_items);

	// construct the tensors
	for (unsigned int i = 0; i < data_local->n_items; ++i) {
		// x
		unsigned char *data_x;
		MALLOC(data_x, n_x);
		memcpy(data_x, &data_raw[i * n_bytes_total], n_x);
		data_local->items[i].x = tensor_create(r_x, s_x, data_x);

		// y
		unsigned char *data_y;
		MALLOC(data_y, n_y);
		memcpy(data_y, &data_raw[i * n_bytes_total + n_x], n_y);
		data_local->items[i].y = tensor_create(r_y, s_y, data_y);
	}

	// clean up
	FREE(data_raw);
}


// functions
void bnn_data_fetch_local(const char *path)
{
	// initialise
	CALLOC(data_local, 1);
	copy_data_into_memory(path);
	format_data();

	// store formatted data in memory
	const struct items_mem_t *data_stored;
	data_stored = data_memory_set_get(data_local, ITEMS_MEM_STORE);
	FATAL_ERROR(data_stored != data_local, MSG_ERR_STORE);
}
