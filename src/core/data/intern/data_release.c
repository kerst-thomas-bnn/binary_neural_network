#include "../../include/debug.h"
#include "../../include/memory.h"
#include "../../include/error.h"
#include "../../tensor/tensor.h"
#include "../memory/memory.h"
#include "../data.h"


// static functions
static void release_items(struct items_mem_t *p)
{
	// return, if no data is allocated
	if (!p)
		return;

	// release the items
	for (unsigned int i = 0; i < p->n_items; ++i) {
		tensor_destroy(p->items[i].x);
		tensor_destroy(p->items[i].y);
	}
	FREE(p->items);
}


// functions
void data_release(void)
{
	// fetch the pointer and release its contents
	struct items_mem_t *p = data_memory_set_get(0, ITEMS_MEM_RETRIEVE);
	release_items(p);
	FREE(p);

	// save the freed memory
	p = data_memory_set_get(0, ITEMS_MEM_STORE);
	FATAL_ERROR(p, "Unable to release stored data");
}
