#include "../../include/debug.h"
#include "../memory/memory.h"
#include "../data.h"


// functions
unsigned int data_get_number_of_local_test_items()
{
	struct items_mem_t *p = data_memory_set_get(0, ITEMS_MEM_RETRIEVE);

	return (p) ? p->n_items : 0;
}
