#include "../../include/debug.h"
#include "../memory/memory.h"
#include "../data.h"

#include <stdio.h>
#include <stdbool.h>


// static functions
static bool are_these_many_items_even_stored(unsigned int item_number)
{
	unsigned int n_items = data_get_number_of_local_test_items();

	// yes, these many items are indeed stored
	if (item_number < n_items)
		return true;

	// no, these many items are not stored
	const char *msg = "\n[BNN_MODEL_PUSH_LOCAL] Trying to access locally stored input item with number %u, though only items with numbers 0 - %u are accessible. Returning NULL pointer.\n";
	fprintf(stdout, msg, item_number, n_items - 1);

	return false;
}


// functions
const struct tensor_t *data_get_local_input(unsigned int item_number)
{
	// check if this many items are stored
	if (!are_these_many_items_even_stored(item_number))
		return 0;

	// fetch the data
	struct items_mem_t *p = data_memory_set_get(0, ITEMS_MEM_RETRIEVE);

	return p->items[item_number].x;
}
