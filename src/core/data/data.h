#ifndef DATA_H
#define DATA_H


// forward declarations
struct tensor_t;


// functions
unsigned int data_get_number_of_local_test_items(void);

const struct tensor_t *data_get_local_input(unsigned int item_number);

const struct tensor_t *data_get_local_output(unsigned int item_number);

void data_release(void);


#endif
