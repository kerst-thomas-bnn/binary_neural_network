#ifndef DATA_MEMORY_H
#define DATA_MEMORY_H


// forward declarations
struct tensor_t;


// enumerations
enum items_mem_op {
	ITEMS_MEM_STORE,
	ITEMS_MEM_RETRIEVE
};


// structs
struct item_t {
	struct tensor_t *x;
	struct tensor_t *y;
};

struct items_mem_t {
	struct item_t *items;
	unsigned int n_items;
};


// functions
struct items_mem_t *data_memory_set_get(struct items_mem_t *new_mem,
                                        enum items_mem_op op);


#endif
