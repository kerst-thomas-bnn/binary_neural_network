#include "../../memory/memory.h"


// functions
struct items_mem_t *data_memory_set_get(struct items_mem_t *new_mem,
                                        enum items_mem_op op)
{
	static struct items_mem_t *stored = 0;
	if (op == ITEMS_MEM_STORE)
		stored = new_mem;

	return stored;
}
