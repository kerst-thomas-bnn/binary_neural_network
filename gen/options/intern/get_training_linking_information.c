#include "../options.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


// constants
#define CONFIG_PATH "configure/train/config"
#define STRING_BUFFER 256

// - strings
#define GEN_ERROR "[GEN ERROR] %s. Aborting library configuration.\n"


// macros
#define FATAL_ERROR(CONDITION, MESSAGE)  \
	do {  \
		if ((CONDITION)) {  \
			fprintf(stdout, GEN_ERROR, (MESSAGE));  \
			exit(1);  \
		}  \
	} while(0)

#define WRITE_TO_FILE(FILE, STRING, ERROR_MESSAGE)  \
	do {  \
		size_t len = fwrite((STRING), 1, strlen((STRING)), (FILE));  \
		FATAL_ERROR(strlen((STRING)) != len, (ERROR_MESSAGE));  \
	} while(0)


// static variables
static char link_folder[STRING_BUFFER];
static char link_library[STRING_BUFFER];


// static functions
static char *get_file_content(void)
{
	// build file path
	char file_path[STRING_BUFFER];
	char *p = getcwd(file_path, STRING_BUFFER);
	for (p += strlen(p) ; *p != '/'; --p)
	sprintf(p, "%s", CONFIG_PATH);

	const char msg[] = "Unable to read out the training configuration";
	// open file
	FILE *f = fopen(file_path, "r");
	FATAL_ERROR(!f, msg);

	// get length
	FATAL_ERROR(fseek(f, 0, SEEK_END), msg);
	size_t len = (size_t)ftell(f);
	FATAL_ERROR(fseek(f, 0, SEEK_SET), msg);

	// read out file
	char *file_content = malloc(len + 1);
	FATAL_ERROR(len != fread(file_content, 1, len, f), msg);
	file_content[len] = 0;

	// close file
	fclose(f);

	return file_content;
}

static void extract_link_folder(const char *line)
{
	// get position of the last '/'
	char *p = strstr(line, "\n");
	for (p += strlen(p); *p != '/'; --p);

	// assemble the link_folder string
	size_t len = p - line;
	sprintf(link_folder, " -L%s", line);
	link_folder[len + 3] = 0;
}

static void extract_link_library(const char *line)
{
	// get position of the last '/'
	char *p = strstr(line, "\n");
	for (p += strlen(p); *p != '/'; --p);

	// get the positin of the last '.'
	char *q = strstr(line, "\n");
	for (q += strlen(q); *q != '.'; --q);

	// assemble the link_folder string
	size_t len = q - p;
	sprintf(link_library, " -l%s", p + 4);
	link_library[len - 1] = 0;
}

static void write_link_instruction(char *instruction)
{
	// if training is not enabled, return
	if (!is_training_enabled())
		return;

	// get relevant file content
	char *file_content = get_file_content();
	char *second_line = strstr(file_content, "\n") + 1;

	// extract the link parameters
	extract_link_folder(second_line);
	extract_link_library(second_line);

	// write the instructions
	sprintf(instruction, "%s%s", link_folder, link_library);

	// clean up
	free(file_content);
}


// functions
const char *get_training_linking_information()
{
	// initialise
	static char link_instruction[STRING_BUFFER];
	link_instruction[0] = 0;

	// write the link link_instruction
	write_link_instruction(link_instruction);

	return link_instruction;
}
