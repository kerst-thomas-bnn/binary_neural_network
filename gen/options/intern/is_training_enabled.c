#include "../options.h"

#include <stdio.h>
#include <unistd.h>
#include <string.h>


// constants
#define CONFIG_PATH "/configure/train/config"
#define STRING_BUFFER 256


// static functions
static const char *build_absolute_file_path(void)
{
	static char buffer[STRING_BUFFER];

	// get workign directory
	char *p = getcwd(buffer, STRING_BUFFER);

	// paste the config path
	for (p += strlen(buffer); *p != '/'; --p);
	sprintf(p, "%s", CONFIG_PATH);

	return buffer;
}


// functions
unsigned char is_training_enabled()
{
	const char *absolute_path = build_absolute_file_path();

	return (access(absolute_path, F_OK) != -1) ? 1 : 0;
}
