#ifndef OPTIONS_H
#define OPTIONS_H


// functions
unsigned char is_training_enabled(void);

const char *get_training_linking_information(void);


#endif
