#ifndef TREE_H
#define TREE_H


// forward declarations
struct tree_element_t;


// functions
struct tree_element_t *tree_build(void);

void tree_free(struct tree_element_t *tree);

void tree_add_folders(void *file, const struct tree_element_t *tree_element);

void tree_add_link_instructions(void *file,
                                const struct tree_element_t *tree_element);

void tree_add_compilation_instructions(void *file,
                                       const struct tree_element_t *tree_elem);

void tree_add_cleaning_instructions(void *file,
                                    const struct tree_element_t *tree_element);


#endif
