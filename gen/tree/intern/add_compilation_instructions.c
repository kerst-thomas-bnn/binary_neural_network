#include "../tree.h"
#include "include/def.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// constants
#ifdef __APPLE__
	#define FLAGS_COMPILE "-c -O3 -Wall -std=c99 -march=native -mtune=native"
#endif
#ifndef FLAGS_COMPILE
	#define FLAGS_COMPILE "-c -O3 -Wall -std=c99 -fPIC"
	#define OPT " -march=native -mtune=native"
#endif

#define FE_HEADER "Unable to write the compilation instructions to the makefile. Aborting program execution\n"
#define FE_DEP "Unable to write the dep information to the makefile. Aborting program execution\n"
#define FE_DEP_CHECK "Unable to open file %s to check dependencies. Aborting program execution.\n"
#define FE_COM "Unable to write the compilation instructions to the makefile. Aborting program execution\n"

#define COMP_LEFT "\n\t@gcc "
#define COMP_RIGHT " $< -o $@  \\\n\t$(eval cnt=$(shell echo $$(($(cnt) + 1))))  \\\n\t$(eval progress=$(shell printf [%3d%%] \"$$((100 * $(cnt) / $(N_TARGETS)))\"))  \\\n\t&& echo  \"${progress} \e[34mCompiling  $<\e[0m\"\n\n"


// static variables
static FILE *f;


// static functions
static void add_header(void)
{
	const char header[] = "\n# compiling\n";
	size_t r = fwrite(header, strlen(header), sizeof(*header), f);
	if (!r) {
		fprintf(stdout, FE_HEADER);
		exit(1);
	}
}

static void add_target(char *tar, const char *src)
{
	// find position of last '/' and copy up to there
	const char *pos = src + strlen(src);
	while (*--pos != '/');
	size_t len = pos - src - 7;
	memcpy(tar, src + 7, len);

	// and then the rest of the string
	sprintf(tar + len, "/bin/%s", pos + 1);
}

static void scan_file_content(const char *path, char *fc)
{
	for (char *p = strtok(fc, "\n"); p; p = strtok(0, "\n")) {
		// filter out dependencies
		if (strstr(p, "#include \"") != p)
			continue;

		// isolte the string
		char buffer[STRING_BUFFER];
		sprintf(buffer, "%s", strstr(p, "\"") + 1);
		*strstr(buffer, "\"") = 0;

		// determine the indentation level
		char *buffer_pos = buffer;
		size_t indentation_level = 1;
		while (*buffer_pos == '.') {
			buffer_pos += 3;
			++indentation_level;
		}

		// create the dependency string
		char dep[STRING_BUFFER];
		sprintf(dep, " %s", path + 7);

		// - indent
		for (char *dp = dep + strlen(dep); dp > dep; --dp) {
			if (*dp == '/') {
				--indentation_level;
				*dp = 0;
			}
			if (!indentation_level)
				break;
		}
		size_t offset = indentation_level ? 1 : strlen(dep);
		const char *seperator = indentation_level ? "" : "/";

		// - and build the string
		sprintf(dep + offset, "%s%s", seperator, buffer_pos);

		// then write to the file
		size_t r = fwrite(dep, strlen(dep), sizeof(*dep), f);
		if (!r) {
			fprintf(stdout, FE_DEP);
			exit(1);
		}
	}
}

static void add_dependencies(const char *path)
{
	// load the file into memory
	int r;
	FILE *fp = fopen(path, "r");
	if (!fp) {
		fprintf(stdout, FE_DEP_CHECK, path);
		exit(1);
	}
	r = fseek(fp, 0, SEEK_END);
	if (r) {
		fprintf(stdout, FE_DEP_CHECK, path);
		exit(1);
	}
	size_t len = (size_t)ftell(fp);
	r = fseek(fp, 0, SEEK_SET);
	if (r) {
		fprintf(stdout, FE_DEP_CHECK, path);
		exit(1);
	}

	char file_content[len + 1];
	size_t rr = fread(file_content, len, 1, fp);
	if (!rr) {
		fprintf(stdout, FE_DEP_CHECK, path);
		exit(1);
	}
	file_content[len] = 0;
	fclose(fp);

	scan_file_content(path, file_content);
}

static void add_opt(char *b)
{
	#ifdef OPT
		sprintf(b + strlen(b), "%s", (__GNUC__ > 4) ? OPT : "");
	#endif
}

static void check_element(const struct tree_element_t *p)
{
	// write out the compilation instruction
	for (unsigned int i = 0; i < p->n_compilation_units; ++i) {
		// add target
		const char *unit = p->compilation_units[i];
		char buffer[STRING_BUFFER];
		add_target(buffer, unit);

		// add source file
		sprintf(buffer + strlen(buffer) - 1, "o: %s", unit + 7);
		size_t r = fwrite(buffer, strlen(buffer), sizeof(*buffer), f);
		if (!r) {
			fprintf(stdout, FE_COM);
			exit(1);
		}

		// add depedencies
		add_dependencies(unit);

		// add compilation instructions
		sprintf(buffer, "%s%s", COMP_LEFT, FLAGS_COMPILE);
		add_opt(buffer);
		sprintf(buffer + strlen(buffer), "%s", COMP_RIGHT);

		// and write it to the makefile
		r = fwrite(buffer, strlen(buffer), sizeof(*buffer), f);
		if (!r) {
			fprintf(stdout, FE_COM);
			exit(1);
		}
	}

	for (unsigned int i = 0; i < p->n_children; ++i)
		check_element(p->children[i]);
}


// functions
void tree_add_compilation_instructions(void *file,
                                       const struct tree_element_t *tree_elem)
{
	f = (FILE *)file;
	add_header();
	check_element(tree_elem);
}
