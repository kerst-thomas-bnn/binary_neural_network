#include "../tree.h"
#include "../../options/options.h"
#include "include/def.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


// constants
#ifdef __APPLE__
	#define FLAG_SHARED "-dynamiclib"
	#define LIB_TYPE "dylib"
#endif
#ifndef FLAG_SHARED
	#define FLAG_SHARED "-shared"
#endif
#ifndef LIB_TYPE
	#define LIB_TYPE "so"
#endif

#define FE_LNK "Unable to write the linking instructions to the makefile. Aborting program execution\n"

#define HEADER "# linking\nc_api/libbnn."
#define LINK_LEFT "\n\t@gcc $^ "
#define LINK_RIGHT " -o $@  \\\n\t&& echo \"[100%] \e[32mBuilding   libbnn\e[0m\"\n\n"
#define LINKING "\n\t@ld -r $^ -o $@  \\\n\t$(eval cnt=$(shell echo $$(($(cnt) + 1))))  \\\n\t$(eval progress=$(shell printf [%3d%%] \"$$((100  *$(cnt) / $(N_TARGETS)))\"))  \\\n\t&& echo \"${progress} \e[36mLinking    $@\e[0m\"\n\n"


// static variables
static FILE *f;


// static functions
static void add_object_file(char *tar, const char *src)
{
	// find position of last '/' and copy up to there
	const char *pos = src + strlen(src);
	while (*--pos != '/');
	size_t len = pos - src - 7;
	memcpy(tar, src + 7, len);

	// and then the rest of the string
	sprintf(tar + len, "/bin/%s", pos + 1);

	// add the object file ending
	char *wrt_pos = strstr(tar + len, ".c");
	if (!wrt_pos)
		wrt_pos = tar + strlen(tar);

	sprintf(wrt_pos, ".o");
}

static void add_library_linking_instruction(const struct tree_element_t *p)
{
	// assemble the instruction string
	char buffer[STRING_BUFFER];
	sprintf(buffer, "%s%s:", HEADER, LIB_TYPE);
	memcpy(buffer, HEADER, strlen(HEADER));
	for (unsigned int i = 0; i < p->n_link_targets; ++i) {
		size_t len = strlen(buffer);
		buffer[len++] = ' ';
		add_object_file(buffer + len, p->link_targets[i]);
		len = strlen(buffer);
	}
	sprintf(buffer + strlen(buffer), "%s%s%s%s",
		LINK_LEFT,
		FLAG_SHARED,
		get_training_linking_information(),
		LINK_RIGHT
	);

	// and add it to the makefile
	size_t r = fwrite(buffer, strlen(buffer), sizeof(*buffer), f);
	if (!r) {
		fprintf(stdout, FE_LNK);
		exit(1);
	}
}

static void check_element(const struct tree_element_t *p)
{
	if (p->n_link_targets + p->n_compilation_units) {
		// add link target
		char buffer[STRING_BUFFER];
		add_object_file(buffer, p->name);
		size_t len = strlen(buffer);
		buffer[len++]= ':';

		// link lower-order link targets
		for (unsigned int i = 0; i < p->n_link_targets; ++i) {
			buffer[len++] = ' ';
			add_object_file(buffer + len, p->link_targets[i]);
			len = strlen(buffer);
		}

		// add compilation units
		for (unsigned int i = 0; i < p->n_compilation_units; ++i) {
			buffer[len++] = ' ';
			add_object_file(buffer + len, p->compilation_units[i]);
			len = strlen(buffer);
		}

		// add the link instruction
		sprintf(buffer + strlen(buffer), "%s", LINKING);

		// and add it to the makefile
		size_t r = fwrite(buffer, strlen(buffer), sizeof(*buffer), f);
		if (!r) {
			fprintf(stdout, FE_LNK);
			exit(1);
		}
	}

	for (unsigned int i = 0; i < p->n_children; ++i)
		check_element(p->children[i]);
}


// functions
void tree_add_link_instructions(void *file,
                                const struct tree_element_t *tree_element)
{
	f = (FILE *)file;
	add_library_linking_instruction(tree_element);
	for (unsigned int i = 0; i < tree_element->n_children; ++i)
		check_element(tree_element->children[i]);
}
