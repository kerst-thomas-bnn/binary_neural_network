#include "../tree.h"
#include "include/def.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// constants
#ifdef __APPLE__
	#define LIB_TYPE "dylib"
#endif
#ifndef LIB_TYPE
	#define LIB_TYPE "so"
#endif

#define ERR_MSG "Unable to write the cleaning instructions to the makefile. Aborting program execution\n"
#define FOOTER "\\\n\t&& echo \"Reset the build. Removed all temporary files.\"\n"


// static variables
static FILE *f;


// static functions
static void add_header(void)
{
	const char header[] = "\n# cleaning\nclean:\n\t@rm -f c_api/libbnn.";
	char buffer[STRING_BUFFER];
	sprintf(buffer, "%s%s", header, LIB_TYPE);

	size_t r = fwrite(buffer, strlen(buffer), sizeof(*buffer), f);
	if (!r) {
		fprintf(stdout, ERR_MSG);
		exit(1);
	}
}

static void check_element(const struct tree_element_t *p)
{
	if (p->n_link_targets + p->n_compilation_units) {
		// compile the buffer
		char buffer[STRING_BUFFER];
		sprintf(buffer, "  \\\n\t%s/bin/*", p->name + 7);
		size_t r = fwrite(buffer, strlen(buffer), sizeof(*buffer), f);
		if (!r) {
			fprintf(stdout, ERR_MSG);
			exit(1);
		}
	}

	for (unsigned int i = 0; i < p->n_children; ++i)
		check_element(p->children[i]);
}

static void add_footer(void)
{
	size_t r = fwrite(FOOTER, strlen(FOOTER), sizeof(*FOOTER), f);
	if (!r) {
		fprintf(stdout, ERR_MSG);
		exit(1);
	}
}


// functions
void tree_add_cleaning_instructions(void *file,
                                    const struct tree_element_t *tree_element)
{
	f = (FILE *)file;
	add_header();
	check_element(tree_element);
	add_footer();
}
