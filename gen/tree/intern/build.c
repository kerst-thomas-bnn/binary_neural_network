#include "../../options/options.h"
#include "../tree.h"
#include "include/def.h"

#include <sys/stat.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


// constants
#define FE_CONFIG "Unable to configure files in directory %s. Aborting program execution.\n"

#define START_FOLDER "../src/core"
#define P_TRAIN "../src/core/train"


// foward declarations
static struct tree_element_t *init_element(struct tree_element_t*,
                                           const char*);


// static functions
static void add_directory(struct tree_element_t *p, const char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);
	if (S_ISREG(path_stat.st_mode))
		return;

	++p->n_children;
	size_t size = p->n_children  *sizeof(*p->children);
	p->children = realloc(p->children, size);
	p->children[p->n_children - 1] = init_element(p, path);
}

static void add_compilation_unit(struct tree_element_t *p,
                                 const struct dirent *d,
                                 const char *path)
{
	if (!(strstr(d->d_name, ".c") == d->d_name + strlen(d->d_name) - 2))
		return;

	++p->n_compilation_units;
	size_t size = p->n_compilation_units  *sizeof(*p->compilation_units);
	p->compilation_units = realloc(p->compilation_units, size);
	char *s = malloc(STRING_BUFFER);
	memcpy(s, path, strlen(path) + 1);
	p->compilation_units[p->n_compilation_units - 1] = s;
}

static struct tree_element_t *init_element(struct tree_element_t *parent,
                                           const char *path)
{
	// initialise the tree element
	struct tree_element_t *p = calloc(1, sizeof(*p));

	// - fill in name and parent
	memcpy(p->name, path, strlen(path) + 1);
	p->parent = parent;

	// open the directory
	DIR *dir = opendir(path);
	if (!dir) {
		fprintf(stdout, FE_CONFIG, path);
		exit(1);
	}

	// read out directory
	for (struct dirent *d = readdir(dir); d; d = readdir(dir)) {
		// skip hidden files and links
		if (*d->d_name == '.')
			continue;

		// construct the full path
		char new_path[STRING_BUFFER];
		sprintf(new_path, "%s/%s", path, d->d_name);

		// is training enabled?
		if (!strcmp(new_path, P_TRAIN) && !is_training_enabled())
			continue;

		// is it a directory?
		add_directory(p, new_path);

		// is it a compilation unit?
		add_compilation_unit(p, d, new_path);
	}

	closedir(dir);

	return p;
}

static bool child_has_compilation_units(const struct tree_element_t *p)
{
	if (p->n_compilation_units)
		return true;

	for (unsigned int i = 0; i < p->n_children; ++i)
		if (child_has_compilation_units(p->children[i]))
			return true;

	return false;
}

static void link_child(struct tree_element_t *p, unsigned int k)
{
	if (!child_has_compilation_units(p->children[k]))
		return;

	++p->n_link_targets;
	size_t size = p->n_link_targets  *sizeof(*p->link_targets);
	p->link_targets = realloc(p->link_targets, size);
	char *s = malloc(STRING_BUFFER);
	memcpy(s, p->children[k]->name, strlen(p->children[k]->name) + 1);
	p->link_targets[p->n_link_targets - 1] = s;
}

static void set_link_targets(struct tree_element_t *p)
{
	// add link targets if necessary
	for (unsigned int i = 0; i < p->n_children; ++i)
		link_child(p, i);

	for (unsigned int i = 0; i < p->n_children; ++i)
		set_link_targets(p->children[i]);
}


// functions
struct tree_element_t *tree_build(void)
{
	struct tree_element_t *root = init_element(0, START_FOLDER);
	set_link_targets(root);

	return root;
}
