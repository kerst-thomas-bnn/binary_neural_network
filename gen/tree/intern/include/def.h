#ifndef TREE_DEF_H
#define TREE_DEF_H


// constants
#define STRING_BUFFER 2048


// structs
struct tree_element_t {
	// name
	char name[STRING_BUFFER];

	// parent
	struct tree_element_t* parent;

	// children
	unsigned int n_children;
	struct tree_element_t** children;

	// compilation units
	unsigned int n_compilation_units;
	char** compilation_units;

	// link targets
	unsigned int n_link_targets;
	char** link_targets;
};


#endif
