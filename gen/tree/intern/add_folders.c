#include "../tree.h"
#include "include/def.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// constants
#define FE_HEADER "Unable to write to the folder adding information to the makefile. Aborting program execution\n"


// static variables
static FILE *f;


// static functions
static void write_header(void)
{
	const char header[] = "add_folders:\n\t@mkdir -p";
	size_t r = fwrite(header, strlen(header), sizeof(*header), f);
	if (!r) {
		fprintf(stdout, FE_HEADER);
		exit(1);
	}
}

static void add_folder(const char *name)
{
	// construct the instruction
	char wrt_string[STRING_BUFFER];
	sprintf(wrt_string, "  \\\n\t%s/bin", name + 7);

	// write it
	size_t r = fwrite(wrt_string, strlen(wrt_string), sizeof(*wrt_string), f);
	if (!r) {
		fprintf(stdout, FE_HEADER);
		exit(1);
	}
}

static void check_element(const struct tree_element_t *p)
{
	if (p->n_link_targets || p->n_compilation_units)
		add_folder(p->name);

	for (unsigned int i = 0; i < p->n_children; ++i)
		check_element(p->children[i]);
}

static void write_footer(void)
{
	const char footer[] = "\n\n\n";
	size_t r = fwrite(footer, strlen(footer), sizeof(*footer), f);
	if (!r) {
		fprintf(stdout, FE_HEADER);
		exit(1);
	}
}


// functions
void tree_add_folders(void *file, const struct tree_element_t *tree_element)
{
	f = (FILE *)file;
	write_header();
	check_element(tree_element);
	write_footer();
}
