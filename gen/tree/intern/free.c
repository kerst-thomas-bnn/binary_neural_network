#include "../tree.h"
#include "include/def.h"

#include <stdio.h>
#include <stdlib.h>


// static functions
static void destroy_element(struct tree_element_t *p)
{
	// free the compilation unit information
	for (unsigned int i = 0; i < p->n_compilation_units; ++i)
		free(p->compilation_units[i]);

	free(p->compilation_units);

	// free the link target information
	for (unsigned int i = 0; i < p->n_link_targets; ++i)
		free(p->link_targets[i]);

	free(p->link_targets);

	// iterate through the children
	for (unsigned int i = 0; i < p->n_children; ++i)
		destroy_element(p->children[i]);

	free(p->children);

	// free the tree element itself
	free(p);
}


// functions
void tree_free(struct tree_element_t *tree)
{
	destroy_element(tree);
}
