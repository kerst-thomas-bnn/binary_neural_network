#include "include/debug.h"
#include "include/constants.h"
#include "tree/tree.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// constants
#define STRING_BUFFER 512
#ifdef __APPLE__
	#define LIB_TYPE "dylib"
#endif
#ifndef LIB_TYPE
	#define LIB_TYPE "so"
#endif
#ifdef __linux__
	#define LDCONFIG "  \\\n\t&& ldconfig"
#endif
#ifndef LDCONFIG
	#define LDCONFIG ""
#endif


// static variables
static FILE *f;
static struct tree_element_t *tree;


// static functions
static void start_writing(void)
{
	f = fopen(PATH_MAKEFILE, "w");
	if (!f) {
		fprintf(stdout, FE_WRT, PATH_MAKEFILE);
		exit(1);
	}
}

static void build_tree(void)
{
	tree = tree_build();
}

static void add_preamble(void)
{
	char buffer[STRING_BUFFER];
	sprintf(buffer, "%s%s\n\n", PREAMBLE_HEADER, LIB_TYPE);

	size_t r = fwrite(buffer, strlen(buffer), sizeof(*buffer), f);
	if (!r) {
		fprintf(stdout, FE_PREAMBLE);
		exit(1);
	}
}

static void free_tree(void)
{
	tree_free(tree);
	tree = 0;
}

static void finish_writing(void)
{
	fclose(f);
	f = 0;
}


// functions
int main(int argc, char const *argv[])
{
	// initialise
	start_writing();
	build_tree();

	// add content
	add_preamble();
	tree_add_folders(f, tree);
	tree_add_link_instructions(f, tree);
	tree_add_compilation_instructions(f, tree);
	tree_add_cleaning_instructions(f, tree);

	// clean up
	free_tree();
	finish_writing();

	return 0;
}
