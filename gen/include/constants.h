#ifndef CONSTANTS_H
#define CONSTANTS_H


// constants
#define PATH_MAKEFILE "../src/makefile"

#define FE_WRT "Unable to write to %s. Aborting program execution.\n"
#define FE_PREAMBLE "Unable to write the preamble to the makefile. Aborting program execution\n"

#define PREAMBLE_HEADER "# variables\ncnt = 0\nprogress = \n\n\n# phony\n.PHONY: default build add_folders clean\n\n\n# default\ndefault: build\n\nbuild: add_folders c_api/libbnn."


#endif
