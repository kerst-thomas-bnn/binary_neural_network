# OS-specifics
UNAME_S := $(shell uname -s)

# LIB_ENDING
LIB_ENDING = dylib
ifeq ($(UNAME_S), Linux)
	LIB_ENDING = so
endif

# LD_CONFIG
LD_CONFIG =
ifeq ($(UNAME_S), Linux)
	LD_CONFIG = && ldconfig
endif

# N_TARGETS
N_TARGETS =


# phony
.PHONY: default pre_compile generate_makefile get_nr_of_targets build configure_train clean install remove

# silent
.SILENT:

# default
default: pre_compile generate_makefile get_nr_of_targets build

pre_compile:
	@cd pre  \
	&& ${MAKE} clean  \
	&& ${MAKE}  \
	&& ./pre  \
	&& cd ..

generate_makefile:
	@cd gen  \
	&& ${MAKE} clean  \
	&& ${MAKE}  \
	&& ./gen  \
	&& cd ..

get_nr_of_targets:
	$(eval N_TARGETS=$(shell cd src && ${MAKE} -n N_TARGETS=1))  \
	$(eval N_TARGETS=$(filter echo, $(N_TARGETS)))  \
	$(eval N_TARGETS=$(words $(N_TARGETS)))

build:
	@cd src  \
	&& ${MAKE} N_TARGETS=$(N_TARGETS)

clean:
	@cd src  \
	&& ${MAKE} clean

configure_train:
	@cd configure/train  \
	&& ${MAKE} clean  \
	&& ${MAKE}  \
	&& ./train  \
	&& cd ../..


# install
install:
ifeq (${USER}, root)
	@cp src/c_api/bnn.h /usr/local/include/bnn.h  \
	&& cp src/c_api/libbnn.${LIB_ENDING} /usr/local/lib/libbnn.${LIB_ENDING}  \
	${LD_CONFIG}  \
	&& echo "Successfully installed libbnn"
else
	@echo "Install libbnn by running \"sudo make install\""
endif


# remove
remove:
ifeq (${USER}, root)
	@rm -f /usr/local/include/bnn.h  \
	&& rm -f /usr/local/lib/libbnn.${LIB_ENDING}  \
	${LD_CONFIG}  \
	&& echo "Successfully removed libbnn"
else
	@echo "Remove libbnn by running \"sudo make remove\""
endif
