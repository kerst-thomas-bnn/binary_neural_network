#include <stdio.h>
#include <string.h>
#include <unistd.h>


// constants
#define STRING_BUFFER 512


// static variables
static char config_path[STRING_BUFFER];
static char header_path[STRING_BUFFER];
static char library_path[STRING_BUFFER];


// static functions
static void get_config_path(void)
{
	char *p = getcwd(config_path, STRING_BUFFER);
	sprintf(p + strlen(p), "/config");
}

static void get_header_path(void)
{
	// ask for input
	fprintf(stdout, "Path to header file: ");
	fflush(stdout);

	// record input
	fgets(header_path, STRING_BUFFER, stdin);
}

static void get_library_path(void)
{
	// ask for input
	fprintf(stdout, "Path to library: ");
	fflush(stdout);

	// record input
	fgets(library_path, STRING_BUFFER, stdin);
}

static void delete_config_file(void)
{
	remove(config_path);
}

static void write_config_file(void)
{
	FILE *f = fopen(config_path, "w");
	fwrite(header_path, 1, strlen(header_path), f);
	fwrite(library_path, 1, strlen(library_path), f);
	fclose(f);
}


// main
int main(int argc, const char *argv[])
{
	// get paths
	get_config_path();
	get_header_path();
	get_library_path();

	// if either path is empty, delete the config file and return
	if ((header_path[0] == '\n') || (library_path[0] == '\n')) {
		delete_config_file();

		return 0;
	}

	// if both paths are provided, write the config file
	write_config_file();

	return 0;
}
